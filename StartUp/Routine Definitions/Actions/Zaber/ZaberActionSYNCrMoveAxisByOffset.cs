﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using NS_Routines_Controller_Common.Routine_Definitions.Base_Classes;
using SFW;
using NS_Zaber_Motion_Controller;
using Zaber_Common.Events.Zaber;
using Zaber_Common.Events.Zaber.NS_ZaberComandEventsParameters;
using System.Threading;

namespace StartUp.Routine_Definitions.Actions.Zaber
{

    /// <summary>
    /// Specify the motion parameters for Zaber Axis device to move relative to actual axis position
    /// </summary>
    public class ZaberActionSYNCrMoveAxisByOffset : BaseActionDescriptor
    {
        #region Private members

        [XmlIgnore]
        ZaberControllerLogic m_ZaberMotionControllerLogic;
        [XmlIgnore]
        Boolean m_ActionPerforming = false;

        #endregion Private members

        #region Constructors

        /// <summary>
        /// Base empty constructor
        /// </summary>
        public ZaberActionSYNCrMoveAxisByOffset() :
            base()
        { }

          
       /// <summary>
       /// Base constructor, with initialize values
       /// </summary>
       /// <param name="inActionName">The Action name</param>
       /// <param name="axis_id">Axis Identifier</param>
       /// <param name="offset_MM">Motion offset parameter </param>
       /// <param name="motion_speed_MMS">Optional parameter, specify the motion target speed, if not set, use the previously used speed</param>
        /// <param name="motion_acceleration_MMS2">Optional parameter, specify the motion acceleration and deceleration, if not set, use the previously used parameter</param>
        public ZaberActionSYNCrMoveAxisByOffset(String inActionName, Byte axis_id, 
                                                     Single offset_MM,
                                                     Single motion_speed_MMS = -1,
                                                     Single motion_acceleration_MMS2 = -1)
            : this()
        {
            m_ActionName = inActionName;
            m_OffsetMM = offset_MM;
            m_MotionSpeedMMS = motion_speed_MMS;
            m_MotionAccelerationMMS2 = motion_acceleration_MMS2;  
        }

        #endregion Constructors

        #region Public properties

        [XmlIgnore]
        private Byte m_AxisID;
        /// <summary>
        /// Specify the axis ID to move
        /// </summary>
        [XmlElement("AxisID")]
        public Byte AxisID
        {
            get { return m_AxisID; }
            set { m_AxisID = value; }
        }

        [XmlIgnore]
        private Single m_OffsetMM;
        /// <summary>
        /// Specify the offset to move from axis position
        /// </summary>
        [XmlElement("OffsetMM")]
        public Single OffsetMM
        {
            get { return m_OffsetMM; }
            set { m_OffsetMM = value; }
        }
        

        [XmlIgnore]
        private Single m_MotionSpeedMMS;
        /// <summary>
        /// Specify the axis motion target speed
        /// </summary>
        [XmlElement("MotionSpeedMMS")]
        public Single MotionSpeedMMS
        {
            get { return m_MotionSpeedMMS; }
            set { m_MotionSpeedMMS = value; }
        }

        [XmlIgnore]
        private Single m_MotionAccelerationMMS2;
        /// <summary>
        /// Specify the axis motion acceleration and deceleration parameter
        /// </summary>
        [XmlElement("MotionAccelerationMMS2")]
        public Single MotionAccelerationMMS2
        {
            get { return m_MotionAccelerationMMS2; }
            set { m_MotionAccelerationMMS2 = value; }
        }
        
        #endregion

        #region Public functions
        #endregion

        #region Public overridden functions

        public override short SetMyDevices(Dictionary<string, IComponent> DeviceList, out String message)
        {
            message = "";
            Boolean bAllDevicesFound = false;
            foreach (object obj in DeviceList)
            {
                if (obj.GetType() == typeof(ZaberControllerLogic))
                {
                    m_ZaberMotionControllerLogic = (ZaberControllerLogic)obj;
                    bAllDevicesFound = true;
                    break;
                }
            }

            if (true == bAllDevicesFound)
                return NS_Common.ErrorCodesList.OK;
            else
            {
                message = "If you want to use 'ZaberActionMoveAxisByOffset' class, you have to pass 'NS_Zaber_Motion_Controller.ZaberLogic' to routine controller";
                return NS_Common.ErrorCodesList.FAILED;
            }
        }

        public override short RemoveMyDevices(out string error_message)
        {
            error_message = "";
            m_ZaberMotionControllerLogic = null;
            return NS_Common.ErrorCodesList.OK;
        }

        public override bool StopActionPerformance()
        {
            lock (m_SynchronizationObject) //myIOLogic)
            {
                if (null != m_SynchronizationObject)
                {
                    m_ThreadCancellationSource.Cancel();

                    System.Threading.Thread.Sleep(500);
                }

                SendEvent(new ZaberCommandEvent(ZaberCommandEventTypes.CT_STOP_AXIS_MOTION, new ZaberCommandPrmStopAxisMotion(m_AxisID)));
           
            }

            return true;
             
        }

        public override string GetShortDescription()
        {
            return "Move Axis";
        }

        public override string GetFullDescription()
        {
            return "Move specified axis to absolute target position with specified speed and acceleration parameters";
        }

        #endregion

        /// <summary>
        /// Action performance main thread
        /// </summary>
        protected override void ThreadFunction(object token)
        {
            Boolean bsucceded = true;
            try
            {
                m_ZaberMotionControllerLogic.ErrorStatus = NS_Common.ErrorCodesList.OK;
                m_ActionPerforming = true;
                SendEvent(new ZaberCommandEvent(ZaberCommandEventTypes.CT_MOVE_AXIS_RELATIVE, new ZaberCommandPrmMoveRelMM(m_AxisID, m_OffsetMM, m_MotionSpeedMMS, m_MotionAccelerationMMS2, (CancellationToken)token)));

                if (NS_Common.ErrorCodesList.OK != m_ZaberMotionControllerLogic.ErrorStatus)
                {
                    m_ActionPerforming = false;
                    String error_message = "Failed to start Motion action " + m_ActionName + " Axis " + m_AxisID + "\n\r" + m_ZaberMotionControllerLogic.ErrorMessage;
                    //if (m_SystemLog.IsErrorEnabled)
                    //    m_SystemLog.Error(error_message);
                    OnFailedDuringActionPerformance(this, NS_Common.ErrorCodesList.FAILED_TO_REACH_TARGET_POSITION, error_message);
                    bsucceded = false;
                }
            }
            catch (ThreadAbortException exc)
            {
                m_ActionPerforming = false;
                if (m_SystemLog.IsDebugEnabled)
                    m_SystemLog.Debug("AVI IV Action thread aborting, code is " + exc.ExceptionState);
                bsucceded = false;
            }
            catch (Exception ex)
            {
                m_ActionPerforming = false;
                OnFailedDuringActionPerformance(this, NS_Common.ErrorCodesList.FAILED, ex.Message);
                bsucceded = false;
            }
            finally
            {
                if (!bsucceded)
                {
                    m_ActionPerforming = false;
                    OnActionCompleted(this);
                }
            }
        
        }
    }
}
