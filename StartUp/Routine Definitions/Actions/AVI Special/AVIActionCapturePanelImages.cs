﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using NS_Routines_Controller_Common.Routine_Definitions.Base_Classes;
using NS_Video_Controller_Common.Events;
using NS_Video_Controller_Common.Events.NS_VideoComandEventsParameters;
using NS_VideoControllerSimulator;
using NS_Zaber_Motion_Controller;
using SFW;
using Zaber_Common.Events.Zaber;
using Zaber_Common.Events.Zaber.NS_ZaberComandEventsParameters;
using NS_Routines_Controller_Common.Events.Routines_Controller.RCDataEventParameters;
using NS_Routines_Controller_Common.Events.Routines_Controller;
using NS_Routines_Controller_Common.Events.Routines_Controller.RCComandEventsParameters;
using NS_Routines_Controller;

namespace StartUp.Routine_Definitions.Actions.AVI_Special
{
    /// <summary>
    /// Capture all Images for specific panel based on parameters
    /// </summary>
    public class AVIActionCapturePanelImages : BaseActionDescriptor, IEventGroupConsumer, IComponent
    {
        #region Private members

        [XmlIgnore]
        private ZaberControllerLogic m_ZaberMotionControllerLogic;
        [XmlIgnore]
        private ZaberControllerListener m_ZaberMotionControllerListener;

        [XmlIgnore]
        private VideoControllerLogic m_VideoControllerLogic;

        [XmlIgnore]
        private ConcurrentDictionary<System.Type, Action<IEventData>> m_Handlers;

        [XmlIgnore]
        Boolean m_ActionPerforming = false;

        #endregion Private members
        
        #region Constructors

        /// <summary>
        /// Base empty constructor
        /// </summary>
        public AVIActionCapturePanelImages() :
            base()
        { }


        /// <summary>
        /// Base constructor, with initialize values
        /// </summary>
        /// <param name="inActionName">Action Name to display</param>
        /// <param name="axis_ids">Specify the list of involved axes ID's to move</param>
        /// <param name="axes_start_positions">Specify the axes start positions(Zero Point) based on AxesIDs Order</param>
        /// <param name="first_stent_relative_coordinates">The list of coordinates based on AxesIDs order of TopLeft Point and Bottom Right Point of a single stent.</param>
        /// <param name="captured_image_size">Specify each image capture size</param>
        /// <param name="stents_in_row">Panel description, how many stents in row</param>
        /// <param name="overlap">Captured Images overlapping</param> 
        /// <param name="stents_in_columnn">Panel description, how many stents in column</param> 
        /// <param name="target_file_base_name">Specify base file name, all new stored images will be saved with this name and running index</param>
        /// <param name="target_files_directory">Specify target directory where images will be stored.</param>
        /// <param name="clean_directory_previous_results"> parameter that indicate if action should delete results files of previous tests</param>
        public AVIActionCapturePanelImages(String inActionName, 
                                           Byte[] axis_ids,
                                           PointF axes_start_positions,
                                           RectangleF first_stent_relative_coordinates,
                                           PointF stent_to_stent_offset,
                                           SizeF overlap,
                                           SizeF captured_image_size,
                                           Byte stents_in_row,
                                           Byte stents_in_columnn,
                                           String target_file_base_name,
                                           String target_files_directory,Single speed, Single acceleration,IntPtr VideoHandle,
                                           Boolean clean_directory_previous_results = true
                                                    )
            : this()
        {
            m_ActionName = inActionName;
            m_AxesIDs = axis_ids;
            m_AxesStartPositions = axes_start_positions;
            m_FirstStentRelativeCoordinates = first_stent_relative_coordinates;
            m_StentToStentOffset = stent_to_stent_offset;
            m_Overlap = overlap; 
            m_CapturedImageSize = captured_image_size;
            m_StentsInRow = stents_in_row;
            m_StentsInColumn = stents_in_columnn;
            m_TargetFileBaseName = target_file_base_name;
            m_TargetFilesDirectory = target_files_directory;
            m_CleanDirectoryPreviousResults = clean_directory_previous_results;
            m_Spd = speed;
            m_acc = acceleration;
            m_VidHandle = VideoHandle;
        }

        #endregion Constructors

        #region Public properties

        [XmlIgnore]
        private AutoResetEvent _VideoSync;
        [XmlIgnore]
        public AutoResetEvent VideoSync
        {
            get { return _VideoSync; }
            set { _VideoSync = value; }
        }

        [XmlIgnore]
        private AutoResetEvent _MotionSync;
        [XmlIgnore]
        public AutoResetEvent MotionSync
        {
            get { return _MotionSync; }
            set { _MotionSync = value; }
        }

        [XmlIgnore]
        public string Name
        {
            get { return m_ActionName; }
        }

        [XmlIgnore]
        private Byte[] m_AxesIDs;
        /// <summary>
        /// Specify the list of involved axes ID's to move
        /// </summary>
        [XmlElement("AxesIDs")]
        public Byte[] AxesIDs
        {
            get { return m_AxesIDs; }
            set { m_AxesIDs = value; }
        }

        [XmlIgnore]
        private PointF  m_AxesStartPositions;
        /// <summary>
        /// Specify the axes start positions(Zero Point) based on AxesIDs Order
        /// </summary>
        [XmlElement("AxesStartPositions")]
        public PointF AxesStartPositions
        {
            get { return m_AxesStartPositions; }
            set { m_AxesStartPositions = value; }
        }

        [XmlIgnore]
        private RectangleF m_FirstStentRelativeCoordinates;
        /// <summary>
        /// The list of coordinates based on AxesIDs order of TopLeft Point and Bottom Right Point of a single stent.
        /// </summary>
        [XmlElement("FirstStentRelativeCoordinates")]
        public RectangleF FirstStentRelativeCoordinates
        {
            get { return m_FirstStentRelativeCoordinates; }
            set 
            {
                //if (value != null)
                //{  
                    m_FirstStentRelativeCoordinates = value;
                //}
            }
        }

        [XmlIgnore]
        private PointF m_StentToStentOffset;
        [XmlElement("StentToStentOffset")]
        public PointF StentToStentOffset
        {
            get { return m_StentToStentOffset; }
            set { m_StentToStentOffset = value; }
        }

        [XmlIgnore]
        private SizeF m_Overlap;
        [XmlElement("Overlap")]
        public SizeF Overlap
        {
            get { return m_Overlap; }
            set { m_Overlap = value; }
        }


        [XmlIgnore]
        private System.Drawing.SizeF m_CapturedImageSize;
        /// <summary>
        /// Get/Set the each image capture size
        /// </summary>
        [XmlElement("CapturedImageSize")]
        public System.Drawing.SizeF CapturedImageSize
        {
            get { return m_CapturedImageSize; }
            set { m_CapturedImageSize = value; }
        }

        [XmlIgnore]
        private Byte m_StentsInRow;
        /// <summary>
        /// Panel description, how many stents in row
        /// </summary>
        [XmlElement("StentsInRow")]
        public Byte StentsInRow
        {
            get { return m_StentsInRow; }
            set { m_StentsInRow = value; }
        }

        [XmlIgnore]
        private Byte m_StentsInColumn;
        /// <summary>
        /// Panel description, how many stents in Column
        /// </summary>
        [XmlElement("StentsInColumn")]
        public Byte StentsInColumn
        {
            get { return m_StentsInColumn; }
            set { m_StentsInColumn = value; }
        }


        [XmlIgnore]
        private String  m_TargetFileBaseName;
        /// <summary>
        /// Get/Set the base file name, all new stored images will be saved with this name and running index
        /// </summary>
        [XmlElement("TargetFileBaseName")]
        public String  TargetFileBaseName
        {
            get { return m_TargetFileBaseName; }
            set { m_TargetFileBaseName = value; }
        }

        [XmlIgnore]
        private String m_TargetFilesDirectory;
        /// <summary>
        /// Get/Set the target directory where images will be stored.
        /// </summary>
        [XmlElement("TargetFilesDirectory")]
        public String TargetFilesDirectory
        {
            get { return m_TargetFilesDirectory; }
            set { m_TargetFilesDirectory = value; }
        }
        
        [XmlIgnore]
        private Boolean m_CleanDirectoryPreviousResults = false;
        /// <summary>
        /// Get/Set the parameter that indicate if action should delete results files
        /// of previous tests
        /// </summary>
        [XmlElement("CleanDirectoryPreviousResults")]
        public Boolean CleanDirectoryPreviousResults
        {
            get { return m_CleanDirectoryPreviousResults; }
            set { m_CleanDirectoryPreviousResults = value; }
        }
        
        #endregion

        #region Public overridden functions

        public override short SetMyDevices(Dictionary<string, IComponent> DeviceList, out String message)
        {
            message = "";
            Boolean bZaberLogicFound = false, bZaberListenerFound = false, bVideoControllerLogicFound = false,
                bRoutineControllerLogicFound = false; ;
             
            if (DeviceList.ContainsKey("ZaberControllerLogic"))
            {
                m_ZaberMotionControllerLogic = ((ZaberControllerLogic)DeviceList["ZaberControllerLogic"]);
                EventGroupReady += m_ZaberMotionControllerLogic.OnProcessEventGroup;
                bZaberLogicFound = true;
            }

             if (DeviceList.ContainsKey("ZaberControllerListener"))
            {
                m_ZaberMotionControllerListener = ((ZaberControllerListener)DeviceList["ZaberControllerListener"]);
                m_ZaberMotionControllerListener.EventGroupReady += this.OnProcessEventGroup;
                bZaberListenerFound = true;
            }

            if (DeviceList.ContainsKey("VideoControllerLogic"))
            {
                m_VideoControllerLogic = ((VideoControllerLogic)DeviceList["VideoControllerLogic"]);
                EventGroupReady += m_VideoControllerLogic.OnProcessEventGroup;
                bVideoControllerLogicFound = true;
            }

            if (DeviceList.ContainsKey("RoutineFactoryLogic"))
            {
                RoutineLogic m_RoutineControllerLogic = ((RoutineLogic)DeviceList["RoutineFactoryLogic"]);
                EventGroupReady += m_RoutineControllerLogic.OnProcessEventGroup;
                bRoutineControllerLogicFound = true;
            }

             

            if (true == bZaberLogicFound && bZaberListenerFound == true && bVideoControllerLogicFound == true &&
                true == bRoutineControllerLogicFound)
                return NS_Common.ErrorCodesList.OK;
            else
            {
                if (!bZaberLogicFound)
                    message = "If you want to use 'AVIActionCapturePanelImages' class, you have to pass 'NS_Zaber_Motion_Controller.ZaberLogic' to routine controller";
                if (!bRoutineControllerLogicFound)
                {
                    if (message != "")
                        message += "\n\r";
                    message = "If you want to use 'AVIActionCapturePanelImages' class, you have to pass 'RoutineFactoryLogic' to routine controller";
                }

                if (!bZaberListenerFound)
                {
                    if (message != "")
                        message += "\n\r";
                    message = "If you want to use 'AVIActionCapturePanelImages' class, you have to pass 'NS_Zaber_Motion_Controller.ZaberListener' to routine controller";
                }
                if (!bVideoControllerLogicFound)
                {
                    if (message != "")
                        message += "\n\r";
                    message = "If you want to use 'AVIActionCapturePanelImages' class, you have to pass 'VideoControllerLogic' to routine controller";
                }
                return NS_Common.ErrorCodesList.FAILED;
            }
        }

        public override short RemoveMyDevices(out string error_message)
        {
            error_message = "";
            m_ZaberMotionControllerLogic = null;
            m_ZaberMotionControllerListener = null;
            m_VideoControllerLogic = null;
            return NS_Common.ErrorCodesList.OK;
        }

        public override bool StopActionPerformance()
        {
            lock (m_SynchronizationObject) //myIOLogic)
            {
                if (null != m_SynchronizationObject)
                {
                    m_ThreadCancellationSource.Cancel();

                    System.Threading.Thread.Sleep(500);
                }

                SendEvent(new ZaberCommandEvent(ZaberCommandEventTypes.CT_STOP_AXES_MOTION, new ZaberCommandPrmStopAxesMotion(m_AxesIDs)));

            }

            return true;

        }

        public override string GetShortDescription()
        {
            return "AVI IV Capture Panel";
        }

        public override string GetFullDescription()
        {
            return "AVI IV Move stages and capture Panel images based on specified offset parameters";
        }
        
        #endregion

        #region IEventGroupConsumer Members


        public void OnProcessEventGroup(object sender, EventGroupData data)
        {
            foreach (IEventData singleEvent in data)
            {
                if (singleEvent != null)
                {
                    if (m_Handlers == null || !m_Handlers.ContainsKey(singleEvent.GetType()))
                    {
                        //throw new NullReferenceException("No handler mapped to this type of event " + singleEvent.GetType());
                    }
                    else
                    {
                        m_Handlers[singleEvent.GetType()](singleEvent);
                    }
                }
            }
        }

        public void MapHandlers()
        {
            m_Handlers.TryAdd(typeof(ZaberDataEvent), HandleZaberDataEvent);
        }

        public void ClearHandlers()
        {
            Action<IEventData> res;
            if (null != m_Handlers)
                m_Handlers.TryRemove(typeof(ZaberDataEvent), out res);
        }

        private void HandleZaberDataEvent(IEventData data)
        {
            switch (((ZaberDataEvent)data).DataType)
            {
                case ZaberDataEventType.ET_DEVICE_MOTION_COMPLETE:
                    {
                       
                        //TargetPositionReached((TMCDataPrmTargetPositionReached)((TMCDataEvent)data).DataParameters);
                        break;
                    }
            }
        }

        #endregion IEventGroupConsumer Members

        /// <summary>
        /// Action performance main thread
        /// </summary>
        protected override void ThreadFunction(object token)
        {
            Boolean bsucceded = true;
            try
            {
                SendEvent(new VideoCommandEvent(VideoCommandEventTypes.CT_STOP_GRABBING, new VideoCommandEventParameters(NS_Video_Controller_Common.Service.CameraIndexEnum.First)));
                
                m_ZaberMotionControllerLogic.ErrorStatus = NS_Common.ErrorCodesList.OK;
                m_ActionPerforming = true;


                //Calculate how many images need to be captured for each stent
                int iNumberOfImagesPerStentWidth = (int)Math.Round(m_FirstStentRelativeCoordinates.Width / (m_CapturedImageSize.Width - m_Overlap.Width) + 0.5, 0);
                int iNumberOfImagesPerStentHeight = (int)Math.Round(m_FirstStentRelativeCoordinates.Height / (m_CapturedImageSize.Height - m_Overlap.Height) + 0.5, 0);

                SendEvent(new RCCommandEvent(RCCommandType.ET_SEND_UPDATE_MESSAGE, new RCCommandPrmSendMessage(String.Format("Num of Images:{0}", (iNumberOfImagesPerStentHeight * iNumberOfImagesPerStentWidth)))));
                SendEvent(new RCCommandEvent(RCCommandType.ET_SEND_UPDATE_MESSAGE, new RCCommandPrmSendMessage(String.Format("Num of Stents:{0}", (m_StentsInRow * m_StentsInColumn)))));


                


                Single StentStartToNextStentXOffset  = m_FirstStentRelativeCoordinates.X + m_StentToStentOffset.X /* +m_FirstStentRelativeCoordinates.Width*/ ;
                Single StentStartToNextStentYOffset  = m_FirstStentRelativeCoordinates.Y +  m_StentToStentOffset.Y /* +m_FirstStentRelativeCoordinates.Height*/ ;


                for (int i = 0; i < m_StentsInRow; i++) //Rows
                {   
                    
                    Single stent_start_x_position = m_AxesStartPositions.X + m_FirstStentRelativeCoordinates.X - m_StentToStentOffset.X * i;
                  
                    for (int j = 0; j < m_StentsInColumn; j++) //Columns
                    {
                        
                        Single stent_start_y_pos = m_AxesStartPositions.Y + m_FirstStentRelativeCoordinates.Y + m_StentToStentOffset.Y * j;
                        //Move to Nest Stent in the row
                        if (m_AxesIDs.Length > 0)
                            SendEvent(new ZaberCommandEvent(ZaberCommandEventTypes.CT_SYNC_MOVE_AXES_ABSOLUTE,
                                                    new ZaberCommandPrmMoveAxesAbsMM(m_AxesIDs,
                                                                                                 new Single[] { stent_start_x_position, stent_start_y_pos },
                                                                                                 new Single[] { m_Spd, m_Spd },
                                                                                                 new Single[] { m_acc, m_acc },
                                                                                 (CancellationToken)token)));
                        SendEvent(new RCCommandEvent(RCCommandType.ET_SEND_UPDATE_MESSAGE, new RCCommandPrmSendMessage("Panel Start - Row:" + j.ToString() + ",Col:" + i.ToString())));
                        CaptureSingleStent(i * m_StentsInColumn + j,                      
                                           stent_start_x_position,
                                           stent_start_y_pos,
                                           m_FirstStentRelativeCoordinates.Width / iNumberOfImagesPerStentWidth,
                                           m_FirstStentRelativeCoordinates.Height / iNumberOfImagesPerStentHeight,
                                           iNumberOfImagesPerStentWidth,
                                           iNumberOfImagesPerStentHeight,
                                           (CancellationToken)token);
                        SendEvent(new RCCommandEvent(RCCommandType.ET_SEND_UPDATE_MESSAGE, new RCCommandPrmSendMessage("Panel End - Row:" + j.ToString() + ",Col:" + i.ToString())));
                        
                        if (((CancellationToken)token).IsCancellationRequested) return;
                    }
                    if (((CancellationToken)token).IsCancellationRequested) return;
                }

                SendEvent(new VideoCommandEvent(VideoCommandEventTypes.CT_START_GRABBING, new VideoCommandPrmSetDisplayWindow(m_VidHandle, NS_Video_Controller_Common.Service.CameraIndexEnum.First)));

                if (NS_Common.ErrorCodesList.OK != m_ZaberMotionControllerLogic.ErrorStatus)
                {
                    m_ActionPerforming = false;
                    String error_message = "Failed to start Motion action " + m_ActionName + " Axis " + m_AxesIDs + "\n\r" + m_ZaberMotionControllerLogic.ErrorMessage;
                    //if (m_SystemLog.IsErrorEnabled)
                    //    m_SystemLog.Error(error_message);
                    OnFailedDuringActionPerformance(this, NS_Common.ErrorCodesList.FAILED_TO_REACH_TARGET_POSITION, error_message);
                    bsucceded = false;
                }
            }
            catch (ThreadAbortException exc)
            {
                m_ActionPerforming = false;
                if (m_SystemLog.IsDebugEnabled)
                    m_SystemLog.Debug("AVI IV Action thread aborting, code is " + exc.ExceptionState);
                bsucceded = false;
            }
            catch (Exception ex)
            {
                m_ActionPerforming = false;
                OnFailedDuringActionPerformance(this, NS_Common.ErrorCodesList.FAILED, ex.Message);
                bsucceded = false;
            }
            finally
            {
                if (bsucceded)
                {
                    m_ActionPerforming = false;
                    OnActionCompleted(this);
                }
            }

        }

        private void CaptureSingleStent(int current_stent_running_number, 
                                        float stent_start_x_pos, float stent_start_y_pos,
                                        float frame_offset_width, float frame_offset_height,
                                        int iNumberOfImagesPerStentWidth, int iNumberOfImagesPerStentHeight,
                                        CancellationToken token    )
        {
            for (int i = 0; i < iNumberOfImagesPerStentHeight; i++)
            {
                if (token.IsCancellationRequested) return;

                SendEvent(new ZaberCommandEvent(ZaberCommandEventTypes.CT_SYNC_MOVE_AXES_ABSOLUTE,
                                               new ZaberCommandPrmMoveAxesAbsMM(m_AxesIDs,
                                                                                new Single[] { stent_start_x_pos, stent_start_y_pos + frame_offset_height * i },
                                                                                 new Single[] { m_Spd, m_Spd },
                                                                                 new Single[] { m_acc, m_acc },
                                                                                 (CancellationToken)token)));

                for (int j = 0; j < iNumberOfImagesPerStentWidth; j++)
                {
                   
                   
                    
                    SendEvent(new ZaberCommandEvent(ZaberCommandEventTypes.CT_SYNC_MOVE_AXIS_ABSOLUTE,
                                               new ZaberCommandPrmMoveAbsMM(m_AxesIDs[0],
                                                                              stent_start_x_pos - frame_offset_width * j,
                                                                               m_Spd,
                                                                                m_acc,
                                                                                (CancellationToken)token)));
                    if (token.IsCancellationRequested) return;

                    SendEvent(new RCCommandEvent(RCCommandType.ET_SEND_UPDATE_MESSAGE, new RCCommandPrmSendMessage("Stent Update - Image:" + (i * iNumberOfImagesPerStentHeight + j).ToString())));
                    if (_MotionSync != null)
                        _MotionSync.WaitOne();

                    String file_name = String.Format("{0}_{1} ({2}-{3}).png", m_TargetFileBaseName, current_stent_running_number, i, j);
                    SendEvent(new VideoCommandEvent(VideoCommandEventTypes.CT_CAPTURE_AND_SAVE_IMAGE,
                                                        new  VideoCommandPrmCaptureImage( NS_Video_Controller_Common.Service.CameraIndexEnum.First  , 
                                                                                        m_TargetFilesDirectory,
                                                                                        file_name)));
                    
                    if(_VideoSync!=null)
                        _VideoSync.WaitOne();
                    if (token.IsCancellationRequested) return;
                }
            }
        }




        public Single m_Spd { get; set; }

        public Single m_acc { get; set; }

        public IntPtr m_VidHandle { get; set; }
    }

   

}
