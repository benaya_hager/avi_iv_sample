﻿
using SFW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using StartUp.Routine_Definitions;
using NS_Routines_Controller_Common.Interfaces;
using NS_Video_Controller_Common.Interfaces;
using Zaber_Common.Interfaces;
using StartUp;
using Routines_Controller_UI;
using VideoControllerSimulator;
using Zaber_Controller_UI;
using StartUp.ViewModels;
using System.Windows.Forms;
using StartUp.ViewModels;
using System.Threading;
using NS_VideoControllerSimulator;

namespace Startup
{
    public class ComponentBuilder : StartUp.IComponentBuilder
    {
        private const string GlobalContainerKey = "Your global Unity container";
        Dictionary<string, SFW.IComponent> m_Components;
        AutoResetEvent VideoSync = new AutoResetEvent(false);
        
        public IUnityContainer m_Container { set; get; }
        

        public ComponentBuilder(IUnityContainer container)
        {
            m_Container = container;
            m_Components = new Dictionary<string, IComponent>();
        }

        public virtual void Build()
        {
            
            RegisterComponents();

            CreateComponents();
                     
        }

        public void CreateComponents()
        {
                                
            FillLocalComponentsList();


            #region Video

                m_Components.Add("frmImageDisplay", new frmImageDisplay("frmImageDisplay", NS_Video_Controller_Common.Service.CameraIndexEnum.First, false));

                ((frmImageDisplay)m_Components["frmImageDisplay"]).FormClosing += new System.Windows.Forms.FormClosingEventHandler(CB_FormClosing);

                ((frmImageDisplay)m_Components["frmImageDisplay"]).EventGroupReady += ((Logic)m_Components["VideoControllerLogic"]).OnProcessEventGroup;

                ((Listener)m_Components["VideoControllerListener"]).EventGroupReady += ((frmImageDisplay)m_Components["frmImageDisplay"]).OnProcessEventGroup;

            #endregion

            #region WPF controls
                m_Components.Add("CapturingRoutinePanelViewModel", (SFW.IComponent)m_Container.Resolve<ICapturingRoutinePanelViewModel>());
                m_Components.Add("X_AxisControlViewModel", (SFW.IComponent)m_Container.Resolve<IAxisControlViewModel>());
                m_Components.Add("Y_AxisControlViewModel", (SFW.IComponent)m_Container.Resolve<IAxisControlViewModel>());
                m_Components.Add("VideoControlViewModel", (SFW.IComponent)m_Container.Resolve<IVideoControlViewModel>());
                m_Components.Add("LotDataControlViewModel", (SFW.IComponent)m_Container.Resolve<ILotDataControlViewModel>());
                m_Components.Add("RoutineProgressViewModel", (SFW.IComponent)m_Container.Resolve<IRoutineProgressViewModel>());


               // ((LotDataControlViewModel)m_Components["LotDataControlViewModel"]).VideoSync = VideoSync;


               // m_Components.Add("ActionSyncManager", (SFW.IComponent)m_Container.Resolve<IActionSyncManager>());

                ((AxisControlViewModel)m_Components["X_AxisControlViewModel"]).MyAxisID = 1;
                ((AxisControlViewModel)m_Components["Y_AxisControlViewModel"]).MyAxisID = 2;


                ((CapturingRoutinePanelViewModel)m_Components["CapturingRoutinePanelViewModel"]).X_Axis = ((AxisControlViewModel)m_Components["X_AxisControlViewModel"]);
                ((CapturingRoutinePanelViewModel)m_Components["CapturingRoutinePanelViewModel"]).Y_Axis = ((AxisControlViewModel)m_Components["Y_AxisControlViewModel"]);
                ((CapturingRoutinePanelViewModel)m_Components["CapturingRoutinePanelViewModel"]).Vid = ((VideoControlViewModel)m_Components["VideoControlViewModel"]);
                ((CapturingRoutinePanelViewModel)m_Components["CapturingRoutinePanelViewModel"]).Lot_Data = ((LotDataControlViewModel)m_Components["LotDataControlViewModel"]);
                ((CapturingRoutinePanelViewModel)m_Components["CapturingRoutinePanelViewModel"]).Rprogress = ((RoutineProgressViewModel)m_Components["RoutineProgressViewModel"]);

                ((VideoControlViewModel)m_Components["VideoControlViewModel"]).VideoForm = ((Form)m_Components["frmImageDisplay"]);
                ((LotDataControlViewModel)m_Components["LotDataControlViewModel"]).VideoHandle = ((Form)m_Components["frmImageDisplay"]).Handle;
                ((LotDataControlViewModel)m_Components["LotDataControlViewModel"]).EventGroupReady += ((Logic)m_Components["VideoControllerLogic"]).OnProcessEventGroup;

                ((LotDataControlViewModel)m_Components["LotDataControlViewModel"]).EventGroupReady += ((Logic)m_Components["RoutineFactoryLogic"]).OnProcessEventGroup;
                ((RoutineProgressViewModel)m_Components["RoutineProgressViewModel"]).EventGroupReady += ((Logic)m_Components["RoutineFactoryLogic"]).OnProcessEventGroup;



                ((Listener)m_Components["RoutineControllerListener"]).EventGroupReady += ((LotDataControlViewModel)m_Components["LotDataControlViewModel"]).OnProcessEventGroup;
                ((Listener)m_Components["RoutineControllerListener"]).EventGroupReady += ((RoutineProgressViewModel)m_Components["RoutineProgressViewModel"]).OnProcessEventGroup;

                ((AxisControlViewModel)m_Components["X_AxisControlViewModel"]).EventGroupReady += ((Logic)m_Components["ZaberControllerLogic"]).OnProcessEventGroup;
                ((AxisControlViewModel)m_Components["Y_AxisControlViewModel"]).EventGroupReady += ((Logic)m_Components["ZaberControllerLogic"]).OnProcessEventGroup;



                ((Listener)m_Components["ZaberControllerListener"]).EventGroupReady += ((AxisControlViewModel)m_Components["X_AxisControlViewModel"]).OnProcessEventGroup;
                ((Listener)m_Components["ZaberControllerListener"]).EventGroupReady += ((AxisControlViewModel)m_Components["Y_AxisControlViewModel"]).OnProcessEventGroup;
                ((Listener)m_Components["ZaberControllerListener"]).EventGroupReady += ((LotDataControlViewModel)m_Components["LotDataControlViewModel"]).OnProcessEventGroup;

            #endregion

            #region Fill Routine Controller objects

            ((IRoutinesController)(m_Components["RoutinesController"])).SetDeviceList(m_Components);

            #endregion Fill Routine Contoller objects


        }

        public void RegisterComponents()
        {
            m_Container.RegisterType<IRoutinesControllerComponentsBuilder, RoutinesControllerComponentsBuilder>("RoutinesControllerComponentsBuilder",
                                                                                new InjectionConstructor(m_Container));
            m_Container.RegisterType<IZaberControllerComponentsBuilder, ZaberControllerComponentsBuilder>("ZaberControllerComponentsBuilder",
                                                                                new InjectionConstructor(m_Container));
            m_Container.RegisterType<IVideoControllerComponentsBuilder, VideoControllerComponentsBuilder>("VideoControllerComponentsBuilder",
                                                                                new InjectionConstructor(m_Container));

            IVideoControllerComponentsBuilder videoBuilder;
            IRoutinesControllerComponentsBuilder routinesBuilder;
            IZaberControllerComponentsBuilder zaiberBilder;

            routinesBuilder = m_Container.Resolve<IRoutinesControllerComponentsBuilder>("RoutinesControllerComponentsBuilder");
            m_Container.RegisterInstance<IRoutinesControllerComponentsBuilder>(routinesBuilder);

            zaiberBilder = m_Container.Resolve<IZaberControllerComponentsBuilder>("ZaberControllerComponentsBuilder");
            m_Container.RegisterInstance<IZaberControllerComponentsBuilder>(zaiberBilder);

            videoBuilder = m_Container.Resolve<IVideoControllerComponentsBuilder>("VideoControllerComponentsBuilder");
            m_Container.RegisterInstance<IVideoControllerComponentsBuilder>(videoBuilder);
        }

        public string Name
        {
            get { return "StartUp Components Builder"; }
        }


        public virtual void Release()
        {
            ReleaseComponents();
        }


        public virtual void ReleaseComponents() { }

        private void FillLocalComponentsList()
        {
            ResolveRoutineControllerObjects();
            ResolveZaberControllerObjects();
            ResolveVideoControllerObjects();
        }


        public Dictionary<string, SFW.IComponent> ComponentsList
        {
            get { return m_Components; }
        }

        private void ResolveVideoControllerObjects()
        {
            lock (m_Components)
            {
                //m_Components.Add("Camera", (IInputDevice)m_Container.Resolve<IMil9BaseCamera>());

                m_Components.Add("VideoControllerListener", (Listener)m_Container.Resolve<IVideoControllerListener>());

                m_Components.Add("VideoControllerLogic", (Logic)m_Container.Resolve<IVideoControllerLogic>());

                

            }
        }

        private void ResolveZaberControllerObjects()
        {
            lock (m_Components)
            {
                m_Components.Add("ZaberMotionController", (IIODevice)m_Container.Resolve<IZaberMotionController>());

                m_Components.Add("ZaberControllerListener", (Listener)m_Container.Resolve<IZaberControllerListener>());

                m_Components.Add("ZaberControllerLogic", (Logic)m_Container.Resolve<IZaberControllerLogic>());
            }
        }

        private void ResolveRoutineControllerObjects()
        {
            lock (m_Components)
            {
                m_Components.Add("RoutinesController", (IIODevice)m_Container.Resolve<IRoutinesController>());

                m_Components.Add("RoutineFactoryLogic", (Logic)m_Container.Resolve<IRoutineControllerLogic>());

                m_Components.Add("RoutineControllerListener", (Listener)m_Container.Resolve<IRoutineControllerListener>());
            }
        }

        protected void CB_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            if (e.Cancel == true) return;

            System.Reflection.PropertyInfo pi = sender.GetType().GetProperty("MyComponentName");
            if (null != pi)
            {
                String name = (String)pi.GetValue(sender, null);

                System.Reflection.MethodInfo mi = sender.GetType().GetMethod("ReleaseChildWindows");
                if (mi != null)
                    mi.Invoke(sender, null);
                if (null != name && "" != name)
                    m_Components.Remove(name);
            }
        }



    }
}
