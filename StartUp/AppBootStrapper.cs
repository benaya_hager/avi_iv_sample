﻿using AVI4.Components.Services;
using Caliburn.Micro;
using Caliburn.Micro.ExposedProperties;

using StartUp.ViewModels;
using Microsoft.Practices.Unity;
using Microsoft.Win32;
using PriorityArsDispatcher;
using SFW;
using StartUp;
using StartUp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;


namespace Startup
{
    /// <summary>
    /// The application bootstrapper  
    /// </summary>
    public class AppBootstrapper : BootstrapperBase
    {
        Microsoft.Practices.Unity.UnityContainer container;
        ComponentBuilder builder;
        

        private const string ROOT_REGISTRY_AVI4 = "HKEY_LOCAL_MACHINE\\SOFTWARE\\Medinol\\AVI4";

        public AppBootstrapper()
        {
            Initialize();
          

           
        }

        protected override void Configure()
        {
            ViewModelBinder.BindProperties = ExposedPropertyBinder.BindProperties;

            container = new UnityContainer();

            container.RegisterInstance<IUnityContainer>(container);

            if (!container.IsRegistered<IWindowManager>())
            {
                container.RegisterInstance<IWindowManager>(new WindowManager());
            }

            if (!container.IsRegistered<IEventAggregator>())
            {
                container.RegisterInstance<IEventAggregator>(new EventAggregator());
            }

            if (!container.IsRegistered<StartUp.IComponentBuilder>())
            {
                container.RegisterInstance<StartUp.IComponentBuilder>(new ComponentBuilder(container));
            }

            if (!container.IsRegistered<ICapturingRoutinePanelViewModel>())
            {
                container.RegisterInstance<ICapturingRoutinePanelViewModel>(new CapturingRoutinePanelViewModel());
            }

            if (!container.IsRegistered<IFamiliesManager>())
            {
                container.RegisterInstance<IFamiliesManager>(new FamiliesManager());

            }

            if (!container.IsRegistered<IActionSyncManager>())
            {
                container.RegisterInstance<IActionSyncManager>(new ActionSyncManager());

            }

           
            //container.RegisterType<IActionSyncManager, ActionSyncManager>("ActionSyncManager"); 
            

            if (!container.IsRegistered<Func<IArsDispatcher>>())
            {
                string PRIORITY_CONNECTION_STRING = (string)Registry.GetValue(ROOT_REGISTRY_AVI4, "PRIORITY_DB_CONNECTION_STRING", @"");
                if (null == PRIORITY_CONNECTION_STRING) Registry.SetValue(ROOT_REGISTRY_AVI4, "PRIORITY_DB_CONNECTION_STRING", @"Data Source=JER-PR15\PRI;Initial Catalog=med1;Persist Security Info=True;User ID=sa;Password=Manager1");

                container.RegisterInstance<IArsDispatcher>(new ARSDispatcher(PRIORITY_CONNECTION_STRING));
            }

           

            container.RegisterType<IAxisControlViewModel, AxisControlViewModel>();
            container.RegisterType<IVideoControlViewModel, VideoControlViewModel>();
            container.RegisterType<ILotDataControlViewModel, LotDataControlViewModel>();
            container.RegisterType<IRoutineProgressViewModel, RoutineProgressViewModel>();
            
            
            builder = (Startup.ComponentBuilder)container.Resolve<StartUp.IComponentBuilder>();
            
            ViewLocator.AddNamespaceMapping("StartUp.ViewModels", "StartUp.ViewModels");

            base.Configure();
        }

        protected override object GetInstance(Type type, string name)
        {
            var result = default(object);

            if (name != null)
            {
                result = container.Resolve(type, name);
            }
            else
            {
                result = container.Resolve(type);
            }

            return result;
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return container.ResolveAll(service);
        }

        protected override void BuildUp(object instance)
        {
            instance = container.BuildUp(instance);
            base.BuildUp(instance);
        }

        protected override void OnStartup(object sender, System.Windows.StartupEventArgs e)
        {


            ((Startup.ComponentBuilder)builder).Build();

           

            DisplayRootViewFor<ICapturingRoutinePanelViewModel>();

        }

        protected override void OnExit(object sender, EventArgs e)
        {


            base.OnExit(sender, e);
        }
    }
}
