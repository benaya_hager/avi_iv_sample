﻿using System;
namespace StartUp
{
    public interface IComponentBuilder
    {
        void Build();
        System.Collections.Generic.Dictionary<string, SFW.IComponent> ComponentsList { get; }
        void CreateComponents();
        Microsoft.Practices.Unity.IUnityContainer m_Container { get; set; }
        string Name { get; }
        void RegisterComponents();
        void Release();
        void ReleaseComponents();
    }
}
