﻿using StartUp.ViewModels;
using NS_Routines_Controller_Common.Events.Routines_Controller;
using NS_Routines_Controller_Common.Events.Routines_Controller.RCDataEventParameters;
using SFW;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StartUp.ViewModels
{
    public class RoutineProgressViewModel : Caliburn.Micro.Screen, IRoutineProgressViewModel, IEventGroupPublisher, SFW.IEventGroupConsumer, SFW.IComponent
    {
        private ConcurrentDictionary<System.Type, Action<IEventData>> m_Handlers;
        public event EventGroupHandler EventGroupReady;
        private int BASE_CHAR_CODE;


        #region Ctor

        public RoutineProgressViewModel()
        {
            m_Handlers = new System.Collections.Concurrent.ConcurrentDictionary<Type, Action<IEventData>>();
            MapHandlers();
            BASE_CHAR_CODE = (int)get_char_code('A');
            NumOfStentsPerPanel = 1;
            NumOfImagesPerStent = 1;
            CurrentImage = 0;
            CurrentCollumn = -1;
            CurrentRow = 0;
            PanelProgressValue = 0;
        }

        #endregion


        #region Props


        private int _PanelProgressValue;

        public int PanelProgressValue
        {
            get { return _PanelProgressValue; }
            set { _PanelProgressValue = value; NotifyOfPropertyChange(() => PanelProgressValue); }
        }

       

        private int _NumOfImagesPerStent;

        public int NumOfImagesPerStent
        {
            get { return _NumOfImagesPerStent; }
            set { _NumOfImagesPerStent = value; NotifyOfPropertyChange(() => NumOfImagesPerStent); }
        }

        private int _NumOfStentsPerPanel;

        public int NumOfStentsPerPanel
        {
            get { return _NumOfStentsPerPanel; }
            set { _NumOfStentsPerPanel = value; NotifyOfPropertyChange(() => NumOfStentsPerPanel); }
        }
        
        private int _currentRow;

        public int CurrentRow
        {
            get { return _currentRow; }
            set
            {
                _currentRow = value;
                //if (_currentCollumn == -1)
                //    CurrentStentName = "";
                //else
                //CurrentStentName = char.ConvertFromUtf32(BASE_CHAR_CODE + _currentCollumn).ToString() + (_currentRow + 1).ToString();
                if (CurrentCollumn == -1 && CurrentRow <= 0)
                    PanelProgressValue = 1;
                else
                    PanelProgressValue++;
                NotifyOfPropertyChange(() => CurrentRow);
            }
        }

        private int _currentCollumn;

        public int CurrentCollumn
        {
            get { return _currentCollumn; }
            set { _currentCollumn = value;
                                   
                NotifyOfPropertyChange(() => CurrentCollumn); }
        }

        private string _routineProgressString;

        public string RoutineProgressString
        {
            get { return _routineProgressString; }
            set { _routineProgressString = value;

            if (value.StartsWith("Panel End"))
                {
                    lock (this)
                    {
                        string[] s = _routineProgressString.Split(',');
                        string[] ss = s[0].Split(':');
                        CurrentRow = Convert.ToInt32(ss[1]);
                        ss = s[1].Split(':');
                        CurrentCollumn = Convert.ToInt32(ss[1]);
                    }
                    
                }
            else if (value.StartsWith("Panel Start"))
            {
                lock (this)
                {
                    string[] s = _routineProgressString.Split(',');
                    string[] ss = s[0].Split(':');
                   int TempRow = Convert.ToInt32(ss[1]);
                    ss = s[1].Split(':');
                    int TempCollumn = Convert.ToInt32(ss[1]);
                    CurrentStentName = "Stent: " + char.ConvertFromUtf32(BASE_CHAR_CODE + TempCollumn).ToString() + (TempRow + 1).ToString();
                }

            }
                else if (value.StartsWith("Stent"))
                {
                    lock (this)
                    {
                        string[] s = _routineProgressString.Split(',');
                        string[] ss = s[0].Split(':');
                        CurrentImage = Convert.ToInt32(ss[1])+1;
                        CurrentImageText = string.Format("Image: {0}", _CurrentImage);
                    }
                }
                else if (value.StartsWith("Num of Images"))
                {
                    lock (this)
                    {
                        string[] s = _routineProgressString.Split(':');
                        NumOfImagesPerStent = Convert.ToInt32(s[1]);
                    }
                }
                else if (value.StartsWith("Num of Stents"))
                {
                    lock (this)
                    {
                        string[] s = _routineProgressString.Split(':');
                        NumOfStentsPerPanel = Convert.ToInt32(s[1]);
                    }
                }
                

                NotifyOfPropertyChange(() => RoutineProgressString); }
        }


        private string _CurrentStentName;

        public string CurrentStentName
        {
            get { return _CurrentStentName; }
            set { _CurrentStentName = value;
                NotifyOfPropertyChange(() => CurrentStentName); }
        }


        private int _CurrentImage;

        public int CurrentImage
        {
            get { return _CurrentImage; }
            set
            {
                _CurrentImage = value;
               
                    
                    NotifyOfPropertyChange(() => CurrentImage);
                
            }
        }

        private string _CurrentImageText;

        public string CurrentImageText
        {
            get { return _CurrentImageText; }
            set { _CurrentImageText = value; NotifyOfPropertyChange(() => CurrentImageText); }
        }
        
        

        #endregion


       

        #region SFW implementation

        public void RaiseEventGroup(EventGroupData data)
        {
            if (EventGroupReady != null)
                EventGroupReady(this, data);
        }

        public void RaiseSingleEvent(IEventData data)
        {
            throw new NotImplementedException();
        }

        public void OnProcessEventGroup(object sender, EventGroupData data)
        {
            foreach (IEventData singleEvent in data)
            {
                if (singleEvent != null)
                {
                    if (m_Handlers == null || !m_Handlers.ContainsKey(singleEvent.GetType()))
                    {

                    }
                    else
                    {
                        m_Handlers[singleEvent.GetType()](singleEvent);
                    }
                }
            }


        }

        public void MapHandlers()
        {
            m_Handlers.TryAdd(typeof(RCDataEvent), HandleRCDataEvent);
        }

        public void ClearHandlers()
        {
            Action<IEventData> res;
            if (null != m_Handlers)
            {
                m_Handlers.TryRemove(typeof(RCDataEvent), out res);
            }
        }

        public string Name
        {
            get { return "RoutineProgressView"; }
        }


        private void SendEvent(IEventData Event)
        {
            EventGroupData eventGroupData = new EventGroupData();
            eventGroupData.Enqueue(Event);

            if (EventGroupReady != null)
                EventGroupReady(this, eventGroupData);
        }

        private delegate void RCDataFormUpdateHandler(IEventData data);

        private void HandleRCDataEvent(IEventData data)
        {

            switch (((RCDataEvent)data).DataType)
            {

                case RCDataEventType.ET_NEW_ROUTINE_LOADED_TO_PERFORMANCE_QUEUE:
                    {
                        CurrentCollumn = -1;
                        CurrentRow = 0;
                        PanelProgressValue = 0;
                        CurrentImage = 0;
                        break;

                    }
                case RCDataEventType.ET_NEW_UPDATE_MESSAGE_RECEIVED:
                    {


                        RCDataPrmNewMessage d = (RCDataPrmNewMessage)((RCDataEvent)data).DataParameters;
                        RoutineProgressString = d.Message;
                        break;
                    }
            }

        }
        #endregion


        public int get_char_code(char character)
        {
            UTF32Encoding encoding = new UTF32Encoding();
            byte[] bytes = encoding.GetBytes(character.ToString().ToCharArray());
            return BitConverter.ToInt32(bytes, 0);
        } 

    }
}
