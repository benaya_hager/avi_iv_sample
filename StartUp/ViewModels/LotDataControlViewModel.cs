﻿using AVI4.Components.Services;
using NS_Routines_Controller_Common.Events.Routines_Controller;
using NS_Routines_Controller_Common.Events.Routines_Controller.RCComandEventsParameters;
using NS_Routines_Controller_Common.Events.Routines_Controller.RCDataEventParameters;
using NS_Routines_Controller_Common.Routine_Definitions.Base_Classes;
using NS_Video_Controller_Common.Events;
using NS_Video_Controller_Common.Events.NS_VideoComandEventsParameters;
using PriorityArsDispatcher;
using SFW;
using StartUp.Routine_Definitions.Actions.AVI_Special;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Tools.CustomRegistry;
using Zaber_Common.Events.Zaber;

namespace StartUp.ViewModels
{
    public class LotDataControlViewModel : Caliburn.Micro.Screen, ILotDataControlViewModel, IEventGroupPublisher, SFW.IEventGroupConsumer,  SFW.IComponent
    {

        #region Private Members
        private IArsDispatcher _arsDispatcher;
        private IFamiliesManager _familyManager;
        private IActionSyncManager _actionSyncManager;
        int m_RoutineIndex;
        Guid m_MyUniqID;
        
       
        private const string DEFAULT_CAPTURED_IMAGES_PATH = @"C:\Medinol\AVI IV\Captured Images\";
        private const string DEFAULT_CAPTURED_IMAGES_BASE_FILE_NAME = "Image";
        private const float DEFAULT_IMAGE_OVERLAP_X = 1;
        private const float DEFAULT_IMAGE_OVERLAP_Y = 1;
        private const float DEFAULT_IMAGE_SIZE_X = 4;
        private const float DEFAULT_IMAGE_SIZE_Y = 2.5F;
        private const float DEFAULT_SPEED = 10;
        private const float DEFAULT_ACCELERATION = 50;
        bool IsRoutineLoaded = false;
        private ConcurrentDictionary<System.Type, Action<IEventData>> m_Handlers;
        public event EventGroupHandler EventGroupReady;
        private RectangleF StentRelativeCoordinates;

        #endregion


        #region Properties


        private string _routineProgressMessage;

        public string RoutineProgressMessage
        {
            get { return _routineProgressMessage; }
            set { _routineProgressMessage = value; NotifyOfPropertyChange(() => RoutineProgressMessage); }
        }
        

        private string _lotNumber;

        public string LotNumberText
        {
            get { return _lotNumber; }
            set { _lotNumber = value; NotifyOfPropertyChange(() => LotNumberText); }
        }

        private string _catNumber;

        public string CatNumber
        {
            get { return _catNumber; }
            set { _catNumber = value; NotifyOfPropertyChange(() => CatNumber); }
        }

        private string _lotDescription;

        public string LotDescription
        {
            get { return _lotDescription; }
            set { _lotDescription = value; NotifyOfPropertyChange(() => LotDescription); }
        }
        

        private string _capturedImagesPath;

        public string CapturedImagesPath
        {
            get { return _capturedImagesPath; }
            set { _capturedImagesPath = value; }
        }

        private string _capturedImagesBaseFileName;

        public string CapturedImagesBaseFileName
        {
            get { return _capturedImagesBaseFileName; }
            set { _capturedImagesBaseFileName = value; }
        }

        private PointF _startPosition;

        public PointF StartPosition
        {
            get { return _startPosition; }
            set { _startPosition = value;
            StentRelativeCoordinates.X = _startPosition.X;
            StentRelativeCoordinates.Y = _startPosition.Y;
            EndPosition = _endPosition;
            }
        }

        private PointF _endPosition;

        public PointF EndPosition
        {
            get { return _endPosition; }
            set { _endPosition = value;
            StentRelativeCoordinates.Width = Math.Abs(_endPosition.X - StentRelativeCoordinates.X);
            StentRelativeCoordinates.Height = Math.Abs(_endPosition.Y - StentRelativeCoordinates.Y);
            }
        }

        private PointF _zeroPoint;

        public PointF ZeroPoint
        {
            get { return _zeroPoint; }
            set { _zeroPoint = value; }
        }
        

        private PointF _stentToStentOffset;

        public PointF StentToStentOffset
        {
            get { return _stentToStentOffset; }
            set { _stentToStentOffset = value; }
        }

        private SizeF _imageOverlap;

        public SizeF ImageOverlap
        {
            get { return _imageOverlap; }
            set { _imageOverlap = value; }
        }

        private SizeF _capturedImageSize;

        public SizeF CapturedImageSize
        {
            get { return _capturedImageSize; }
            set { _capturedImageSize = value; }
        }


        private byte _stentsInRow;

        public byte StentsInRow
        {
            get { return _stentsInRow; }
            set { _stentsInRow = value; }
        }


        private byte _stentsInCollumn;
       

        public byte StentsInCollumn
        {
            get { return _stentsInCollumn; }
            set { _stentsInCollumn = value; }
        }


        private AutoResetEvent _VideoSync;

        public AutoResetEvent VideoSync
        {
            get { return _VideoSync; }
            set { _VideoSync = value; }
        }

        private IntPtr _VideoHandle;

        public IntPtr VideoHandle
        {
            get { return _VideoHandle; }
            set { _VideoHandle = value; }
        }
        
        

        #endregion

        #region Ctor

        public LotDataControlViewModel(IArsDispatcher arsDispatcher, IFamiliesManager familyManager, IActionSyncManager actionSyncManager)
        {
            m_Handlers = new System.Collections.Concurrent.ConcurrentDictionary<Type, Action<IEventData>>();
            MapHandlers();
            m_MyUniqID = Guid.NewGuid();
            InitRegistryParameters();
            SetRoutineParameters(new PointF(194, 15F), new PointF(0,0), new PointF(20, 5), new PointF(38, 8F), 3, 6);
            _arsDispatcher = arsDispatcher;
            _familyManager = familyManager;
            _actionSyncManager = actionSyncManager;
           
        }

        #endregion

        #region SFW implementation

        public  void RaiseEventGroup(EventGroupData data)
        {
            if (EventGroupReady != null)
                EventGroupReady(this, data);
        }

        public void RaiseSingleEvent(IEventData data)
        {
            throw new NotImplementedException();
        }

        public void OnProcessEventGroup(object sender, EventGroupData data)
        {
            foreach (IEventData singleEvent in data)
            {
                if (singleEvent != null)
                {
                    if (m_Handlers == null || !m_Handlers.ContainsKey(singleEvent.GetType()))
                    {
                        
                    }
                    else
                    {
                        m_Handlers[singleEvent.GetType()](singleEvent);
                    }
                }
            }

            
        }

        public void MapHandlers()
        {
            m_Handlers.TryAdd(typeof(RCDataEvent), HandleRCDataEvent);
            m_Handlers.TryAdd(typeof(ZaberDataEvent), HandleZaberDataEvent);
        }

        public void ClearHandlers()
        {
            Action<IEventData> res;
            if (null != m_Handlers)
            {
                m_Handlers.TryRemove(typeof(RCDataEvent), out res);
                m_Handlers.TryRemove(typeof(ZaberDataEvent), out res);
            }
        }

        public string Name
        {
            get { return "LotDataControlView"; }
        }

        #endregion


        #region Routine Load and Run


        public bool CanRunRoutine
        {
            get { return IsRoutineLoaded; }
        }

        public void RunRoutine()
        {
           
            SendEvent(new VideoCommandEvent(VideoCommandEventTypes.CT_STOP_GRABBING, new VideoCommandEventParameters(NS_Video_Controller_Common.Service.CameraIndexEnum.First)));
            Thread.Sleep(500);
            SendEvent(new RCCommandEvent(RCCommandType.CT_START_ROUTINE_BY_INDEX, new RCCommandPrmStartRoutineByIndex(m_RoutineIndex)));   
           
        }

        public void StopRoutine()
        {
            SendEvent(new RCCommandEvent(RCCommandType.CT_STOP_ROUTINE, new RCCommandPrmStopRoutine(m_RoutineIndex)));
            SendEvent(new VideoCommandEvent(VideoCommandEventTypes.CT_START_GRABBING, new VideoCommandPrmSetDisplayWindow(_VideoHandle, NS_Video_Controller_Common.Service.CameraIndexEnum.First)));
        }

        public void SetRoutineParameters(PointF ZroPnt, PointF StrtPos, PointF EndPos, PointF BetweenStentOffset, byte StntsInRow,byte StntsInCollumn)
        {

            ZeroPoint = ZroPnt;
            StartPosition = StrtPos;
            EndPosition = EndPos;
            StentToStentOffset = BetweenStentOffset;
            StentsInRow = StntsInRow;
            StentsInCollumn = StntsInCollumn;

        }

        public void ClearLotData()
        {
            IsRoutineLoaded = false;
            LotNumberText = "";
            LotDescription = "";
            CatNumber = "";

        }

        public void LoadRoutine()
        {
            NS_Routines_Controller_Common.Routine_Definitions.Routines.Routine routine = new NS_Routines_Controller_Common.Routine_Definitions.Routines.Routine("Test Routine");
            NS_Routines_Controller_Common.Routine_Definitions.Steps.Step step = new NS_Routines_Controller_Common.Routine_Definitions.Steps.Step("capture step");

            //AVIActionCapturePanelImages action = new AVIActionCapturePanelImages("capture panel action",
            //                                                                     new byte[] { 1, 2 },
            //                                                                     new PointF(105, 3),
            //                                                                     new RectangleF(0, 0, 38, 8),
            //                                                                     new PointF(38, 8),
            //                                                                     new Size(1, 1),
            //                                                                     new Size(10, 4),
            //                                                                     3, 3,
            //                                                                     _capturedImagesBaseFileName, _capturedImagesPath, true);

            AVIActionCapturePanelImages action = new AVIActionCapturePanelImages("capture panel action",
                                                                                new byte[] { 1, 2 },
                                                                                _zeroPoint,
                                                                                StentRelativeCoordinates,
                                                                               _stentToStentOffset,
                                                                               _imageOverlap,
                                                                               _capturedImageSize,
                                                                                _stentsInRow,_stentsInCollumn,
                                                                                _capturedImagesBaseFileName, _capturedImagesPath, _Speed, _Acceleration,_VideoHandle, true);
            action.VideoSync = _actionSyncManager.GetSyncObject("VideoSync");
            action.MotionSync = _actionSyncManager.GetSyncObject("MotionSync");

            step.AddAction(action);
            routine.AddSingleStep(step, true);

            SendEvent(new RCCommandEvent(RCCommandType.CT_ADD_ROUTINE_TO_QUEUE, new RCCommandPrmAddRoutineToQueue(this, m_MyUniqID, routine)));
        }

        #endregion

        private void InitRegistryParameters()
        {

            cRegistryFunc objRegistry = new cRegistryFunc("Routines Controller",
                                                                "",
                                                          cRegistryFunc._REG_BASE_KEY_TYPES.HKEY_LOCAL_MACHINE);

            _capturedImagesPath = (objRegistry.GetRegKeyValue("", "Captured Image Path", DEFAULT_CAPTURED_IMAGES_PATH));
            objRegistry.SetRegKeyValue("", "Captured Image Path", _capturedImagesPath);

            _capturedImagesBaseFileName = (objRegistry.GetRegKeyValue("", "Captured Image Base File Name", DEFAULT_CAPTURED_IMAGES_BASE_FILE_NAME));
            objRegistry.SetRegKeyValue("", "Captured Image Base File Name", _capturedImagesBaseFileName);

            float OverlapX = (float)(objRegistry.GetRegKeyValue("", "Image Overlap X", DEFAULT_IMAGE_OVERLAP_X));
            objRegistry.SetRegKeyValue("", "Image Overlap X", OverlapX);

            float OverlapY = (float)(objRegistry.GetRegKeyValue("", "Image Overlap Y", DEFAULT_IMAGE_OVERLAP_Y));
            objRegistry.SetRegKeyValue("", "Image Overlap Y", OverlapY);

            _imageOverlap = new SizeF(OverlapX, OverlapY);

            float ImageSizeX = (float)(objRegistry.GetRegKeyValue("", "Image Size X", DEFAULT_IMAGE_SIZE_X));
            objRegistry.SetRegKeyValue("", "Image Size X", ImageSizeX);

            float ImageSizeY = (float)(objRegistry.GetRegKeyValue("", "Image Size Y", DEFAULT_IMAGE_SIZE_Y));
            objRegistry.SetRegKeyValue("", "Image Size Y", ImageSizeY);

            _Speed = (float)(objRegistry.GetRegKeyValue("", "Speed", DEFAULT_SPEED));
            objRegistry.SetRegKeyValue("", "Speed", _Speed);

            _Acceleration = (float)(objRegistry.GetRegKeyValue("", "Acceleration", DEFAULT_ACCELERATION));
            objRegistry.SetRegKeyValue("", "Acceleration", _Acceleration);

            _capturedImageSize = new SizeF(ImageSizeX, ImageSizeY);


           


        }


        private void SendEvent(IEventData Event)
        {
            EventGroupData eventGroupData = new EventGroupData();
            eventGroupData.Enqueue(Event);

            if (EventGroupReady != null)
                EventGroupReady(this, eventGroupData);
        }

        private delegate void RCDataFormUpdateHandler(IEventData data);

        private void HandleZaberDataEvent(IEventData data)
        {
            switch (((ZaberDataEvent)data).DataType)
            {
                case ZaberDataEventType.ET_DEVICE_MOTION_COMPLETE:
                    {
                       // Thread.Sleep(200);
                        if (_actionSyncManager.GetSyncObject("MotionSync") != null)
                            _actionSyncManager.GetSyncObject("MotionSync").Set();
                        //TargetPositionReached((TMCDataPrmTargetPositionReached)((TMCDataEvent)data).DataParameters);
                        break;
                    }
            }
        }

        private void HandleRCDataEvent(IEventData data)
        {
          
                switch (((RCDataEvent)data).DataType)
                {
                    case RCDataEventType.ET_NEW_ROUTINE_LOADED_TO_PERFORMANCE_QUEUE:
                        {
                            UpdateNewLoadedRoutinePlayer((RCDataPrmNewRoutineLoaded)((RCDataEvent)data).DataParameters);
                            IsRoutineLoaded = true;
                            this.NotifyOfPropertyChange(() => CanRunRoutine);
                            break;
                        }
                    case RCDataEventType.ET_NEW_UPDATE_MESSAGE_RECEIVED:
                        {


                            RCDataPrmNewMessage d = (RCDataPrmNewMessage)((RCDataEvent)data).DataParameters;
                            RoutineProgressMessage = d.Message;
                            break;
                        }
                    
                }
            
        }

        private void UpdateNewLoadedRoutinePlayer(RCDataPrmNewRoutineLoaded rCDataPrmNewRoutineLoaded)
        {
            if (this.Equals(rCDataPrmNewRoutineLoaded.Owner))
                m_RoutineIndex = ((BaseRoutineDescriptor)rCDataPrmNewRoutineLoaded.Routine).RoutineIndex;
        }


        public void GetLotData(KeyEventArgs keyArgs)
        {

            if (keyArgs.Key == Key.Enter)
            {
                var lotData = _arsDispatcher.GetLotInfoWithFamilyInfo(LotNumberText);
                if (lotData == null)
                    return;
                LotDescription = lotData.LotDescription;
                CatNumber = lotData.CatNumber;

              
            }
        }

        public Single _Speed { get; set; }

        public Single _Acceleration { get; set; }
    }
}
