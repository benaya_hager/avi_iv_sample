﻿using SFW;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zaber_Common.Events.Zaber;
using Zaber_Common.Events.Zaber.NS_ZaberComandEventsParameters;
using Zaber_Common.Events.Zaber.NS_ZaberDataEventParameters;
using Caliburn.Micro;

using Tools.CustomRegistry;

namespace StartUp.ViewModels
{
    public class AxisControlViewModel : Caliburn.Micro.Screen, IEventGroupPublisher, IEventGroupConsumer, IComponent, IAxisControlViewModel
    {

        #region Private Members

        private ConcurrentDictionary<System.Type, Action<IEventData>> m_Handlers;
        private String[] AxisTypes = {"X","Y"};
        System.Timers.Timer UPAxisTimer = new System.Timers.Timer(50);

        const float DEFAULT_ACCELERATION = 50;
        const float DEFAULT_SPEED = 5;


        #endregion

        
        #region Public members


        private decimal _targetPosition;

        public decimal TargetPosition
        {
            get { return _targetPosition; }
            set { _targetPosition = value; NotifyOfPropertyChange(() => TargetPosition); }
        }

        private decimal _axisZeroPoint;

        public decimal AxisZeroPoint
        {
            get { return _axisZeroPoint; }
            set { _axisZeroPoint = value; NotifyOfPropertyChange(() => AxisZeroPoint); }
        }
        
        
        private string _axisCurrentPosition;

        public string AxisCurrentPosition
        {
            get { return _axisCurrentPosition; }
            set {
                var s = value.Split('\n');
                if (s[0] != "0")
                {

                    _axisCurrentPosition = s[1];
                }
                else
                    _axisCurrentPosition = s[0];

                NotifyOfPropertyChange(() => AxisCurrentPosition);

            }
        }

        private float _axisCurrentSpeed;

        public float AxisCurrentSpeed
        {
            get { return _axisCurrentSpeed; }
            set { _axisCurrentSpeed = value; NotifyOfPropertyChange(() => AxisCurrentSpeed); }
        }

        private float _axisCurrentAcceleration;

        public float AxisCurrentAcceleration
        {
            get { return _axisCurrentAcceleration; }
            set { _axisCurrentAcceleration = value; NotifyOfPropertyChange(() => AxisCurrentAcceleration); }
        }

        private string _axisCurrentState;

        public string AxisCurrentState
        {
            get { return _axisCurrentState; }
            set {
                string s = value.Split('\n')[1].Split(',')[0];
                _axisCurrentState = s; 
                
                NotifyOfPropertyChange(() => AxisCurrentState); }
        }

        private bool _ismotionComplete;

        public bool IsmotionComplete
        {
            get { return _ismotionComplete; }
            set { _ismotionComplete = value; NotifyOfPropertyChange(() => IsmotionComplete); }
        }


        private string _axisHomedStatus;

        public string AxisHomeStatus
        {
            get { return _axisHomedStatus; }
            set { _axisHomedStatus = value; NotifyOfPropertyChange(() => AxisHomeStatus); }
        }


        private string _AxisIDText;

        public string AxisIDText
        {
            get { return _AxisIDText; }
            set { _AxisIDText = value; NotifyOfPropertyChange(() => AxisIDText); }
        }
        

        private Byte m_MyAxisID;

        public Byte MyAxisID
        {
            get { return m_MyAxisID; }
            set
            {
                if (value != m_MyAxisID)
                {
                    m_MyAxisID = value;
                    AxisIDText  = String.Format("Axis: {0}", AxisTypes[m_MyAxisID-1]);
                    SendEvent(new ZaberCommandEvent(ZaberCommandEventTypes.CT_START_AXIS_STATUS_MONITORING, new ZaberCommandPrmAxisMonitoringThread(m_MyAxisID, Convert.ToInt32(100))));
                }
            }
        }


        public event EventGroupHandler EventGroupReady;


        #endregion


        #region Ctor

        public AxisControlViewModel()
        {
            // update Position first time
            UPAxisTimer.Elapsed += delegate
            {
                SendEvent(new ZaberCommandEvent(ZaberCommandEventTypes.CT_SEND_AXIS_TO_HOME_POSITION, new ZaberCommandEventParameters(m_MyAxisID)));
                SendEvent(new ZaberCommandEvent(ZaberCommandEventTypes.CT_START_AXIS_STATUS_MONITORING, new ZaberCommandPrmAxisMonitoringThread(m_MyAxisID,100)));
                SendEvent(new ZaberCommandEvent(ZaberCommandEventTypes.CT_UPDATE_AXIS_STATUS, new ZaberCommandPrmUpdateAxis(m_MyAxisID)));
                

                UPAxisTimer.Enabled = false;
            };
            m_Handlers = new System.Collections.Concurrent.ConcurrentDictionary<Type, Action<IEventData>>();

            MapHandlers();

            InitRegistryParameters();

           
            
            AxisHomeStatus = @"N\A";
            UPAxisTimer.Enabled = true;
            AxisCurrentPosition = "0";

        }

        private void InitRegistryParameters()
        {
            //Initialize application registry dataVid
            cRegistryFunc objRegistry = new cRegistryFunc("Zaber Motion Controller",
                                                                "",
                                                          cRegistryFunc._REG_BASE_KEY_TYPES.HKEY_LOCAL_MACHINE);

            _axisCurrentSpeed = (float)(objRegistry.GetRegKeyValue("", "Default Speed", DEFAULT_SPEED ));
            objRegistry.SetRegKeyValue("", "Default Speed", _axisCurrentSpeed);

            _axisCurrentAcceleration = (float)(objRegistry.GetRegKeyValue("", "Default Acceleration", DEFAULT_ACCELERATION));
            objRegistry.SetRegKeyValue("", "Default Acceleration", _axisCurrentAcceleration);
        }

        ~AxisControlViewModel()
        {
            ClearHandlers();
        }

        #endregion


        #region Private methods


        private void SendEvent(IEventData Event)
        {
            EventGroupData eventGroupData = new EventGroupData();
            eventGroupData.Enqueue(Event);

            if (EventGroupReady != null)
                EventGroupReady(this, eventGroupData);
        }

       



        #region IEventGroupConsumer Members

        public void OnProcessEventGroup(object sender, EventGroupData data)
        {
            foreach (IEventData singleEvent in data)
            {
                if (singleEvent != null)
                {
                    if (m_Handlers == null || !m_Handlers.ContainsKey(singleEvent.GetType()))
                    {
                        //throw new NullReferenceException("No handler mapped to this type of event " + singleEvent.GetType());
                    }
                    else
                    {
                        m_Handlers[singleEvent.GetType()](singleEvent);
                    }
                }
            }
        }


        public void MapHandlers()
        {
            m_Handlers.TryAdd(typeof(ZaberDataEvent), HandleZaberDataEvent);
        }

        public void ClearHandlers()
        {
            Action<IEventData> res;
            if (null != m_Handlers)
            {
                m_Handlers.TryRemove(typeof(ZaberDataEvent), out res);
            }
        }


        public void RaiseEventGroup(EventGroupData data)
        {
            if (EventGroupReady != null)
                EventGroupReady(this, data);
        }

        public void RaiseSingleEvent(IEventData data)
        {
            if (EventGroupReady != null)
            {
                EventGroupData groupData = new EventGroupData();

                groupData.Enqueue(data);

                EventGroupReady(this, groupData);
            }
        }
       

        #endregion


        private delegate void ZaberDataFormUpdateHandler(IEventData data);

        private void HandleZaberDataEvent(IEventData data)
        {
            if ((((ZaberDataEvent)data).DataParameters).DeviceID != m_MyAxisID) return;

     
                 Execute.BeginOnUIThread((System.Action)(() => 
     
            {

                switch (((ZaberDataEvent)data).DataType)
                {
                    case ZaberDataEventType.ET_CURRENT_POSITION_CHANGED:
                        {
                            CurrentPositionChanged((ZaberDataPrmCurPosChanged)((ZaberDataEvent)data).DataParameters);
                            break;
                        }
                  
                    case ZaberDataEventType.ET_DEVICE_STATUS_CHANGED:
                        {
                            UpdateDeviceCurrentState((ZaberDataPrmDeviceStatus)((ZaberDataEvent)data).DataParameters);
                            break;
                        }
                    case ZaberDataEventType.ET_DEVICE_HOMED_STATUS_CHANGED:
                        {
                            UpdateAxisHomedStatus((ZaberDataPrmBoolean)((ZaberDataEvent)data).DataParameters);
                            break;
                        }
                    case ZaberDataEventType.ET_DEVICE_MOTION_COMPLETE:
                        {
                            UpdateInMotionStatus((ZaberDataPrmBoolean)((ZaberDataEvent)data).DataParameters);
                            break;
                        }
                }
            }));
        }


        private void UpdateInMotionStatus(ZaberDataPrmBoolean zaberDataPrmBoolean)
        {
            _ismotionComplete = zaberDataPrmBoolean.BooleanParameter;
           
        }

        private void UpdateAxisHomedStatus(ZaberDataPrmBoolean zaberDataPrmBoolean)
        {
            AxisHomeStatus = String.Format("Axis Homed: ({0})", zaberDataPrmBoolean.BooleanParameter);
        }

        private void UpdateDeviceCurrentState(ZaberDataPrmDeviceStatus zaberDataPrmDeviceStatus)
        {
            AxisCurrentState = "Device Current State: \n" + Tools.String_Enum.StringEnum.GetStringValue(zaberDataPrmDeviceStatus.DeviceStatus);
        }

        //private void UpdateCurrentStateOfMonitoringThread(ZaberDataPrmDeviceMonitoringThreadStatus zaberDataPrmDeviceMonitoringThreadStatus)
        //{
        //    if (zaberDataPrmDeviceMonitoringThreadStatus.DeviceMonitoringThreadStatus)
        //    {
        //        btnMonitoringState.BackgroundImage = global::ZaberTest.Properties.Resources.Stop_Monitoring;
        //        btnMonitoringState.Tag = 1;
        //    }
        //    else
        //    {
        //        btnMonitoringState.BackgroundImage = global::ZaberTest.Properties.Resources.Start_Monitoring;
        //        btnMonitoringState.Tag = 0;
        //    }
        //}

        private void CurrentPositionChanged(ZaberDataPrmCurPosChanged zaberDataPrmCurPosChanged)
        {
                AxisCurrentPosition = String.Format("Current Position: \n {0}", zaberDataPrmCurPosChanged.CurrentPosition);
        }

        public void GoToPoint()
        {
            SendEvent(new ZaberCommandEvent(ZaberCommandEventTypes.CT_MOVE_AXIS_ABSOLUTE, new ZaberCommandPrmMoveAbsMM(m_MyAxisID,
                                                                                                                       Convert.ToSingle(_targetPosition),
                                                                                                                       Convert.ToSingle(AxisCurrentSpeed),
                                                                                                                       Convert.ToSingle(_axisCurrentAcceleration))));

        }

        public void Stop()
        {
            
            SendEvent(new ZaberCommandEvent(ZaberCommandEventTypes.CT_STOP_AXIS_MOTION, new ZaberCommandPrmStopAxisMotion(m_MyAxisID)));

        }

        public void ZeroPoint()
        {
            AxisZeroPoint = Convert.ToDecimal(AxisCurrentPosition);
        }

        public void UpdateAxis()
        {
            SendEvent(new ZaberCommandEvent(ZaberCommandEventTypes.CT_UPDATE_AXIS_STATUS, new ZaberCommandPrmUpdateAxis(m_MyAxisID)));
        }


        #endregion




        public string Name
        {
            get { throw new NotImplementedException(); }
        }
    }
}
