﻿using System;
namespace StartUp.ViewModels
{
    public interface IRoutineProgressViewModel
    {
        void ClearHandlers();
        event SFW.EventGroupHandler EventGroupReady;
        void MapHandlers();
        string Name { get; }
        void OnProcessEventGroup(object sender, SFW.EventGroupData data);
        void RaiseEventGroup(SFW.EventGroupData data);
        void RaiseSingleEvent(SFW.IEventData data);
    }
}
