﻿using NS_Video_Controller_Common.Events;
using NS_Video_Controller_Common.Events.NS_VideoComandEventsParameters;
using NS_Video_Controller_Common.Service;
using SFW;
using System;
using System.Collections.Concurrent;
using System.Windows.Forms;
using System.Windows.Forms.Integration;

namespace StartUp.ViewModels
{
    public class VideoControlViewModel : Caliburn.Micro.Screen,  SFW.IEventGroupConsumer, IComponent, StartUp.ViewModels.IVideoControlViewModel
    {
        private ConcurrentDictionary<System.Type, Action<IEventData>> m_Handlers;
        public event EventGroupHandler EventGroupReady;
        public VideoControlViewModel()
        {
           
        }

       

        private Form _videoForm;

        public Form VideoForm
        {
            get { return _videoForm; }
            set
            {
                _videoForm = value;
                _videoForm.TopLevel = false;
                _videoForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;

                
                _videoForm.MouseEnter += new EventHandler(_videoFor_MouseEnter);
              
                _videoForm.MouseWheel += new MouseEventHandler(_videoForm_MouseWheel);
            }
        }

        void _videoFor_MouseEnter(object sender, EventArgs e)
        {
            (sender as System.Windows.Forms.Form).Focus();
        }

        private void _videoForm_MouseWheel(object sender, MouseEventArgs e)
        {
            Single update_zoom_factor = 0;
            frmImageDisplay tf = _videoForm as frmImageDisplay;
            CameraIndexEnum cam = tf.myCameraIndex;
            if (e.Delta < 0)
            {
                update_zoom_factor = (Single)(-0.01);
            }
            else
            {
                update_zoom_factor = (Single)0.01;
            }

            SendEvent(new VideoCommandEvent(VideoCommandEventTypes.CT_UPDATE_ZOOM_FACTOR, new VideoCommandPrmUpdateZoom(update_zoom_factor, cam)));
        }

       

        public WindowsFormsHost VideoFormHost
        {
            get { return new WindowsFormsHost() { Child = _videoForm }; }
        }  

        public string Name
        {
            get { throw new NotImplementedException(); }
        }

        #region IEventGroupConsumer Members

        private void SendEvent(IEventData Event)
        {
            EventGroupData eventGroupData = new EventGroupData();
            eventGroupData.Enqueue(Event);

            if (EventGroupReady != null)
                EventGroupReady(this, eventGroupData);
        }

        public void OnProcessEventGroup(object sender, EventGroupData data)
        {
            foreach (IEventData singleEvent in data)
            {
                if (singleEvent != null)
                {
                    if (m_Handlers == null || !m_Handlers.ContainsKey(singleEvent.GetType()))
                    {
                        //throw new NullReferenceException("No handler mapped to this type of event " + singleEvent.GetType());
                    }
                    else
                    {
                        m_Handlers[singleEvent.GetType()](singleEvent);
                    }
                }
            }

            //RaiseEventGroup(data);
        }

        protected void RaiseEventGroup(EventGroupData data)
        {
            if (EventGroupReady != null)
                EventGroupReady(this, data);
        }

        #endregion IEventGroupConsumer Members
        public void MapHandlers()
        {
            throw new NotImplementedException();
        }

        public void ClearHandlers()
        {
            throw new NotImplementedException();
        }
    }
}
