﻿using System;
namespace StartUp.ViewModels
{
    public interface ILotDataControlViewModel
    {
        void ClearHandlers();
        event SFW.EventGroupHandler EventGroupReady;
        void MapHandlers();
        string Name { get; }
        void OnProcessEventGroup(object sender, SFW.EventGroupData data);
        void RaiseEventGroup(SFW.EventGroupData data);
        void RaiseSingleEvent(SFW.IEventData data);
    }
}
