﻿using Caliburn.Micro;
using SFW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections.Concurrent;
using SFW;
using Zaber_Common.Events.Zaber;
using Zaber_Common.Events.Zaber.NS_ZaberDataEventParameters;
using Zaber_Common.Events.Zaber.NS_ZaberComandEventsParameters;
using StartUp.ViewModels;
using Microsoft.Practices.Unity;

namespace StartUp.ViewModels
{
    public class CapturingRoutinePanelViewModel : Caliburn.Micro.Screen, IEventGroupPublisher, SFW.IEventGroupConsumer, ICapturingRoutinePanelViewModel, SFW.IComponent
    {

        #region Private Members

        private ConcurrentDictionary<System.Type, Action<IEventData>> m_Handlers;

        #endregion


       
        
        private AxisControlViewModel _xAxis;

        public AxisControlViewModel X_Axis
        {
            get { return _xAxis; }
            set { _xAxis = value; NotifyOfPropertyChange(() => X_Axis); }
        }

        private AxisControlViewModel _yAxis;

        public AxisControlViewModel Y_Axis
        {
            get { return _yAxis; }
            set { _yAxis = value; NotifyOfPropertyChange(() => Y_Axis); }
        }

        private VideoControlViewModel _vid;

        public VideoControlViewModel Vid
        {
            get { return _vid; }
            set { _vid = value; NotifyOfPropertyChange(() => Vid); }
        }

        private LotDataControlViewModel _Lot_Data;

        public LotDataControlViewModel Lot_Data
        {
            get { return _Lot_Data; }
            set { _Lot_Data = value; NotifyOfPropertyChange(() => Lot_Data); }
        }

        private RoutineProgressViewModel _Rprogress;

        public RoutineProgressViewModel Rprogress
        {
            get { return _Rprogress; }
            set { _Rprogress = value; NotifyOfPropertyChange(() => Rprogress); }
        }
        

        [InjectionConstructor]
        public CapturingRoutinePanelViewModel()
        {
            base.DisplayName = "Capturing Routine";
           
        }


        protected void RaiseEventGroup(EventGroupData data)
        {
            if (EventGroupReady != null)
                EventGroupReady(this, data);
        }

        protected virtual void RaiseSingleEvent(IEventData data)
        {
            if (EventGroupReady != null)
            {
                EventGroupData groupData = new EventGroupData();

                groupData.Enqueue(data);

                EventGroupReady(this, groupData);
            }
        }
        
        public event EventGroupHandler EventGroupReady;
    

        public void OnProcessEventGroup(object sender, EventGroupData data)
        {
            throw new NotImplementedException();
        }

        public void MapHandlers()
        {
            throw new NotImplementedException();
        }

        public void ClearHandlers()
        {
            throw new NotImplementedException();
        }



        void SFW.IEventGroupPublisher.RaiseEventGroup(SFW.EventGroupData data)
        {
            throw new NotImplementedException();
        }

        void SFW.IEventGroupPublisher.RaiseSingleEvent(SFW.IEventData data)
        {
            throw new NotImplementedException();
        }

        public string Name
        {
            get { return "CapturingRoutinePanelViewModel"; }
        }
    }
}
