﻿namespace User_Controls
{
    partial class AxisControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chckbxMotionComplete = new System.Windows.Forms.CheckBox();
            this.lblTargetPosition = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.nudTargetPosition = new System.Windows.Forms.NumericUpDown();
            this.lblRelativeMotionOffset = new System.Windows.Forms.Label();
            this.nudRelativeMotionOffset = new System.Windows.Forms.NumericUpDown();
            this.btnHome = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lblSpeed = new System.Windows.Forms.Label();
            this.nudAcceleration = new System.Windows.Forms.NumericUpDown();
            this.lblAcceleration = new System.Windows.Forms.Label();
            this.btnStop = new System.Windows.Forms.Button();
            this.nudSpeed = new System.Windows.Forms.NumericUpDown();
            this.lblAxisHomedStatus = new System.Windows.Forms.Label();
            this.lblCurrentPosition = new System.Windows.Forms.Label();
            this.btnStartAbsoluteMotion = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblDeviceCurrentStateTitle = new System.Windows.Forms.Label();
            this.btnStartRelativeMotion = new System.Windows.Forms.Button();
            this.grbxControlAxis = new System.Windows.Forms.GroupBox();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTargetPosition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRelativeMotionOffset)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudAcceleration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSpeed)).BeginInit();
            this.panel3.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.grbxControlAxis.SuspendLayout();
            this.SuspendLayout();
            // 
            // chckbxMotionComplete
            // 
            this.chckbxMotionComplete.AutoSize = true;
            this.chckbxMotionComplete.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chckbxMotionComplete.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chckbxMotionComplete.Enabled = false;
            this.chckbxMotionComplete.Location = new System.Drawing.Point(164, 203);
            this.chckbxMotionComplete.Name = "chckbxMotionComplete";
            this.chckbxMotionComplete.Size = new System.Drawing.Size(155, 126);
            this.chckbxMotionComplete.TabIndex = 20;
            this.chckbxMotionComplete.Text = "Motion Complete:";
            this.chckbxMotionComplete.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.chckbxMotionComplete.UseVisualStyleBackColor = true;
            // 
            // lblTargetPosition
            // 
            this.lblTargetPosition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTargetPosition.Location = new System.Drawing.Point(0, 0);
            this.lblTargetPosition.Name = "lblTargetPosition";
            this.lblTargetPosition.Size = new System.Drawing.Size(106, 15);
            this.lblTargetPosition.TabIndex = 7;
            this.lblTargetPosition.Text = "Target Position";
            this.lblTargetPosition.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.lblTargetPosition);
            this.panel5.Controls.Add(this.nudTargetPosition);
            this.panel5.Location = new System.Drawing.Point(325, 203);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(106, 35);
            this.panel5.TabIndex = 19;
            // 
            // nudTargetPosition
            // 
            this.nudTargetPosition.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.nudTargetPosition.Location = new System.Drawing.Point(0, 15);
            this.nudTargetPosition.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.nudTargetPosition.Minimum = new decimal(new int[] {
            1316134911,
            2328,
            0,
            -2147483648});
            this.nudTargetPosition.Name = "nudTargetPosition";
            this.nudTargetPosition.Size = new System.Drawing.Size(106, 20);
            this.nudTargetPosition.TabIndex = 2;
            this.nudTargetPosition.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudTargetPosition.Value = new decimal(new int[] {
            13000,
            0,
            0,
            0});
            // 
            // lblRelativeMotionOffset
            // 
            this.lblRelativeMotionOffset.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblRelativeMotionOffset.Location = new System.Drawing.Point(0, 0);
            this.lblRelativeMotionOffset.Name = "lblRelativeMotionOffset";
            this.lblRelativeMotionOffset.Size = new System.Drawing.Size(155, 106);
            this.lblRelativeMotionOffset.TabIndex = 17;
            this.lblRelativeMotionOffset.Text = "Step Size";
            this.lblRelativeMotionOffset.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // nudRelativeMotionOffset
            // 
            this.nudRelativeMotionOffset.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.nudRelativeMotionOffset.Location = new System.Drawing.Point(0, 106);
            this.nudRelativeMotionOffset.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.nudRelativeMotionOffset.Minimum = new decimal(new int[] {
            1316134911,
            2328,
            0,
            -2147483648});
            this.nudRelativeMotionOffset.Name = "nudRelativeMotionOffset";
            this.nudRelativeMotionOffset.Size = new System.Drawing.Size(155, 20);
            this.nudRelativeMotionOffset.TabIndex = 16;
            this.nudRelativeMotionOffset.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudRelativeMotionOffset.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // btnHome
            // 
            this.btnHome.BackgroundImage = global::User_Controls.Properties.Resources.apps_home_icon;
            this.btnHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnHome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnHome.Location = new System.Drawing.Point(3, 135);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(155, 62);
            this.btnHome.TabIndex = 10;
            this.btnHome.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.lblRelativeMotionOffset);
            this.panel4.Controls.Add(this.nudRelativeMotionOffset);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 203);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(155, 126);
            this.panel4.TabIndex = 19;
            // 
            // lblSpeed
            // 
            this.lblSpeed.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSpeed.Location = new System.Drawing.Point(0, 0);
            this.lblSpeed.Name = "lblSpeed";
            this.lblSpeed.Size = new System.Drawing.Size(155, 17);
            this.lblSpeed.TabIndex = 5;
            this.lblSpeed.Text = "Speed";
            this.lblSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // nudAcceleration
            // 
            this.nudAcceleration.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.nudAcceleration.Location = new System.Drawing.Point(0, 17);
            this.nudAcceleration.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.nudAcceleration.Minimum = new decimal(new int[] {
            1316134911,
            2328,
            0,
            -2147483648});
            this.nudAcceleration.Name = "nudAcceleration";
            this.nudAcceleration.Size = new System.Drawing.Size(155, 20);
            this.nudAcceleration.TabIndex = 4;
            this.nudAcceleration.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudAcceleration.Value = new decimal(new int[] {
            205,
            0,
            0,
            0});
            // 
            // lblAcceleration
            // 
            this.lblAcceleration.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblAcceleration.Location = new System.Drawing.Point(0, 37);
            this.lblAcceleration.Name = "lblAcceleration";
            this.lblAcceleration.Size = new System.Drawing.Size(155, 22);
            this.lblAcceleration.TabIndex = 6;
            this.lblAcceleration.Text = "Acceleration";
            this.lblAcceleration.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnStop
            // 
            this.btnStop.BackgroundImage = global::User_Controls.Properties.Resources.stop_icon;
            this.btnStop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnStop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnStop.Location = new System.Drawing.Point(164, 135);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(155, 62);
            this.btnStop.TabIndex = 0;
            this.btnStop.UseVisualStyleBackColor = true;
            // 
            // nudSpeed
            // 
            this.nudSpeed.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.nudSpeed.Location = new System.Drawing.Point(0, 59);
            this.nudSpeed.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.nudSpeed.Minimum = new decimal(new int[] {
            1316134911,
            2328,
            0,
            -2147483648});
            this.nudSpeed.Name = "nudSpeed";
            this.nudSpeed.Size = new System.Drawing.Size(155, 20);
            this.nudSpeed.TabIndex = 3;
            this.nudSpeed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudSpeed.Value = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            // 
            // lblAxisHomedStatus
            // 
            this.lblAxisHomedStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAxisHomedStatus.Location = new System.Drawing.Point(3, 0);
            this.lblAxisHomedStatus.Name = "lblAxisHomedStatus";
            this.lblAxisHomedStatus.Size = new System.Drawing.Size(155, 132);
            this.lblAxisHomedStatus.TabIndex = 22;
            this.lblAxisHomedStatus.Text = "Axis Homed: (N/A)";
            this.lblAxisHomedStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCurrentPosition
            // 
            this.lblCurrentPosition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCurrentPosition.Location = new System.Drawing.Point(164, 0);
            this.lblCurrentPosition.Name = "lblCurrentPosition";
            this.lblCurrentPosition.Size = new System.Drawing.Size(155, 132);
            this.lblCurrentPosition.TabIndex = 21;
            this.lblCurrentPosition.Text = "Current Position:";
            this.lblCurrentPosition.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnStartAbsoluteMotion
            // 
            this.btnStartAbsoluteMotion.BackgroundImage = global::User_Controls.Properties.Resources.tm_mot_ico;
            this.btnStartAbsoluteMotion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnStartAbsoluteMotion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnStartAbsoluteMotion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.btnStartAbsoluteMotion.Location = new System.Drawing.Point(325, 335);
            this.btnStartAbsoluteMotion.Name = "btnStartAbsoluteMotion";
            this.btnStartAbsoluteMotion.Size = new System.Drawing.Size(155, 79);
            this.btnStartAbsoluteMotion.TabIndex = 1;
            this.btnStartAbsoluteMotion.Text = "Absolute";
            this.btnStartAbsoluteMotion.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnStartAbsoluteMotion.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.lblSpeed);
            this.panel3.Controls.Add(this.nudAcceleration);
            this.panel3.Controls.Add(this.lblAcceleration);
            this.panel3.Controls.Add(this.nudSpeed);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(164, 335);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(155, 79);
            this.panel3.TabIndex = 19;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this.lblDeviceCurrentStateTitle, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblAxisHomedStatus, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblCurrentPosition, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnStartAbsoluteMotion, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.btnStartRelativeMotion, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.btnStop, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnHome, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel5, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.chckbxMotionComplete, 1, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 68F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 84F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(483, 417);
            this.tableLayoutPanel1.TabIndex = 18;
            // 
            // lblDeviceCurrentStateTitle
            // 
            this.lblDeviceCurrentStateTitle.AutoSize = true;
            this.lblDeviceCurrentStateTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDeviceCurrentStateTitle.Location = new System.Drawing.Point(325, 0);
            this.lblDeviceCurrentStateTitle.Name = "lblDeviceCurrentStateTitle";
            this.tableLayoutPanel1.SetRowSpan(this.lblDeviceCurrentStateTitle, 2);
            this.lblDeviceCurrentStateTitle.Size = new System.Drawing.Size(155, 200);
            this.lblDeviceCurrentStateTitle.TabIndex = 23;
            this.lblDeviceCurrentStateTitle.Text = "Device Current State:  N/A";
            this.lblDeviceCurrentStateTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnStartRelativeMotion
            // 
            this.btnStartRelativeMotion.BackgroundImage = global::User_Controls.Properties.Resources.startrelative;
            this.btnStartRelativeMotion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnStartRelativeMotion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnStartRelativeMotion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.btnStartRelativeMotion.Location = new System.Drawing.Point(3, 335);
            this.btnStartRelativeMotion.Name = "btnStartRelativeMotion";
            this.btnStartRelativeMotion.Size = new System.Drawing.Size(155, 79);
            this.btnStartRelativeMotion.TabIndex = 15;
            this.btnStartRelativeMotion.Text = "Relative";
            this.btnStartRelativeMotion.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnStartRelativeMotion.UseVisualStyleBackColor = true;
            // 
            // grbxControlAxis
            // 
            this.grbxControlAxis.Controls.Add(this.tableLayoutPanel1);
            this.grbxControlAxis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbxControlAxis.Location = new System.Drawing.Point(0, 0);
            this.grbxControlAxis.Name = "grbxControlAxis";
            this.grbxControlAxis.Size = new System.Drawing.Size(489, 436);
            this.grbxControlAxis.TabIndex = 12;
            this.grbxControlAxis.TabStop = false;
            this.grbxControlAxis.Text = "Axis #";
            // 
            // AxisControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.grbxControlAxis);
            this.Name = "AxisControl";
            this.Size = new System.Drawing.Size(489, 436);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nudTargetPosition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRelativeMotionOffset)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nudAcceleration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSpeed)).EndInit();
            this.panel3.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.grbxControlAxis.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox chckbxMotionComplete;
        private System.Windows.Forms.Label lblTargetPosition;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.NumericUpDown nudTargetPosition;
        private System.Windows.Forms.Label lblRelativeMotionOffset;
        private System.Windows.Forms.NumericUpDown nudRelativeMotionOffset;
        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lblSpeed;
        private System.Windows.Forms.NumericUpDown nudAcceleration;
        private System.Windows.Forms.Label lblAcceleration;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.NumericUpDown nudSpeed;
        private System.Windows.Forms.Label lblAxisHomedStatus;
        private System.Windows.Forms.Label lblCurrentPosition;
        private System.Windows.Forms.Button btnStartAbsoluteMotion;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblDeviceCurrentStateTitle;
        private System.Windows.Forms.Button btnStartRelativeMotion;
        private System.Windows.Forms.GroupBox grbxControlAxis;
    }
}
