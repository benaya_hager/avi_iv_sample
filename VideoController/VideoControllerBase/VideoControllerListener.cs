﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using NS_Common;
using NS_Video_Controller_Common.Events;
using NS_Video_Controller_Common.Events.NS_VideoDataEventParameters;
using NS_Video_Controller_Common.Interfaces;
using NS_Video_Controller_Common.Service;
using SFW; 

namespace NS_VideoControllerBase
{
    public class VideoControllerListener : Listener, IVideoControllerListener
    {

        #region protected Members

        protected ILog m_Log;
        protected Dictionary<CameraIndexEnum, IMil10BaseCamera> m_DeviceList = null; 

        #endregion

        #region Public Properties

        protected short m_ErrorStatus = ErrorCodesList.OK;
        /// <summary>
        /// Get/Set the error status for controller
        /// </summary>
        public virtual short ErrorStatus
        {
            get { return m_ErrorStatus; }
            set { m_ErrorStatus = value; }
        }

        protected String m_ErrorMessage = "";
        /// <summary>
        /// Get/Set the error status Message for controller
        /// </summary>
        public virtual String ErrorMessage
        {
            get { return m_ErrorMessage; }
            set { m_ErrorMessage = value; }
        }

        #endregion

        #region Constructors

        public VideoControllerListener(IMil10BaseCamera camera_device)
            : base(camera_device)
        {
            m_Name = "VideoControllerListener";
             
            m_Log = LogManager.GetLogger("VideoController");

            //If passed Device is not null
            if (null != ((IMil10BaseCamera)camera_device))
            {
                //Check If Device Implements INotifyPropertyChanged interface
                var NotifyObj = camera_device as System.ComponentModel.INotifyPropertyChanged;
                if (NotifyObj != null)
                {
                    //Subscribe to all Property changed events
                    ((System.ComponentModel.INotifyPropertyChanged)NotifyObj).PropertyChanged += (s, e) => { HandlePropertyChanged(s, e); };
                }
            } 
        } 

        public VideoControllerListener( params IMil10BaseCamera[] cameras_list)
            : base(cameras_list[0])
        {
            m_Name = "VideoControllerListener";
             
            m_Log = LogManager.GetLogger("VideoController");

            foreach (IMil10BaseCamera camera_device in cameras_list)
            {
                //If passed Device is not null
                if (null != ((IMil10BaseCamera)camera_device))
                {
                    //Check If Device Implements INotifyPropertyChanged interface
                    var NotifyObj = camera_device as System.ComponentModel.INotifyPropertyChanged;
                    if (NotifyObj != null)
                    {
                        //Subscribe to all Property changed events
                        ((System.ComponentModel.INotifyPropertyChanged)NotifyObj).PropertyChanged += (s, e) => { HandlePropertyChanged(s, e); };
                    }
                }
            }
        }

        public VideoControllerListener(Dictionary<CameraIndexEnum, IMil10BaseCamera> device_list )
            : this(device_list[CameraIndexEnum.First])
        {
            m_Name = "VideoControllerListener";

            m_Log = LogManager.GetLogger("VideoController");

            foreach (IMil10BaseCamera camera_device in device_list.Values )
            {
                //If passed Device is not null
                if (null != ((IMil10BaseCamera)camera_device))
                {
                    //Check If Device Implements INotifyPropertyChanged interface
                    var NotifyObj = camera_device as System.ComponentModel.INotifyPropertyChanged;
                    if (NotifyObj != null)
                    {
                        //Subscribe to all Property changed events
                        ((System.ComponentModel.INotifyPropertyChanged)NotifyObj).PropertyChanged += (s, e) => { HandlePropertyChanged(s, e); };
                    }
                }
            }
        }
       
        #endregion

        protected virtual void HandlePropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs property_data)
        {
            IMil10BaseCamera camera = (sender as IMil10BaseCamera);
            if (null != camera)
            {
                switch (property_data.PropertyName)
                {
                    case "WindowHandle":
                        SendUpdateEvent(new VideoDataEvent(VideoDataEventType.ET_TARGET_WINDOW_PTR_CHANGED,
                                                          new VideoDataPrmIntPtrChanged( camera.CameraIndex , camera.WindowHandle)));
                        break;
                    case "Shutter":
                        SendUpdateEvent(new VideoDataEvent(VideoDataEventType.ET_SHUTTER_CHANGED,
                                                            new VideoDataPrmSingleChanged(camera.CameraIndex, camera.Shutter)));
                        break;
                    case "Gain":
                        SendUpdateEvent(new VideoDataEvent(VideoDataEventType.ET_GAIN_CHANGED ,
                                                            new VideoDataPrmSingleChanged(camera.CameraIndex, camera.Gain)));
                        break;
                    case "ImageCropRectangle":
                        SendUpdateEvent(new VideoDataEvent(VideoDataEventType.ET_CROP_RECTANGLE_CHANGED,
                                                            new VideoDataPrmRectangleChanged(camera.CameraIndex, camera.ImageCropRectangle)));
                        break;
                    case "FocusValue":
                        SendUpdateEvent(new VideoDataEvent(VideoDataEventType.ET_FOCUS_VALUE_CHANGED,
                                                            new VideoDataPrmFocusChanged(camera.CameraIndex, camera.FocusValue,new List<System.Drawing.Rectangle>(),false)));
                        break;
                    case "PixelToMillimter":
                        SendUpdateEvent(new VideoDataEvent(VideoDataEventType.ET_PXL_2_MM_CHANGED,
                                                            new VideoDataPrmDoubleChanged(camera.CameraIndex, camera.PixelToMillimter)));


                        break;
                }
            }
        }


        protected virtual void SendUpdateEvent(VideoDataEvent Event, Boolean isSync = false)
        {
            if (isSync)
            {
                m_EventGroupData = new EventGroupData();

                m_EventGroupData.Enqueue(Event);

                RaiseEventGroupReady();
            }
            else
            {
                System.Threading.Tasks.Task.Factory.StartNew(() =>
                {
                    m_EventGroupData = new EventGroupData();

                    m_EventGroupData.Enqueue(Event);

                    RaiseEventGroupReady();
                });
            }
        }


        protected override void OnInputDeviceDataReceived(object sender, SFW.EventArgs<object> e)
        {
            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                m_EventGroupData = new EventGroupData();

                m_EventGroupData.Enqueue((IEventData)e.Value);

                // m_Log.Info("Sending " + m_LastMeasure);

                RaiseEventGroupReady();
            });
        }
    }
}
