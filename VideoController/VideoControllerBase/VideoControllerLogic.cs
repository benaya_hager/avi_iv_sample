﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using NS_Video_Controller_Common.Events;
using NS_Video_Controller_Common.Events.NS_VideoComandEventsParameters;
using NS_Video_Controller_Common.Interfaces;
using NS_Video_Controller_Common.Service;
using SFW;
using NS_VideoControllerBase;
using System.Threading;
using NS_Video_Controller_Common.Events.VideoComandEventsParameters;
using NS_Video_Controller_Common.Events.NS_VideoDataEventParameters;
using Navitar;

namespace NS_VideoControllerSimulator
{
    public class VideoControllerLogic : Logic, IVideoControllerLogic
    {
        #region Protected Members

        protected ILog m_Log;
        protected IVideoControllerListener m_Listener;
        protected IActionSyncManager m_actionSyncManager;
        protected SortedList<int,List<int>> m_FocusResultsList;
        //protected VideoController m_VideoContollerDevice; 

        #endregion

        #region Constructors

        public VideoControllerLogic(IActionSyncManager actionSyncManager)
            : base()
        {
            m_actionSyncManager = actionSyncManager;
            m_FocusResultsList = new SortedList<int, List<int>>(3);
        }

        public VideoControllerLogic(IMil10BaseCamera camera_device, IVideoControllerListener listener, IActionSyncManager actionSyncManager)
            : this(actionSyncManager)
        {
            m_Log = LogManager.GetLogger("VideoControllerLogic"); 
            m_Mil10CameraDevices = new System.Collections.Generic.Dictionary<CameraIndexEnum, IMil10BaseCamera>();
            if (null != camera_device)
                m_Mil10CameraDevices.Add(camera_device.CameraIndex, camera_device);
            m_Listener = listener;
            
        }

        public VideoControllerLogic(IVideoControllerListener listener,  IActionSyncManager actionSyncManager,params IMil10BaseCamera[] cameras_list)
            : this(cameras_list[0], listener,actionSyncManager)
        {
            if (null == m_Mil10CameraDevices)
                m_Mil10CameraDevices = new Dictionary<CameraIndexEnum, IMil10BaseCamera>();
            for (int i = 0; i < cameras_list.Length; i++)
            {
                if (!m_Mil10CameraDevices.ContainsKey(cameras_list[i].CameraIndex))
                    m_Mil10CameraDevices.Add(cameras_list[i].CameraIndex, cameras_list[i]);
            } 
        }

        public VideoControllerLogic(Dictionary<CameraIndexEnum, IMil10BaseCamera> device_list, IVideoControllerListener listener, IActionSyncManager actionSyncManager)
            : this(device_list[CameraIndexEnum.First], listener,actionSyncManager)
        {
            m_Mil10CameraDevices = device_list; 
        }


        #endregion

        #region Public Properties

       
        

        protected Int32 m_AutomaticImagesSavingTimeout = 5000;
        public Int32 AutomaticImagesSavingTimeout
        {
            get { return m_AutomaticImagesSavingTimeout; }
            set { m_AutomaticImagesSavingTimeout = value; }
        }


        protected Dictionary<CameraIndexEnum, IMil10BaseCamera> m_Mil10CameraDevices;
        public virtual Dictionary<CameraIndexEnum, IMil10BaseCamera> Mil10CameraDevices
        {
            get { return m_Mil10CameraDevices; }
            protected set { m_Mil10CameraDevices = value; }
        }

        /// <summary>
        /// Number of available cameras in system.
        /// </summary>
        public virtual int TheNumberOfCameras
        {
            get { return m_Mil10CameraDevices.Count; }
        }

        #endregion

        #region Protected Methods

        protected override void MapHandlers()
        {
            m_Handlers.TryAdd(typeof(VideoCommandEvent), HandleVideoCommandEvent);
        }

        protected override void ClearHandlers()
        {
            Action<IEventData> res;
            if (null != m_Handlers)
            {
                m_Handlers.TryRemove(typeof(VideoCommandEvent), out res);
            }
        }

        protected virtual void HandleVideoCommandEvent(IEventData data)
        {
            if (data.GetType() != typeof(VideoCommandEvent))
            {
                throw new ArgumentException("value must be of type VideoCommandEvent.", "value");
            }

            switch (((VideoCommandEvent)data).CmdType)
            {
                case VideoCommandEventTypes.CT_START_GRABBING:
                    {
                        StartGrabbing((VideoCommandPrmSetDisplayWindow)((VideoCommandEvent)data).CommandParameters);
                        break;
                    }
                case VideoCommandEventTypes.CT_STOP_GRABBING:
                    {
                        StopGrabbing((VideoCommandEventParameters)((VideoCommandEvent)data).CommandParameters);
                        break;
                    }
                case VideoCommandEventTypes.CT_RELEASE_COMPONENTS:
                    {
                        ReleaseComponents((VideoCommandPrmReleaseComponents)((VideoCommandEvent)data).CommandParameters);
                        break;
                    }

                case VideoCommandEventTypes.CT_SET_DISPLAY_WINDOW:
                    {
                        SetDisplayWindow((VideoCommandPrmSetDisplayWindow)((VideoCommandEvent)data).CommandParameters);
                        break;
                    }

                case VideoCommandEventTypes.CT_UPDATE_SHUTTER_FACTOR:
                    {
                        UpdateShutter((VideoCommandPrmUpdateShutter)((VideoCommandEvent)data).CommandParameters);
                        break;
                    }
                case VideoCommandEventTypes.CT_COMPUTE_FOCUS:
                    {
                        ComputeFocus((VideoCommandEventComputeFocus)((VideoCommandEvent)data).CommandParameters);
                       
                        break;

                    }
                case VideoCommandEventTypes.CT_COMPUTE_MULTI_FOCUS:
                    {
                        
                        ComputeMultiFocus((VideoCommandEventComputeFocus)((VideoCommandEvent)data).CommandParameters);
                        break;

                    }
                case VideoCommandEventTypes.CT_CAPTURE_MULTI_FOCUS:
                    {

                        CaptureAndComputeMultiFocus((VideoCommandEventComputeFocus)((VideoCommandEvent)data).CommandParameters);
                        break;

                    }
                case VideoCommandEventTypes.CT_UPDATE_GAIN_FACTOR:
                    {
                        UpdateGain((VideoCommandPrmUpdateGain)((VideoCommandEvent)data).CommandParameters);
                        break;
                    }
                case VideoCommandEventTypes.CT_GET_LAST_SESSION_SETUP_PARAMETERS:
                {
                    ReadLastUsageDataSetup((VideoCommandEventParameters)((VideoCommandEvent)data).CommandParameters);
                    break;
                }
                case VideoCommandEventTypes.CT_UPDATE_PIXEL_TO_MILLIMETER:
                    {
                        //UpdatePixel2MillimeterParameter((VideoCommandPrmUpdatePxl2MM)((VideoCommandEvent)data).CommandParameters);
                        break;
                    }

                #region Parameters Update

                case VideoCommandEventTypes.CT_UPDATE_IMAGE_CROP_RECTANGLE:
                    {
                        UpdateImageCropRectangle((VideoCommandPrmUpdateImageCropRectangle)((VideoCommandEvent)data).CommandParameters);
                        break;
                    }


                case VideoCommandEventTypes.CT_UPDATE_PAN_FACTOR:
                    {
                        UpdatePanFactor((VideoCommandPrmUpdatePan)((VideoCommandEvent)data).CommandParameters);
                        break;
                    }
                case VideoCommandEventTypes.CT_UPDATE_ZOOM_FACTOR:
                    {
                        UpdateZoomFactor((VideoCommandPrmUpdateZoom)((VideoCommandEvent)data).CommandParameters);
                        break;
                    }

                case VideoCommandEventTypes.CT_RESET_ZOOM_FACTOR:
                    {
                        ResetZoomFactor((VideoCommandEventParameters)((VideoCommandEvent)data).CommandParameters);
                        break;
                    } 
                #endregion

                #region Draw On Overlay

                case VideoCommandEventTypes.CT_DRAW_CROSS:
                    {
                        DrawCrossOnOverlay((VideoCommandPrmDrawCross)((VideoCommandEvent)data).CommandParameters);
                        break;
                    }

                #endregion

                #region Image Saving

                case VideoCommandEventTypes.CT_CAPTURE_AND_SAVE_IMAGE:
                    {
                        CaptureAndSaveImage((VideoCommandPrmCaptureImage)((VideoCommandEvent)data).CommandParameters);
                        break;
                    }
                case VideoCommandEventTypes.CT_CAPTURE_AND_SAVE_IMAGE_REGION:
                    {
                        CaptureAndSaveImage((VideoCommandPrmCaptureImageRegion)((VideoCommandEvent)data).CommandParameters);
                        break;
                    }

                case VideoCommandEventTypes.CT_SAVE_IMAGE:
                    {
                        SaveImage((VideoCommandPrmCaptureImage)((VideoCommandEvent)data).CommandParameters);
                        break;
                    }
                //case VideoCommandEventTypes.CT_START_IMAGES_SAVING_THREAD:
                //    {
                //        StartImageSavingThread((VideoCommandPrmSetImageesSavingThread)((VideoCommandEvent)data).CommandParameters);
                //        break;
                //    }
                //case VideoCommandEventTypes.CT_STOP_IMAGES_SAVING_THREAD:
                //    {
                //        StopImageSavingThread((VideoCommandEventParameters)((VideoCommandEvent)data).CommandParameters);
                //        break;
                //    }


                #endregion
            }
        }

        private void ComputeFocus(VideoCommandEventComputeFocus videoCommandEventComputeFocus)
        {

            foreach (IMil10BaseCamera cam in m_Mil10CameraDevices.Values)
            {
                if (null != cam)
                {
                    


                        long f = cam.MILComputeFocus(videoCommandEventComputeFocus.CropRectangle);
                        SendEvent(new VideoDataEvent(VideoDataEventType.ET_FOCUS_VALUE_CHANGED,
                                                              new VideoDataPrmFocusChanged(cam.CameraIndex, f, videoCommandEventComputeFocus.CropRectangle,videoCommandEventComputeFocus.IsFineFocus)));
                    
                 
                }
            }



        }

        private void ComputeMultiFocus(VideoCommandEventComputeFocus videoCommandEventComputeFocus)
        {

            foreach (IMil10BaseCamera cam in m_Mil10CameraDevices.Values)
            {
                if (null != cam)
                {

                    List<int> flist = cam.MILComputeMultiFocus(videoCommandEventComputeFocus.CropRectangle, videoCommandEventComputeFocus.MultiFocusStage);
                    if (m_FocusResultsList.ContainsKey(videoCommandEventComputeFocus.MultiFocusStage))
                        m_FocusResultsList[videoCommandEventComputeFocus.MultiFocusStage] = flist;
                    else
                    {
                        m_FocusResultsList.Add(videoCommandEventComputeFocus.MultiFocusStage, flist);
                    }
                    if (videoCommandEventComputeFocus.MultiFocusStage < 3)
                    {
                       
                        SendEvent(new VideoDataEvent(VideoDataEventType.ET_MULTI__FOCUS_VALUE_CHANGED,
                                                          new VideoDataPrmFocusChanged(cam.CameraIndex, (int)flist.Average(), videoCommandEventComputeFocus.CropRectangle, flist, videoCommandEventComputeFocus.IsFineFocus)));
                    }
                    else
                    {
                        List<System.Drawing.Rectangle> areaList = videoCommandEventComputeFocus.CropRectangle;
                        List<int> MaxFocusIndexes;
                        MaxFocusIndexes = GetMaxFocusIndexes(m_FocusResultsList);
                        cam.CreateComposedFocusedImage(MaxFocusIndexes, videoCommandEventComputeFocus.CropRectangle);

                        if (m_actionSyncManager.GetSyncObject("FocusSync") != null)
                            m_actionSyncManager.GetSyncObject("FocusSync").Set();
                    }
                    


                }
            }



        }

        private void CaptureAndComputeMultiFocus(VideoCommandEventComputeFocus videoCommandEventComputeFocus)
        {
            foreach (IMil10BaseCamera cam in m_Mil10CameraDevices.Values)
            {
                if (null != cam)
                {
                    cam.CaptureMultiFocusImage(videoCommandEventComputeFocus.TargetDirectory, videoCommandEventComputeFocus.TargetFileName, videoCommandEventComputeFocus.CropRectangle, 2, videoCommandEventComputeFocus.BestFocusPosition, 100);
                    if (m_actionSyncManager.GetSyncObject("VideoSync") != null)
                        m_actionSyncManager.GetSyncObject("VideoSync").Set();
                }
            }
        }

        private List<int> GetMaxFocusIndexes(SortedList<int, List<int>> m_FcsRsltsLst)
        {
            List<int> tempList;
            
            List<int> resultList = new List<int>();
            tempList = m_FcsRsltsLst.First().Value;
            int AreaCount = tempList.Count();
            int ImageCount = m_FcsRsltsLst.Count();
            for (int i = 0; i < AreaCount; i++)
            {
                int MaxFocus = 0;
                int MaxFocusIndex = 0;
                int j = 0;
                foreach (var item in m_FcsRsltsLst)
                {
                    if (item.Value[i] > MaxFocus)
                    {
                        MaxFocus = item.Value[i];
                        MaxFocusIndex = j;
                       
                    }
                    j++;
                }
                resultList.Add(MaxFocusIndex);

            }

            return resultList;

        }
               
                

            

          
        

        private void DrawCrossOnOverlay(VideoCommandPrmDrawCross videoCommandPrmDrawCross)
        {
             switch (videoCommandPrmDrawCross.CameraIndex)
            {
                case CameraIndexEnum.NOT_SET:
                    { return; }

                case CameraIndexEnum.ALL:
                    {
                        foreach (IMil10BaseCamera cam in m_Mil10CameraDevices.Values)
                        {
                            if (null != cam)
                                cam.DrawCross(videoCommandPrmDrawCross.CenterCircleSize, videoCommandPrmDrawCross.CrossThikness);
                        }
                        break;
                    }

                default:
                    {
                        if (null != m_Mil10CameraDevices[videoCommandPrmDrawCross.CameraIndex])
                            m_Mil10CameraDevices[videoCommandPrmDrawCross.CameraIndex].DrawCross(videoCommandPrmDrawCross.CenterCircleSize, videoCommandPrmDrawCross.CrossThikness);
                        break;
                    }

            }
        }
        

       

        #endregion

        #region Save Image Methods

        protected virtual void CaptureAndSaveImage(VideoCommandPrmCaptureImageRegion videoCommandPrmCaptureImageRegion)
        {
            switch (videoCommandPrmCaptureImageRegion.CameraIndex)
            {
                case CameraIndexEnum.NOT_SET:
                    { return; }

                case CameraIndexEnum.ALL:
                    {
                        foreach (IMil10BaseCamera cam in m_Mil10CameraDevices.Values)
                        {
                            if (null != cam)
                                cam.CaptureAndSaveImage(videoCommandPrmCaptureImageRegion.TargetDirectory,
                                                          videoCommandPrmCaptureImageRegion.TargetFileName,
                                                          videoCommandPrmCaptureImageRegion.CropRectangle);
                        }
                        if (m_actionSyncManager.GetSyncObject("VideoSync") != null)
                            m_actionSyncManager.GetSyncObject("VideoSync").Set();
                        break;
                    }
                default:
                    {
                        if (null != m_Mil10CameraDevices[videoCommandPrmCaptureImageRegion.CameraIndex])
                            m_Mil10CameraDevices[videoCommandPrmCaptureImageRegion.CameraIndex].CaptureAndSaveImage(videoCommandPrmCaptureImageRegion.TargetDirectory,
                                                                                                                  videoCommandPrmCaptureImageRegion.TargetFileName,
                                                                                                                  videoCommandPrmCaptureImageRegion.CropRectangle);
                         if (m_actionSyncManager.GetSyncObject("VideoSync") != null)
                            m_actionSyncManager.GetSyncObject("VideoSync").Set();
                        break;
                    }
            }
            
        }

        protected virtual void CaptureAndSaveImage(VideoCommandPrmCaptureImage videoCommandPrmCaptureImage)
        {
            switch (videoCommandPrmCaptureImage.CameraIndex)
            {
                case CameraIndexEnum.NOT_SET:
                    { return; }

                case CameraIndexEnum.ALL:
                    {
                        foreach (IMil10BaseCamera cam in m_Mil10CameraDevices.Values)
                        {
                            if (null != cam)
                                cam.CaptureAndSaveImage(videoCommandPrmCaptureImage.TargetDirectory,
                                              videoCommandPrmCaptureImage.TargetFileName);
                        }

                        if (m_actionSyncManager.GetSyncObject("VideoSync") != null)
                            m_actionSyncManager.GetSyncObject("VideoSync").Set();
                        break;
                    }
                default:
                    {
                        if (null != m_Mil10CameraDevices[videoCommandPrmCaptureImage.CameraIndex])
                            m_Mil10CameraDevices[videoCommandPrmCaptureImage.CameraIndex].CaptureAndSaveImage(videoCommandPrmCaptureImage.TargetDirectory,
                                                                                                             videoCommandPrmCaptureImage.TargetFileName);
                         if (m_actionSyncManager.GetSyncObject("VideoSync") != null)
                            m_actionSyncManager.GetSyncObject("VideoSync").Set();
                        break;
                    }
            }


        }

        protected virtual void SaveImage(VideoCommandPrmCaptureImage videoCommandPrmCaptureImage)
        {
            switch (videoCommandPrmCaptureImage.CameraIndex)
            {
                case CameraIndexEnum.NOT_SET:
                    { return; }

                case CameraIndexEnum.ALL:
                    {
                        foreach (IMil10BaseCamera cam in m_Mil10CameraDevices.Values)
                        {
                            if (null != cam)
                                cam.SaveImage(videoCommandPrmCaptureImage.TargetDirectory,
                                              videoCommandPrmCaptureImage.TargetFileName);
                        }

                        if (m_actionSyncManager.GetSyncObject("VideoSync") != null)
                            m_actionSyncManager.GetSyncObject("VideoSync").Set();
                        break;
                    }
                default:
                    {
                        if (null != m_Mil10CameraDevices[videoCommandPrmCaptureImage.CameraIndex])
                            m_Mil10CameraDevices[videoCommandPrmCaptureImage.CameraIndex].SaveImage(videoCommandPrmCaptureImage.TargetDirectory,
                                                                                                             videoCommandPrmCaptureImage.TargetFileName);
                        if (m_actionSyncManager.GetSyncObject("VideoSync") != null)
                            m_actionSyncManager.GetSyncObject("VideoSync").Set();
                        break;
                    }
            }


        }

        #endregion

        #region Grabbing methods

        protected void StopGrabbing()
        {
            foreach (IMil10BaseCamera cam in m_Mil10CameraDevices.Values)
            {
                if (null != cam)
                    cam.StopGrabbing();
            }
        }

        protected void StopGrabbing(VideoCommandEventParameters videoCommandEventParameters)
        {

            switch (videoCommandEventParameters.CameraIndex)
            {
                case CameraIndexEnum.NOT_SET:
                    { return; }

                case CameraIndexEnum.ALL:
                    {
                        foreach (IMil10BaseCamera cam in m_Mil10CameraDevices.Values)
                        {
                            if (null != cam)
                                cam.StopGrabbing();
                        }
                        break;
                    }

                default:
                    {
                        if (null != m_Mil10CameraDevices[videoCommandEventParameters.CameraIndex])
                            m_Mil10CameraDevices[videoCommandEventParameters.CameraIndex].StopGrabbing();
                        break;
                    }

            }
        }


        protected void StartGrabbing(VideoCommandPrmSetDisplayWindow videoCommandPrmSetDisplayWindow)
        {
            switch (videoCommandPrmSetDisplayWindow.CameraIndex)
            {
                case CameraIndexEnum.NOT_SET:
                    { return; }

                case CameraIndexEnum.ALL:
                    {
                        foreach (IMil10BaseCamera cam in m_Mil10CameraDevices.Values)
                        {
                            if (null != cam)
                                cam.StartGrabbing(videoCommandPrmSetDisplayWindow. DisplayWindowHandle);
                        }
                        break;
                    }

                default:
                    {
                        if (null != m_Mil10CameraDevices[videoCommandPrmSetDisplayWindow.CameraIndex])
                            m_Mil10CameraDevices[videoCommandPrmSetDisplayWindow.CameraIndex].StartGrabbing(videoCommandPrmSetDisplayWindow.DisplayWindowHandle );
                        break;
                    }

            }
        }

        protected void StartGrabbing()
        {
            foreach (IMil10BaseCamera cam in m_Mil10CameraDevices.Values)
            {
                if (null != cam)
                    cam.StartGrabbing();
            }
        }

         
        
        #endregion

        #region Matrox Parameters Methods

        protected void ReadLastUsageDataSetup(VideoCommandEventParameters videoCommandPrm)
        {
            switch (videoCommandPrm.CameraIndex)
            {
                case CameraIndexEnum.NOT_SET:
                    { return; }

                case CameraIndexEnum.ALL:
                    {
                        foreach (Mil10BaseCamera cam in m_Mil10CameraDevices.Values)
                        {
                            if (null != cam)
                                cam.ReadLastUsageDataSetup();
                        }
                        break;
                    }

                default:
                    {
                        if (null != m_Mil10CameraDevices[videoCommandPrm.CameraIndex])
                            m_Mil10CameraDevices[videoCommandPrm.CameraIndex].ReadLastUsageDataSetup();
                        break;
                    }
            }
        
        }

        private void ReleaseComponents(VideoCommandEventParameters videoCommandPrmReleaseComponents)
        {
            switch (videoCommandPrmReleaseComponents.CameraIndex)
            {
                case CameraIndexEnum.NOT_SET:
                    { return; }

                case CameraIndexEnum.ALL:
                    {
                        foreach (IMil10BaseCamera cam in m_Mil10CameraDevices.Values)
                        {
                            if (null != cam)
                                cam.ReleaseMyOwnedObjects();
                        }
                        break;
                    }

                default:
                    {
                        if (null != m_Mil10CameraDevices[videoCommandPrmReleaseComponents.CameraIndex])
                            m_Mil10CameraDevices[videoCommandPrmReleaseComponents.CameraIndex].ReleaseMyOwnedObjects();
                        break;
                    }

            }
        }
        

        //protected void UpdatePixel2MillimeterParameter(VideoCommandPrmUpdatePxl2MM videoCommandPrmUpdatePxl2MM)
        //{
        //    switch (videoCommandPrmUpdatePxl2MM.CameraIndex)
        //    {
        //        case CameraIndexEnum.NOT_SET:
        //            { return; }

        //        case CameraIndexEnum.ALL:
        //            {
        //                foreach (Mil9BaseCamera cam in m_Mil9CameraDevices.Values)
        //                {
        //                    if (null != cam)
        //                        cam.PixelToMillimter = videoCommandPrmUpdatePxl2MM.Pxl2MM;
        //                }
        //                break;
        //            }

        //        default:
        //            {
        //                if (null != m_Mil9CameraDevices[videoCommandPrmUpdatePxl2MM.CameraIndex])
        //                    m_Mil9CameraDevices[videoCommandPrmUpdatePxl2MM.CameraIndex].PixelToMillimter = videoCommandPrmUpdatePxl2MM.Pxl2MM;
        //                break;
        //            }
        //    }
        //}


        protected void UpdateGain(VideoCommandPrmUpdateGain videoCommandPrmUpdateGain)
        {
            switch (videoCommandPrmUpdateGain.CameraIndex)
            {
                case CameraIndexEnum.NOT_SET:
                    { return; }

                case CameraIndexEnum.ALL:
                    {
                        foreach (IMil10BaseCamera cam in m_Mil10CameraDevices.Values)
                        {
                            if (null != cam)
                                cam.Gain = videoCommandPrmUpdateGain.UpdateGainFactor;
                        }
                        break;
                    }

                default:
                    {
                        if (null != m_Mil10CameraDevices[videoCommandPrmUpdateGain.CameraIndex])
                            m_Mil10CameraDevices[videoCommandPrmUpdateGain.CameraIndex].Gain = videoCommandPrmUpdateGain.UpdateGainFactor;
                        break;
                    }

            }
        }

        protected void UpdateShutter(VideoCommandPrmUpdateShutter videoCommandPrmUpdateShutter)
        {
            switch (videoCommandPrmUpdateShutter.CameraIndex)
            {
                case CameraIndexEnum.NOT_SET:
                    { return; }

                case CameraIndexEnum.ALL:
                    {
                        foreach (IMil10BaseCamera cam in m_Mil10CameraDevices.Values)
                        {
                            if (null != cam)
                                cam.Shutter = videoCommandPrmUpdateShutter.UpdateShutterFactor;
                        }
                        break;
                    }

                default:
                    {
                        if (null != m_Mil10CameraDevices[videoCommandPrmUpdateShutter.CameraIndex])
                            m_Mil10CameraDevices[videoCommandPrmUpdateShutter.CameraIndex].Shutter = videoCommandPrmUpdateShutter.UpdateShutterFactor;
                        break;
                    }
            }
        }

        protected void UpdateImageCropRectangle(VideoCommandPrmUpdateImageCropRectangle videoCommandPrmUpdateImageCropRectangle)
        {
            switch (videoCommandPrmUpdateImageCropRectangle.CameraIndex)
            {
                case CameraIndexEnum.NOT_SET:
                    { return; }

                case CameraIndexEnum.ALL:
                    {
                        foreach (IMil10BaseCamera cam in m_Mil10CameraDevices.Values)
                        {
                            if (null != cam)
                                cam.UpdateImageCropRectangle(videoCommandPrmUpdateImageCropRectangle.CropRectangle.Left,
                                                                             videoCommandPrmUpdateImageCropRectangle.CropRectangle.Top,
                                                                             videoCommandPrmUpdateImageCropRectangle.CropRectangle.Width,
                                                                             videoCommandPrmUpdateImageCropRectangle.CropRectangle.Height);
                        }
                        break;
                    }

                default:
                    {
                        if (null != m_Mil10CameraDevices[videoCommandPrmUpdateImageCropRectangle.CameraIndex])
                            m_Mil10CameraDevices[videoCommandPrmUpdateImageCropRectangle.CameraIndex].UpdateImageCropRectangle(videoCommandPrmUpdateImageCropRectangle.CropRectangle.Left,
                                                                             videoCommandPrmUpdateImageCropRectangle.CropRectangle.Top,
                                                                             videoCommandPrmUpdateImageCropRectangle.CropRectangle.Width,
                                                                             videoCommandPrmUpdateImageCropRectangle.CropRectangle.Height);
                        break;
                    }
            }
        }




        protected void ResetZoomFactor(VideoCommandEventParameters videoCommandEventParameters)
        {
            switch (videoCommandEventParameters.CameraIndex)
            {
                case CameraIndexEnum.NOT_SET:
                    { return; }

                case CameraIndexEnum.ALL:
                    {
                        foreach (IMil10BaseCamera cam in m_Mil10CameraDevices.Values)
                        {
                            if (null != cam)
                                cam.ResetZoomFactor();
                        }
                        break;
                    }

                default:
                    {
                        if (null != m_Mil10CameraDevices[videoCommandEventParameters.CameraIndex])
                            m_Mil10CameraDevices[videoCommandEventParameters.CameraIndex].ResetZoomFactor();
                        break;
                    }
            }
        }


        protected void UpdateZoomFactor(VideoCommandPrmUpdateZoom videoCommandPrmUpdateZoom)
        {
            switch (videoCommandPrmUpdateZoom.CameraIndex)
            {
                case CameraIndexEnum.NOT_SET:
                    { return; }

                case CameraIndexEnum.ALL:
                    {
                        foreach (IMil10BaseCamera cam in m_Mil10CameraDevices.Values)
                        {
                            if (null != cam)
                                cam.UpdateZoomFactor(videoCommandPrmUpdateZoom.UpdateZoomFactor);
                        }
                        break;
                    }
                default:
                    {
                        if (null != m_Mil10CameraDevices[videoCommandPrmUpdateZoom.CameraIndex])
                            m_Mil10CameraDevices[videoCommandPrmUpdateZoom.CameraIndex].UpdateZoomFactor(videoCommandPrmUpdateZoom.UpdateZoomFactor);
                        break;
                    }
            }
        }

        protected void UpdatePanFactor(VideoCommandPrmUpdatePan videoCommandPrmUpdatePan)
        {
            switch (videoCommandPrmUpdatePan.CameraIndex)
            {
                case CameraIndexEnum.NOT_SET:
                    { return; }

                case CameraIndexEnum.ALL:
                    {
                        foreach (IMil10BaseCamera cam in m_Mil10CameraDevices.Values)
                        {
                            if (null != cam)
                                cam.UpdatePanScroll(videoCommandPrmUpdatePan.UpdateHorizontalPanFactor, videoCommandPrmUpdatePan.UpdateVerticalPanFactor);
                        }
                        break;
                    }
                default:
                    {
                        if (null != m_Mil10CameraDevices[videoCommandPrmUpdatePan.CameraIndex])
                            m_Mil10CameraDevices[videoCommandPrmUpdatePan.CameraIndex].UpdatePanScroll(videoCommandPrmUpdatePan.UpdateHorizontalPanFactor, videoCommandPrmUpdatePan.UpdateVerticalPanFactor);
                        break;
                    }
            }
        }


        protected void SetDisplayWindow(VideoCommandPrmSetDisplayWindow videoCommandPrmSetDisplayWindow)
        {
            switch (videoCommandPrmSetDisplayWindow.CameraIndex)
            {
                case CameraIndexEnum.NOT_SET:
                    { return; }

                case CameraIndexEnum.ALL:
                    {
                        foreach (IMil10BaseCamera cam in m_Mil10CameraDevices.Values)
                        {
                            if (null != cam)
                                cam.SetVideoWindow(videoCommandPrmSetDisplayWindow.DisplayWindowHandle);
                        }
                        break;
                    }
                default:
                    {
                        if (null != m_Mil10CameraDevices[videoCommandPrmSetDisplayWindow.CameraIndex])
                            m_Mil10CameraDevices[videoCommandPrmSetDisplayWindow.CameraIndex].SetVideoWindow(videoCommandPrmSetDisplayWindow.DisplayWindowHandle);
                        break;
                    }
            }
        }

        #endregion

        private void SendEvent(IEventData Event)
        {
            EventGroupData eventGroupData = new EventGroupData();
            eventGroupData.Enqueue(Event);

            RaiseEventGroup(eventGroupData);
        }
    }
}
