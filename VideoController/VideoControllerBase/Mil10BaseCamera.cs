﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SFW;
using Matrox.MatroxImagingLibrary;
using System.Threading;
using System.IO; 
using System.Drawing;
using NS_Video_Controller_Common.Interfaces;
using NS_Common;
using NS_Common.ViewModels.Common;
using NS_Video_Controller_Common.Service;
using System.Diagnostics;
using Navitar;

namespace NS_VideoControllerBase
{
    public class Mil10BaseCamera : ViewModelBase,IInputDevice, IMil10BaseCamera, IDisposable
    {
        #region protected Constant Values
            // Default image dimensions.
            protected const int DEFAULT_IMAGE_SIZE_X = 2452;
            protected const int DEFAULT_IMAGE_SIZE_Y = 2054;
            protected const int DEFAULT_IMAGE_SIZE_BAND = 3;
            static Object SynchronizationSaveImageObj = new object();

           
        
        #endregion

        #region Protected Members

        // Flag: Has Dispose already been called? 
        bool disposed = false;


        protected MIL_ID m_MilDisplay = MIL.M_NULL;      // MIL Display identifier.
        protected MIL_ID m_MilImage = MIL.M_NULL;        // MIL Image buffer identifier.
        protected MIL_ID m_MilDigitizer = MIL.M_NULL;    // MIL Digitizer identifier.        
        protected MIL_ID m_MilGrabbedImage = MIL.M_NULL;

        protected MIL_ID m_MilOverlayImage = MIL.M_NULL;
        protected MIL_ID m_MilDrawingImage = MIL.M_NULL;
        protected MIL_ID m_MilProcessingImage = MIL.M_NULL;
        protected MIL_ID m_MilColorGraphicsContext = MIL.M_NULL;


        protected MIL_ID m_MilFirstFocusImage = MIL.M_NULL;
        protected MIL_ID m_MilSecondFocusImage = MIL.M_NULL;
        protected MIL_ID m_MilThirdFocusImage = MIL.M_NULL;
        protected MIL_ID m_MilComposedFocusedImage = MIL.M_NULL;

        protected MIL_INT m_BufSizeX = DEFAULT_IMAGE_SIZE_X;
        protected MIL_INT m_BufSizeY = DEFAULT_IMAGE_SIZE_Y;
        protected MIL_INT m_BufSizeBand = DEFAULT_IMAGE_SIZE_BAND;

        protected long m_DefaultBufferAttributes;

        /// <summary>
        /// The reference to Store grabbed images thread. Used to store grabbed images
        /// on disk Or/And Message Queue
        /// </summary>
        protected static Thread StoreGrabbedImagesThread;
        protected CancellationTokenSource m_StoreGrabbedImagesTokenSource;

        
        #endregion

        #region Public Properties

        protected Mil10Base m_myBaseObjects;
        public Mil10Base myBaseObjects
        {
            get { return m_myBaseObjects; }
            protected set { m_myBaseObjects = value; }
        }


        protected CameraIndexEnum m_CameraIndex = CameraIndexEnum.NOT_SET;
        public CameraIndexEnum CameraIndex
        {
            get { return m_CameraIndex; }
            set { m_CameraIndex = value; }
        }

          

        /// <summary>
        /// Indicate if the system running in simulation mode,
        /// and not necessary  to raise error events
        /// </summary>
        protected Boolean m_SimulationMode = false;
        public Boolean SimulationMode
        {
            get { return m_SimulationMode; }
            protected set { m_SimulationMode = value; }
        }

         
        /// <summary>
        /// Used to refer window control from different than creation thread. ()
        /// </summary>
        protected IntPtr m_WindowHandle; 
        /// <summary>
        /// Set the active window to display grabbed images. Use Form.Handle in case of forms
        /// </summary>
        public IntPtr WindowHandle
        {
            get { return m_WindowHandle; }
            protected set
            {
                //m_DisplayForm = value;
                if (value != m_WindowHandle)
                {
                    m_WindowHandle = value;//.Handle;
                    RaisePropertyChanged("WindowHandle");
                }

            }
        }

        /// <summary>
        /// Indicate if the system shell start grabbing video on start
        /// </summary>
        protected Boolean m_GrabOnStart = true;
        public Boolean GrabOnStart
        {
            get { return m_GrabOnStart; }
            set { m_GrabOnStart = value; }
        }


        #region Camera properties

        protected long m_Shutter = 350;
        public long Shutter
        {
            get { return m_Shutter; }
            set
            {
                if (m_Shutter != value)
                {  
                    if (m_MilDigitizer != MIL.M_NULL)
                    {
                        m_Shutter = value;
                        MIL.MdigControl(m_MilDigitizer, MIL.M_SHUTTER, m_Shutter);
                        Tools.CustomRegistry.cRegistryFunc.SetRegistryFields(null, Tools.String_Enum.StringEnum.GetStringValue(m_CameraIndex), "Shutter", value);
                        RaisePropertyChanged("Shutter");
                    }
                    
                }
            }
        }
         
        protected long m_Gain = 0;
        public long Gain
        {
            get { return m_Gain; }
            set
            {
                if (m_Gain != value)
                {  
                    if (m_MilDigitizer != MIL.M_NULL)
                    {
                        m_Gain = value;
                        MIL.MdigControl(m_MilDigitizer, MIL.M_GAIN, m_Gain);
                        Tools.CustomRegistry.cRegistryFunc.SetRegistryFields(null, Tools.String_Enum.StringEnum.GetStringValue(m_CameraIndex), "Gain", value);
                        RaisePropertyChanged("Gain");
                    }
                }
            }
        }

        private int m_FocusValue;

        public int FocusValue
        {
            get { return m_FocusValue; }
            set {
                if (m_FocusValue != value)
                {
                    if (m_MilDigitizer != MIL.M_NULL)
                    {
                        m_FocusValue = value;

                        RaisePropertyChanged("FocusValue");
                    }
                }
            }
        }
        

        protected Rectangle m_ImageCropRectangle = new Rectangle(0, 0, DEFAULT_IMAGE_SIZE_X, DEFAULT_IMAGE_SIZE_Y);// {, new Rectangle(450, 1100, 1500, 600) };   
        public virtual Rectangle ImageCropRectangle
        {
            get { return m_ImageCropRectangle; }
            protected set
            {
                //if (m_ImageCropRectangle.Left != value.Left || m_ImageCropRectangle.Top != value.Top ||
                //    m_ImageCropRectangle.Width != value.Width || m_ImageCropRectangle.Height != value.Height)
                //{
                m_ImageCropRectangle = value;
                if (m_MilImage != MIL.M_NULL)
                {
                    MIL.MbufFree(m_MilImage);
                    m_MilImage = MIL.M_NULL;
                }
                MIL.MbufChild2d(m_MilGrabbedImage, m_ImageCropRectangle.Left, m_ImageCropRectangle.Top, m_ImageCropRectangle.Width, m_ImageCropRectangle.Height, ref m_MilImage);
                RaisePropertyChanged("ImageCropRectangle");
                //}
            }
        }


        /// <summary>
        ///Local copy of parameter for the Pixel To Millimeter
        /// </summary>
        protected Double m_PixelToMillimter = 1;
        /// <summary>
        ///Specifies the Pixel To Millimeter Coefficient.
        /// </summary>
        public Double PixelToMillimter
        {
            get { return m_PixelToMillimter; }
            set
            {
                if (m_PixelToMillimter != value)
                {
                    m_PixelToMillimter = value;
                    RaisePropertyChanged("PixelToMillimter");
                }
            }
        }

        private Single m_VerticalPan;
        public Single VerticalPan
        {
            get { return m_VerticalPan; }
            set 
            {
                if (value != m_VerticalPan)
                {
                    m_VerticalPan = value;
                    Tools.CustomRegistry.cRegistryFunc.SetRegistryFields(null, Tools.String_Enum.StringEnum.GetStringValue(m_CameraIndex), "VerticalPan", value);
                }
            
            }
        }

        private Single m_HorisontalPan;
        public Single HorisontalPan
        {
            get { return m_HorisontalPan; }
            set
            {
                if (value != m_HorisontalPan)
                {
                    m_HorisontalPan = value;
                    Tools.CustomRegistry.cRegistryFunc.SetRegistryFields(null, Tools.String_Enum.StringEnum.GetStringValue(m_CameraIndex), "HorisontalPan", value);
                }

            }
        }

        

        protected Single m_ZoomFactor = 1;
        public Single ZoomFactor
        {
            get { return m_ZoomFactor; }
            set
            {
                if (value != m_ZoomFactor)
                {
                    m_ZoomFactor = value;
                    MIL.MdispZoom(m_MilDisplay, Convert.ToDouble(m_ZoomFactor), Convert.ToDouble(m_ZoomFactor));
                    Tools.CustomRegistry.cRegistryFunc.SetRegistryFields(null, Tools.String_Enum.StringEnum.GetStringValue(m_CameraIndex), "ZoomFactor", value);
                }    
            }
        }

        #endregion


        protected String m_DefaultTargetPath;
        /// <summary>
        /// Get/Set the Target Path.
        /// </summary>
        public String DefaultTargetPath
        {
            get { return m_DefaultTargetPath; }
            set { m_DefaultTargetPath = value; }
        }

        protected Int32 m_SavingTimeout = 30000;
        public Int32 SavingTimeout
        {
            get { return m_SavingTimeout; }
            set { m_SavingTimeout = value; }
        }

        #endregion

        #region Constructors

        public Mil10BaseCamera(CameraIndexEnum cam_index, Mil10Base base_objects) : this(cam_index, base_objects, 0, "M_DEFAULT") { }

        public  Mil10BaseCamera(CameraIndexEnum cam_index,
                                Mil10Base base_objects  , 
                                Int32 digitizer_number ,
                                String digitizer_name  )
        {
            m_SimulationMode = base_objects.SimulationMode;
            m_DefaultTargetPath = @"C:\Medinol\Images\";
            m_myBaseObjects = base_objects;
            m_CameraIndex = cam_index;

            if (!m_SimulationMode)
            {
                #region Initialize camera Matrox Classes
                    // Allocate a MIL display.
                    MIL.MdispAlloc(base_objects.MilSystem, MIL.M_DEFAULT, "M_DEFAULT", MIL.M_WINDOWED, ref m_MilDisplay);

                    MIL_INT  number_of_availible_digitizers = MIL.MsysInquire(base_objects.MilSystem, MIL.M_DIGITIZER_NUM, MIL.M_NULL);
                    if (number_of_availible_digitizers > 0)
                    {
                        MIL.MdigAlloc(base_objects.MilSystem, digitizer_number, digitizer_name,MIL.M_DEFAULT, ref m_MilDigitizer);
                    }

                    // Allocate a MIL buffer.
                    m_DefaultBufferAttributes = MIL.M_IMAGE + MIL.M_DISP + MIL.M_PROC;
                    if (m_MilDigitizer != MIL.M_NULL)
                    {
                        // Add M_GRAB attribute if a digitizer is allocated.
                        m_DefaultBufferAttributes |= MIL.M_GRAB;
                    }

                    MIL.MbufAlloc2d(base_objects.MilSystem, DEFAULT_IMAGE_SIZE_X, DEFAULT_IMAGE_SIZE_Y, 8 + MIL.M_UNSIGNED, m_DefaultBufferAttributes, ref m_MilImage);

                    MIL.MbufAlloc2d(base_objects.MilSystem, DEFAULT_IMAGE_SIZE_X, DEFAULT_IMAGE_SIZE_Y, 8 + MIL.M_UNSIGNED, m_DefaultBufferAttributes, ref m_MilGrabbedImage);

                    MIL.MbufAlloc2d(base_objects.MilSystem, DEFAULT_IMAGE_SIZE_X, DEFAULT_IMAGE_SIZE_Y, 8 + MIL.M_UNSIGNED, m_DefaultBufferAttributes, ref m_MilProcessingImage);

                    MIL.MbufAlloc2d(m_myBaseObjects.MilSystem, DEFAULT_IMAGE_SIZE_X, DEFAULT_IMAGE_SIZE_Y, 8 + MIL.M_UNSIGNED, m_DefaultBufferAttributes, ref m_MilFirstFocusImage);

                    MIL.MbufAlloc2d(m_myBaseObjects.MilSystem, DEFAULT_IMAGE_SIZE_X, DEFAULT_IMAGE_SIZE_Y, 8 + MIL.M_UNSIGNED, m_DefaultBufferAttributes, ref m_MilSecondFocusImage);

                    MIL.MbufAlloc2d(m_myBaseObjects.MilSystem, DEFAULT_IMAGE_SIZE_X, DEFAULT_IMAGE_SIZE_Y, 8 + MIL.M_UNSIGNED, m_DefaultBufferAttributes, ref m_MilThirdFocusImage);

                    MIL.MbufAlloc2d(m_myBaseObjects.MilSystem, DEFAULT_IMAGE_SIZE_X, DEFAULT_IMAGE_SIZE_Y, 8 + MIL.M_UNSIGNED, m_DefaultBufferAttributes, ref m_MilComposedFocusedImage);

                    // Clear the buffer.
                    MIL.MbufClear(m_MilImage, 0);

                    // Clear the buffer.
                    MIL.MbufClear(m_MilGrabbedImage, 0);

                    // Clear the buffer.
                    MIL.MbufClear(m_MilProcessingImage, 0);

                    long Attributes = MIL.M_IMAGE + MIL.M_DISP + MIL.M_DIB + MIL.M_GDI;

                    MIL.MbufAllocColor(base_objects.MilSystem, m_BufSizeBand, m_BufSizeX, m_BufSizeY, 8 + MIL.M_UNSIGNED, Attributes, ref m_MilDrawingImage);

                    MIL.MgraAlloc(base_objects.MilSystem, ref m_MilColorGraphicsContext);
                    // Clear the buffer.
                    MIL.MbufClear(m_MilDrawingImage, 0);
                #endregion
            }
        }

        ~Mil10BaseCamera()
        {
            if (!m_SimulationMode)
            {
                Dispose(false);
            }
        }
        #endregion

        #region Protected Methods

        /// <summary>
        ///Public implementation of Dispose pattern callable by consumers.  
        /// </summary>
        public virtual void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///  Protected implementation of Dispose pattern. 
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
               // managed area releases
                ReleaseMyOwnedObjects();
            }

            //unmanaged area releases
            //if (_mtx != null) _mtx.Dispose();

            disposed = true;
        } 

        public virtual void ReleaseMyOwnedObjects()
        {
            FreeMatroxReferences();
        }

        protected virtual void FreeMatroxReferences()
        {
            if (!m_SimulationMode)
            {
                #region Free Default Matrox references
                if (m_MilImage != MIL.M_NULL)
                {
                    //// Clear the buffer.
                    MIL.MbufClear(m_MilImage, 0);
                    // Free allocated objects.
                    MIL.MbufFree(m_MilImage);
                    m_MilImage = MIL.M_NULL;
                }

                if (m_MilProcessingImage != MIL.M_NULL)
                {
                    // Clear the buffer.
                    MIL.MbufClear(m_MilProcessingImage, 0);
                    MIL.MbufFree(m_MilProcessingImage);

                    m_MilProcessingImage = MIL.M_NULL;
                }

                if (m_MilDrawingImage != MIL.M_NULL)
                {
                    //// Clear the buffer.
                    MIL.MbufClear(m_MilDrawingImage, 0);
                    MIL.MbufFree(m_MilDrawingImage);
                    m_MilDrawingImage = MIL.M_NULL;
                }

                if (m_MilDigitizer > 0)
                {
                    MIL.MdigFree(m_MilDigitizer);
                    m_MilDigitizer = MIL.M_NULL;
                }


                if (m_MilDisplay != MIL.M_NULL)
                {
                    MIL.MdispFree(m_MilDisplay);
                    m_MilDisplay = MIL.M_NULL;
                }

                if(m_MilColorGraphicsContext != MIL.M_NULL)
                {
                    MIL.MgraFree(m_MilColorGraphicsContext);
                    m_MilColorGraphicsContext = MIL.M_NULL;
                }

                if(m_MilFirstFocusImage !=MIL.M_NULL)
                {
                    MIL.MbufFree(m_MilFirstFocusImage);
               
                }

                if (m_MilSecondFocusImage != MIL.M_NULL)
                {
                  
                    MIL.MbufFree(m_MilSecondFocusImage);
                   
                }

                if (m_MilThirdFocusImage != MIL.M_NULL)
                {
                 
                    MIL.MbufFree(m_MilThirdFocusImage);
                }

                if(m_MilComposedFocusedImage != MIL.M_NULL)
                {
                    MIL.MbufFree(m_MilComposedFocusedImage);
                }
               

                #endregion
            }
        }

        #region Registry Methods
        

        
        #endregion


        #endregion

        #region Public Members

        public virtual void StartGrabbing()
        {
            if (m_MilDigitizer != MIL.M_NULL)
            {
                MIL_INT is_grabbing = MIL.M_NO;
                MIL.MdigInquire(m_MilDigitizer, MIL.M_GRAB_IN_PROGRESS, ref is_grabbing);
                if (MIL.M_NO == is_grabbing) // Grab continuously.
                    MIL.MdigGrabContinuous(m_MilDigitizer, m_MilGrabbedImage);
            }
        }

        public virtual void StartGrabbing(IntPtr display_form_handle)
        {
            SetVideoWindow(display_form_handle);
            if (m_MilDigitizer != MIL.M_NULL)
            {
                //Wait while additional initialization thread complete
                System.Threading.Thread.Sleep(100);   
                MIL_INT is_grabbing = MIL.M_NO;
                MIL.MdigInquire(m_MilDigitizer, MIL.M_GRAB_IN_PROGRESS, ref is_grabbing);
                if (MIL.M_NO == is_grabbing) // Grab continuously.
                    MIL.MdigGrabContinuous(m_MilDigitizer, m_MilGrabbedImage);
            }
        }

        public virtual void StopGrabbing()
        {
            if (m_MilDigitizer != MIL.M_NULL)
            {
                MIL_INT is_grabbing = MIL.M_NO;
                MIL.MdigInquire(m_MilDigitizer, MIL.M_GRAB_IN_PROGRESS, ref is_grabbing);
                if (MIL.M_YES == is_grabbing)
                    MIL.MdigHalt(m_MilDigitizer);
            }
        }


        public void DrawCross(int cirSize,int linethickness)
        {


            MIL.MgraColor(m_MilColorGraphicsContext, MIL.M_COLOR_LIGHT_GRAY);
            for (int i = 0; i < linethickness; i++)
            {
                MIL.MgraLine(m_MilColorGraphicsContext, m_MilOverlayImage, 0, m_ImageCropRectangle.Height / 2 + i, m_ImageCropRectangle.Width, m_ImageCropRectangle.Height / 2 + i);
                MIL.MgraLine(m_MilColorGraphicsContext, m_MilOverlayImage, m_ImageCropRectangle.Width / 2 + i, 0, m_ImageCropRectangle.Width / 2 + i, m_ImageCropRectangle.Height + i);

                MIL.MgraLine(m_MilColorGraphicsContext, m_MilOverlayImage, 0, m_ImageCropRectangle.Height / 2 - i, m_ImageCropRectangle.Width, m_ImageCropRectangle.Height / 2 - i);
                MIL.MgraLine(m_MilColorGraphicsContext, m_MilOverlayImage, m_ImageCropRectangle.Width / 2 - i, 0, m_ImageCropRectangle.Width / 2 - i, m_ImageCropRectangle.Height - i);

            }

            for (int i = 0; i < linethickness; i++)
            {
                MIL.MgraArc(m_MilColorGraphicsContext, m_MilOverlayImage, m_ImageCropRectangle.Width / 2, m_ImageCropRectangle.Height / 2, (double)cirSize / 2 + i, (double)cirSize / 2 + i, 0, 360);
                MIL.MgraArc(m_MilColorGraphicsContext, m_MilOverlayImage, m_ImageCropRectangle.Width / 2, m_ImageCropRectangle.Height / 2, (double)cirSize / 2 - i, (double)cirSize / 2 - i, 0, 360);
            }           
            

        }

        public virtual void SetVideoWindow(IntPtr display_form_handle)
        {
            if (!m_SimulationMode)
            {
                #region Initialize Display Matrox Form


                // Select the MIL buffer to be displayed in the user-specified window.
                MIL.MdispSelectWindow(m_MilDisplay, m_MilImage, display_form_handle);
                

                MIL.MdispInquire(m_MilDisplay, MIL.M_OVERLAY_ID, ref m_MilOverlayImage);

                // Grab in the user window if supported.
                if (m_MilDigitizer != MIL.M_NULL && m_GrabOnStart)
                {
                    // Grab continuously.
                    MIL.MdigGrabContinuous(m_MilDigitizer, m_MilGrabbedImage);
                }


                #endregion
            }
        }

   

        #region Automatic saving grabbed images thread functions

        public virtual void StopImageSavingThread()
        {
            Int32 iCount = 5;
            if (null != m_StoreGrabbedImagesTokenSource)
            {
                m_StoreGrabbedImagesTokenSource.Cancel();
                while (--iCount > 0)
                {
                    System.Threading.Thread.Sleep(100);
                }
            }
        }

        public virtual void StartImageSavingThread()
        {
            m_StoreGrabbedImagesTokenSource = new CancellationTokenSource();
            StoreGrabbedImagesThread = new Thread(new ParameterizedThreadStart(StoreGrabbedImageThreadLoop));
            StoreGrabbedImagesThread.IsBackground = true;
            StoreGrabbedImagesThread.SetApartmentState(ApartmentState.STA);
            StoreGrabbedImagesThread.Name = "Mil 9 Device - Store Grabbed Images Thread";
            StoreGrabbedImagesThread.Priority = ThreadPriority.Lowest;
            StoreGrabbedImagesThread.Start(m_StoreGrabbedImagesTokenSource.Token);
        }

        protected virtual void StoreGrabbedImageThreadLoop(object token)
        {
            try
            {
                while (true)
                {
                    if (((CancellationToken)token).IsCancellationRequested) return;
                    lock (SynchronizationSaveImageObj)
                    {
                        try
                        {
                            MIL.MbufExport(Path.Combine(m_DefaultTargetPath, Tools.String_Enum.StringEnum.GetStringValue(m_CameraIndex) + "_" +
                                                                                Convert.ToString(DateTime.Now.Hour) + "_" +
                                                                             Convert.ToString(DateTime.Now.Minute) + "_" +
                                                                             Convert.ToString(DateTime.Now.Second) + ".jpg"),
                                           MIL.M_JPEG_LOSSY,
                                           m_MilImage);
                        }
                        catch { }
                    }
                    if (((CancellationToken)token).IsCancellationRequested) return;
                    System.Threading.Thread.Sleep(m_SavingTimeout);
                }
            }
            catch (ThreadAbortException exc)
            {

                Console.WriteLine("Thread aborting, code is " +
                 exc.ExceptionState);
            }
        }
         
        #endregion

        public virtual void UpdateImageCropRectangle(Int32 image_crop_rectangle_left,
                                                     Int32 image_crop_rectangle_top,
                                                     Int32 image_crop_rectangle_width,
                                                     Int32 image_crop_rectangle_height)
        {

            ImageCropRectangle = new Rectangle(image_crop_rectangle_left, image_crop_rectangle_top, image_crop_rectangle_width, image_crop_rectangle_height);
        }


        public virtual void UpdateZoomFactor(float update_zoom_factor)
        {
            ZoomFactor += update_zoom_factor;

            
        }

        public virtual void ResetZoomFactor()
        {
            ZoomFactor = 1;

            //MIL.MdispZoom(m_MilDisplay, m_ZoomFactor, m_ZoomFactor);
        }

        public virtual void UpdatePanScroll(Single update_horizontal_pan_factor, Single update_vertical_pan_factor)
        {
            VerticalPan = update_vertical_pan_factor;
            HorisontalPan = update_horizontal_pan_factor;
            MIL.MdispPan(m_MilDisplay, update_horizontal_pan_factor, update_vertical_pan_factor);
        }


       

        public virtual int CaptureAndSaveImage(string target_path, string target_file_name)
        {

            //return CaptureAndSaveImage(target_path, target_file_name, new RectangleF(0, 0, 1000, 1000)); // Screen.PrimaryScreen.WorkingArea.Width, Screen.PrimaryScreen.WorkingArea.Height));

            try
            {
                if (!m_SimulationMode)
                {
                    try
                    {
                        MIL.MdigGrab(m_MilDigitizer, m_MilGrabbedImage);
                        MIL.MbufExport(Path.Combine(target_path, target_file_name),
                                                           MIL.M_JPEG_LOSSY,
                                                           m_MilImage);
                        return ErrorCodesList.OK;
                    }
                    catch
                    { return ErrorCodesList.FAILED; }
                }
                return ErrorCodesList.OK; 
            }
            catch
            {
                return ErrorCodesList.FAILED;
            }

        }

        public virtual int SaveImage(string target_path, string target_file_name)
        {

            //return CaptureAndSaveImage(target_path, target_file_name, new RectangleF(0, 0, 1000, 1000)); // Screen.PrimaryScreen.WorkingArea.Width, Screen.PrimaryScreen.WorkingArea.Height));

            try
            {
                if (!m_SimulationMode)
                {
                    try
                    {
                       
                        MIL.MbufExport(Path.Combine(target_path, target_file_name),
                                                           MIL.M_JPEG_LOSSY,
                                                           m_MilComposedFocusedImage);
                        return ErrorCodesList.OK;
                    }
                    catch
                    { return ErrorCodesList.FAILED; }
                }
                return ErrorCodesList.OK;
            }
            catch
            {
                return ErrorCodesList.FAILED;
            }

        }


        public  virtual int CaptureMultiFocusImage(string target_path, string target_file_name ,List<Rectangle> FocusArea, int numOfSteps,int baseFocusPosition,int FocusStep)
        {
            List<int> MaxFocusResults = new List<int>();
            List<int> MaxFocusIndexList = new List<int>();
            List<int> TempFocuslist = new List<int>();
           MIL_ID[,] SectionImageList = new MIL_ID[FocusArea.Count(),numOfSteps];

            foreach (Rectangle item in FocusArea)
            {
                MaxFocusResults.Add(0);
                    MaxFocusIndexList.Add(0);
                MIL_ID tempID;
               // SectionImageList.Add(tempID);
             //   MIL.MbufAlloc2d(m_myBaseObjects.MilSystem, item.Width, item.Height, 8 + MIL.M_UNSIGNED, MIL.M_IMAGE + MIL.M_PROC, ref tempID);
            }
            try
            {
                Navitar.ControllerGen2 myControler = (ControllerGen2)Navitar.ControllerHub.GetAll().First();

                var res = myControler.Read(ControllerGen2.regCurrent_2);

                for (int i = 0; i < numOfSteps; i++)
                {
                    myControler.Write(ControllerGen2.regTarget_2, baseFocusPosition + i * FocusStep);
                    do
                    {
                        Thread.Sleep(50);
                    } while (myControler.Read(ControllerGen2.regCurrent_2) != baseFocusPosition + i * FocusStep);

                    TempFocuslist = MILComputeMultiFocus(FocusArea,i);
                    for (int j = 0; j < MaxFocusResults.Count; j++)
                    {
                        if (TempFocuslist[j] > MaxFocusResults[j])
                        {
                            MaxFocusResults[i] = TempFocuslist[j];
                            MaxFocusIndexList[j] = i+1;
                        }
                    }

                   

                }

                CreateComposedFocusedImage(MaxFocusIndexList, FocusArea);

                MIL.MbufExport(Path.Combine(target_path, target_file_name),
                                                          MIL.M_JPEG_LOSSY,
                                                          m_MilComposedFocusedImage);
               

                return ErrorCodesList.OK; 
            }
            catch 
            {

                return ErrorCodesList.FAILED; 
            }
        }
     

        public virtual int MILComputeFocus(List<Rectangle> FocusArea)
        {
            MIL_ID FocusImage = MIL.M_NULL;
            //MIL_ID FocusImage2 = MIL.M_NULL;
            //MIL_ID FocusImage3 = MIL.M_NULL;
            //MIL_ID FocusImage4 = MIL.M_NULL;
            // left
          //  MIL.MbufChild2d(m_MilGrabbedImage, FocusArea[0].Left, FocusArea[0].Top, FocusArea[0].Width, FocusArea[0].Height, ref FocusImage);
            // top
            //MIL.MbufChild2d(m_MilGrabbedImage, FocusArea[1].Left, FocusArea[1].Top, FocusArea[1].Width, FocusArea[1].Height, ref FocusImage2);
            ////bottom
            //MIL.MbufChild2d(m_MilGrabbedImage, FocusArea[2].Left, FocusArea[2].Top, FocusArea[2].Width, FocusArea[2].Height, ref FocusImage3);
            //// right
            //MIL.MbufChild2d(m_MilGrabbedImage, FocusArea[3].Left, FocusArea[3].Top, FocusArea[3].Width, FocusArea[3].Height, ref FocusImage4);

            MIL_INT res=0,res2=0,res3=0,res4 = 0;

            var IsGrab = MIL.MdigInquire(m_MilDigitizer, MIL.M_GRAB_IN_PROGRESS);
            if (!(IsGrab == 1))
                MIL.MdigGrab(m_MilDigitizer, m_MilGrabbedImage);

            MIL.MdigFocus(MIL.M_NULL, m_MilGrabbedImage, MIL.M_DEFAULT, MoveLensHookFunction, MIL.M_NULL, 0, 0, 0, 0, MIL.M_EVALUATE + MIL.M_NO_FILTER + MIL.M_NO_SUBSAMPLING, ref res);
            //MIL.MdigFocus(MIL.M_NULL, FocusImage2, MIL.M_DEFAULT, MoveLensHookFunction, MIL.M_NULL, 0, 0, 0, 0, MIL.M_EVALUATE + MIL.M_NO_FILTER + MIL.M_NO_SUBSAMPLING, ref res2);
            //MIL.MdigFocus(MIL.M_NULL, FocusImage3, MIL.M_DEFAULT, MoveLensHookFunction, MIL.M_NULL, 0, 0, 0, 0, MIL.M_EVALUATE + MIL.M_NO_FILTER + MIL.M_NO_SUBSAMPLING, ref res3);

            //MIL.MdigFocus(MIL.M_NULL, FocusImage4, MIL.M_DEFAULT, MoveLensHookFunction, MIL.M_NULL, 0, 0, 0, 0, MIL.M_EVALUATE + MIL.M_NO_FILTER + MIL.M_NO_SUBSAMPLING, ref res4);


          //  MIL.MbufFree(FocusImage);
            //MIL.MbufFree(FocusImage2);
            //MIL.MbufFree(FocusImage3);
            //MIL.MbufFree(FocusImage4);
            foreach (Rectangle item in FocusArea)
            {
                for (int i = 0; i < 3; i++)
                {
                    MIL.MgraRect(m_MilColorGraphicsContext, m_MilOverlayImage, item.Left + i, item.Top + i, item.Left + i + item.Width - 2 * i, item.Top + i + item.Height - 2 * i);


                }
            }
            string rr = "Results: 1 -" + res.ToString();//+ " 2 -" + res2.ToString() + " 3 -" + res3.ToString() + " 4 -" + res4.ToString();
            Debug.WriteLine(rr.ToString());
            
           // return (int)((res+res2+res3+res4)/4);

           // return (int)(( res3 + res4) / 2);
            return (int)(res);
        }


        public virtual List<int> MILComputeMultiFocus(List<Rectangle> FocusArea, int MultiFocusStage)
        {
           
            List<MIL_ID> ChildList = new List<MIL_ID>();
            List<int> FocusList = new List<int>();
            foreach (Rectangle item in FocusArea)
            {
                MIL_ID TempFocusImage = MIL.M_NULL;
                MIL.MbufChild2d(m_MilGrabbedImage, item.Left, item.Top, item.Width, item.Height, ref TempFocusImage);
                ChildList.Add(TempFocusImage);
            }


            var IsGrab = MIL.MdigInquire(m_MilDigitizer, MIL.M_GRAB_IN_PROGRESS);
            if (!(IsGrab == 1))
                MIL.MdigGrab(m_MilDigitizer, m_MilGrabbedImage);

            foreach (MIL_ID item in ChildList)
            {
                MIL_INT res = 0;
                MIL.MdigFocus(MIL.M_NULL, item, MIL.M_DEFAULT, MoveLensHookFunction, MIL.M_NULL, 0, 0, 0, 0, MIL.M_EVALUATE + MIL.M_NO_FILTER + MIL.M_NO_SUBSAMPLING, ref res);
                MIL.MbufFree(item);
                FocusList.Add((int)res);
            }


            string outputstring = "Results: ";
            int j = 0;
            foreach (Rectangle item in FocusArea)
            {
                for (int i = 0; i < 3; i++)
                {
                    MIL.MgraRect(m_MilColorGraphicsContext, m_MilOverlayImage, item.Left + i, item.Top + i, item.Left + i + item.Width - 2 * i, item.Top + i + item.Height - 2 * i);
                    outputstring += j.ToString() + " : " + FocusList[j].ToString() + " ";

                }
                j++;
            }

            Debug.WriteLine(outputstring);
            switch (MultiFocusStage)
            {
                case 1:
                   // if (m_MilFirstFocusImage ==0)
                      
                    MIL.MbufCopy(m_MilGrabbedImage, m_MilFirstFocusImage);
                    MIL.MbufSave("c:\\firstimage.tif", m_MilFirstFocusImage);
                    break;

                case 2:
                 //   if (m_MilSecondFocusImage == 0)
                 //       MIL.MbufAlloc2d(m_myBaseObjects.MilSystem, DEFAULT_IMAGE_SIZE_X, DEFAULT_IMAGE_SIZE_Y, 8 + MIL.M_UNSIGNED, m_DefaultBufferAttributes, ref m_MilSecondFocusImage);
                    MIL.MbufCopy(m_MilGrabbedImage, m_MilSecondFocusImage);
                    MIL.MbufSave("c:\\secondimage.tif", m_MilSecondFocusImage);
                    break;

                case 3:
                //    if (m_MilThirdFocusImage == 0)
               //         MIL.MbufAlloc2d(m_myBaseObjects.MilSystem, DEFAULT_IMAGE_SIZE_X, DEFAULT_IMAGE_SIZE_Y, 8 + MIL.M_UNSIGNED, m_DefaultBufferAttributes, ref m_MilThirdFocusImage);
                    MIL.MbufCopy(m_MilGrabbedImage, m_MilThirdFocusImage);
                    MIL.MbufSave("c:\\thirdimage.tif", m_MilThirdFocusImage);
                    break;
                case 4:

                    break;
            }
            return FocusList;
        }

        public virtual void CreateComposedFocusedImage(List<int> MaxFocusIndexList, List<Rectangle> FocusAreaList)
        {
           
           
            int i = 0;
            
             MIL.MbufCopy(m_MilGrabbedImage, m_MilComposedFocusedImage);
            foreach (Rectangle item in FocusAreaList)
            {
                MIL_ID TempFocusImage = MIL.M_NULL;
                MIL_ID Tempimg = MIL.M_NULL;
                if (MaxFocusIndexList[i] == 0)
                    Tempimg = m_MilFirstFocusImage;
                if (MaxFocusIndexList[i] ==1)
                    Tempimg = m_MilSecondFocusImage;
                if (MaxFocusIndexList[i] == 2)
                    Tempimg = m_MilThirdFocusImage;

               

                MIL.MbufCopyColor2d(Tempimg, m_MilComposedFocusedImage, MIL.M_ALL_BANDS, item.Left, item.Top, MIL.M_ALL_BANDS, item.Left, item.Top, item.Width, item.Height);
                           
                i++;
            }
          
           
        }

        static MIL_INT MoveLensHookFunction(MIL_INT HookType, MIL_INT Position, IntPtr UserDataHookPtr)
        {
            return 0;
        }

        public virtual int CaptureAndSaveImage(string target_path, string target_file_name, RectangleF rectangle)
        {

            try
            {
                if (!m_SimulationMode)
                {
                    try
                    {
                        MIL.MdigGrab(m_MilDigitizer, m_MilGrabbedImage);
                        MIL.MbufExport(Path.Combine(target_path, target_file_name),
                                                           MIL.M_JPEG_LOSSY,
                                                           m_MilImage);
                        return ErrorCodesList.OK;
                    }
                    catch
                    { return ErrorCodesList.FAILED; }
                }
                else
                {

                    using (Bitmap bmpScreenCapture = new Bitmap((int)rectangle.Width,
                                                    (int)rectangle.Height))
                    {
                        using (Graphics g = Graphics.FromImage(bmpScreenCapture))
                        {
                            g.CopyFromScreen((int)rectangle.X,
                                                (int)rectangle.Y,
                                                0, 0,
                                                bmpScreenCapture.Size,
                                                CopyPixelOperation.SourceCopy);
                        }
                        String FileName = Path.Combine(target_path, target_file_name);
                        bmpScreenCapture.Save(FileName);
                        return ErrorCodesList.OK;
                    }
                }
            }
            catch
            {
                return ErrorCodesList.FAILED;
            }

        }

        public virtual void ReadLastUsageDataSetup()
        {
            Shutter = Tools.CustomRegistry.cRegistryFunc.GetRegistryFields(null, Tools.String_Enum.StringEnum.GetStringValue(m_CameraIndex), "Shutter", m_Shutter);
            Gain = Tools.CustomRegistry.cRegistryFunc.GetRegistryFields(null, Tools.String_Enum.StringEnum.GetStringValue(m_CameraIndex), "Gain", m_Gain );


            VerticalPan = Tools.CustomRegistry.cRegistryFunc.GetRegistryFields(null, Tools.String_Enum.StringEnum.GetStringValue(m_CameraIndex), "VerticalPan", m_VerticalPan);
            HorisontalPan = Tools.CustomRegistry.cRegistryFunc.GetRegistryFields(null, Tools.String_Enum.StringEnum.GetStringValue(m_CameraIndex), "HorisontalPan", m_HorisontalPan);

            ZoomFactor = Tools.CustomRegistry.cRegistryFunc.GetRegistryFields(null, Tools.String_Enum.StringEnum.GetStringValue(m_CameraIndex), "ZoomFactor", m_ZoomFactor);
            
        }


        #endregion

        #region IInputDevice Members

        public event DataEventHandler DataReceived;

        #endregion IInputDevice Members

        #region IOutputDevice Members

        public void Send(string data)
        {
            throw new NotImplementedException();
        }

        #endregion IOutputDevice Members

        #region IComponent Members

        public string Name
        {
            get { return "Mil 9 Base Camera Device"; }
        }

        #endregion IComponent Members


         
    }
}
