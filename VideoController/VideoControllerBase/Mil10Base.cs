﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using Tools.CustomRegistry;
using Matrox.MatroxImagingLibrary;
using log4net;
using SFW;
using System.ComponentModel;
using System.IO;
using System.Threading;
using NS_Common;
using AVI4.Common.DataHolders;
using Navitar;

namespace NS_VideoControllerBase
{
    public class Mil10Base : NS_Video_Controller_Common.Interfaces.IMil10Base, IDisposable, SFW.IComponent 
    {
        #region Protected Members

        // Flag: Has Dispose already been called? 
        protected bool disposed = false;

        #endregion

        #region Public Properties

       

        protected ILog m_MatroxImageLog;
        public ILog MatroxImageLog
        {
            get { return m_MatroxImageLog; }
            private set { m_MatroxImageLog = value; }
        }

        protected VIDEO_TYPES m_VideoType;

        public VIDEO_TYPES VideoType
        {
            get { return m_VideoType; }
            set { m_VideoType = value; }
        }
        

        // MIL variables
        protected MIL_ID m_MilApplication = MIL.M_NULL;  // MIL Application identifier.
        public MIL_ID MilApplication
        {
            get { return m_MilApplication; }
            protected set { m_MilApplication = value; }
        }

        protected MIL_ID m_MilSystem = MIL.M_NULL;       // MIL System identifier.
        public MIL_ID MilSystem
        {
            get { return m_MilSystem; }
            protected set { m_MilSystem = value; }
        }
        


        /// <summary>
        /// Indicate if the system running in simulation mode,
        /// and not necessary  to raise error events
        /// </summary>
        protected Boolean m_SimulationMode = false;
        public Boolean SimulationMode
        {
            get { return m_SimulationMode; }
            protected set { m_SimulationMode = value; }
        }


        protected int m_TheNumberOfCameras = 1;
        /// <summary>
        /// Number of available cameras in system.
        /// </summary>
        public int NumberOfCameras
        {
            get { return m_TheNumberOfCameras; }
            set { m_TheNumberOfCameras = value; }
        }

        #endregion

        #region Constructors \ Destructor

        public Mil10Base()
        {
            InitRegistryData();

            m_MatroxImageLog = LogManager.GetLogger("MatroxImageLog");

           
            if (!m_SimulationMode)
            {
                #region Initialize base Matrox Classes
                // Allocate a MIL application.
                MIL.MappAlloc(MIL.M_DEFAULT, ref m_MilApplication);
                MIL.MappControl(m_MilApplication, MIL.M_ERROR, MIL.M_PRINT_DISABLE);

                switch (m_VideoType)
                {
                    case VIDEO_TYPES.GigE:
                        // Allocate a MIL system.
                        MIL.MsysAlloc("Matrox GigE Vision", MIL.M_DEFAULT, MIL.M_DEFAULT, ref m_MilSystem);
                        break;
                    case VIDEO_TYPES.FireWire:
                        // Allocate a MIL system.
                        MIL.MsysAlloc("Matrox IEEE 1394 IIDC", MIL.M_DEFAULT, MIL.M_DEFAULT, ref m_MilSystem);
                        break;
                    case VIDEO_TYPES.Default:
                        // Allocate a MIL system.
                        MIL.MsysAlloc("M_DEFAULT", MIL.M_DEFAULT, MIL.M_DEFAULT, ref m_MilSystem);
                        break;
                    default:
                        // Allocate a MIL system.
                        MIL.MsysAlloc("M_DEFAULT", MIL.M_DEFAULT, MIL.M_DEFAULT, ref m_MilSystem);
                        break;
                }

                

                // Tell MIL to throw exceptions when an error occurs.
                // The type of Exception thrown by MIL functions is MILException
                MIL.MappControl(MIL.M_ERROR, MIL.M_THROW_EXCEPTION);

                #endregion
            }
        }

        public Mil10Base(int appID)
        {
            InitRegistryData();

            m_MatroxImageLog = LogManager.GetLogger("MatroxImageLog");


            if (!m_SimulationMode)
            {
                #region Initialize base Matrox Classes
                // Allocate a MIL application.

                m_MilApplication = (MIL_ID)appID;
                switch (m_VideoType)
                {
                    case VIDEO_TYPES.GigE:
                        // Allocate a MIL system.
                        MIL.MsysAlloc(MIL.M_SYSTEM_GIGE_VISION, MIL.M_DEFAULT, MIL.M_DEFAULT, ref m_MilSystem);
                        break;
                    case VIDEO_TYPES.FireWire:
                        // Allocate a MIL system.
                        MIL.MsysAlloc(MIL.M_SYSTEM_1394, MIL.M_DEFAULT, MIL.M_DEFAULT, ref m_MilSystem);
                        break;
                    case VIDEO_TYPES.Default:
                        // Allocate a MIL system.
                        MIL.MsysAlloc("M_DEFAULT", MIL.M_DEFAULT, MIL.M_DEFAULT, ref m_MilSystem);
                        break;
                    default:
                        // Allocate a MIL system.
                        MIL.MsysAlloc("M_DEFAULT", MIL.M_DEFAULT, MIL.M_DEFAULT, ref m_MilSystem);
                        break;
                }

                // Tell MIL to throw exceptions when an error occurs.
                // The type of Exception thrown by MIL functions is MILException
                MIL.MappControl(MIL.M_ERROR, MIL.M_THROW_EXCEPTION);

                #endregion
            }
        } 
      
      
        ~Mil10Base()
        {
            Dispose(false); 
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Get base data from registry
        /// </summary>
        protected int InitRegistryData()
        {
            try
            {
                //Initialize application registry data
                cRegistryFunc objRegistry = new cRegistryFunc("Video Controller",
                                                              System.Reflection.Assembly.GetExecutingAssembly().GetName().ToString(),
                                                              cRegistryFunc._REG_BASE_KEY_TYPES.HKEY_LOCAL_MACHINE);

                m_SimulationMode = (objRegistry.GetRegKeyValue("", "Is Simulation Mode", (m_SimulationMode == true ? 1 : 0)) == 1 ? true : false);
                objRegistry.SetRegKeyValue("", "Is Simulation Mode", m_SimulationMode == true ? 1 : 0);

                //m_VideoType = (VIDEO_TYPES) objRegistry.GetRegKeyValue("", "Video Type", (int)VIDEO_TYPES.Default);
                m_VideoType = (VIDEO_TYPES)AVI4S.Configuration.Configuration.GetValue("Video Controller/VideoControllerBase/Video Type", 2);
                //objRegistry.SetRegKeyValue("", "Video Type", (int)m_VideoType);

                   
            }
            catch { };
            return ErrorCodesList.OK;
        }

        protected virtual void FreeMatroxReferences()
        {
            if (!m_SimulationMode)
            {
                #region Free Default Matrox references
                
                
                if (m_MilSystem != MIL.M_NULL)
                {
                    //MIL.MsysFree(m_MilSystem);
                    //m_MilSystem = MIL.M_NULL;
                }

                if (m_MilApplication != MIL.M_NULL)
                {
                    MIL.MappFree(m_MilApplication);
                    m_MilApplication = MIL.M_NULL;
                }
                #endregion
            }
        }

        #endregion

        #region Public Members

        public virtual void ReleaseMyOwnedObjects()
        {
            FreeMatroxReferences();
        }

        #endregion

        #region IDisposable

        // Public implementation of Dispose pattern callable by consumers. 
        public virtual void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern. 
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
               // managed area releases
            }

            //unmanaged area releases
            FreeMatroxReferences();

            disposed = true;
        }
 

        #endregion


        public string Name
        {
            get { return "Mil9Base"; }
        }
    }
}
