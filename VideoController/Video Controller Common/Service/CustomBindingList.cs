﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel; 
using System.Collections;

namespace NS_Video_Controller_Common.Service
{
    public class Mil9BaseRegionBindingList : BindingList<Mil9BaseRegion>
    {
        public Mil9BaseRegionBindingList()
        {            
        }

        public static Boolean operator !=(Mil9BaseRegionBindingList c1, Mil9BaseRegionBindingList c2) 
        {
            if (c1.Count != c2.Count) return true;

            for (Int16 i = 0; i < c1.Count; i++)
            {
                if (c1[i].RegionStartPosition != c2[i].RegionStartPosition ||
                    c1[i].RegionWidth != c2[i].RegionWidth)
                    return true;
            }

            return false;
        }

        public static Boolean operator ==(Mil9BaseRegionBindingList c1, Mil9BaseRegionBindingList c2)
        {
            if (c1.Count != c2.Count) return false  ;

            for (Int16 i = 0; i < c1.Count; i++)
            {
                if (c1[i].RegionStartPosition != c2[i].RegionStartPosition ||
                    c1[i].RegionWidth != c2[i].RegionWidth)
                    return false;
            }

            return true;
        }


        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

   
}
