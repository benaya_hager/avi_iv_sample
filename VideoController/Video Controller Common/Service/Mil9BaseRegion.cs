﻿using System;
using System.Collections.Generic;
using System.ComponentModel; 

namespace NS_Video_Controller_Common.Service
{
    public class Mil9BaseRegion : INotifyPropertyChanged
    {
        // These fields hold the values for the public properties.
        protected Guid idValue = Guid.NewGuid();
        protected int m_RegionStartPosition = 0;
        protected int m_RegionWidth = 0;
        protected Double m_RegionEstimatedHeight = 0;

        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        #region Constructors
        // The constructor is protected to enforce the factory pattern.
        protected Mil9BaseRegion()
        {
             
            m_RegionStartPosition = 0;
            m_RegionWidth = 0;
            m_RegionEstimatedHeight = 0;
        }
        public Mil9BaseRegion(Int32 region_start_position, Int32 region_width, Double region_estimated_height)
        {
            m_RegionStartPosition = region_start_position;
            m_RegionWidth = region_width;
            m_RegionEstimatedHeight = region_estimated_height;
        }
        
        #endregion

        // This is the public factory method.
        public Mil9BaseRegion CreateNewRegion()
        {
            return new Mil9BaseRegion();
        }

        #region Public Properties
        [DisplayName("Region Start (%)")]
        public Int32 RegionStartPosition
        {
            get { return this.m_RegionStartPosition; }
            set
            {
                if (value != this.m_RegionStartPosition)
                {
                    m_RegionStartPosition = value;
                    NotifyPropertyChanged("RegionStart");
                }
            }
        }

        // This property represents an ID, suitable
        // for use as a primary key in a database.
        [Browsable(false)]
        public Guid ID
        {
            get
            {
                return this.idValue;
            }
        }

        [DisplayName("Region Width (pxl)")]
        public Int32 RegionWidth
        {
            get { return this.m_RegionWidth; }
            set
            {
                if (value != this.m_RegionWidth)
                {
                    this.m_RegionWidth = value;
                    NotifyPropertyChanged("RegionWidth");
                }
            }
        }
         
        [DisplayName("Region Estimated Height")]
        public Double RegionEstimatedHeight
        {
            get { return this.m_RegionEstimatedHeight; }
            set
            {
                if (value != this.m_RegionEstimatedHeight)
                {
                    this.m_RegionEstimatedHeight = value;
                    NotifyPropertyChanged("RegionEstimatedWidth");
                }
            }
        }

        #endregion
    }
}
