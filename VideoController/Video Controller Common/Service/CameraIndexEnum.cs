﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NS_Video_Controller_Common.Service
{
    public enum  CameraIndexEnum
    {
        [Tools.String_Enum.StringValue("Net Set")]
        NOT_SET = -1,
        [Tools.String_Enum.StringValue("All Cameras")]
        ALL = 0,
        [Tools.String_Enum.StringValue("First Camera")]
        First = 1,
        [Tools.String_Enum.StringValue("Second Camera")]
        Second = 2,
        [Tools.String_Enum.StringValue("Third Camera")]
        Third = 3,
        [Tools.String_Enum.StringValue("Fourth Camera")]
        Fourth= 4,
        [Tools.String_Enum.StringValue("Fifth Camera")]
        Fifth = 5,
        [Tools.String_Enum.StringValue("Sixth Camera")]
        Sixth = 6,
        [Tools.String_Enum.StringValue("Seventh Camera")]
        Seventh = 7,
        [Tools.String_Enum.StringValue("Eighth Camera")]
        Eighth = 8,
        [Tools.String_Enum.StringValue("Ninth Camera")]
        Ninth = 9
    };
}
