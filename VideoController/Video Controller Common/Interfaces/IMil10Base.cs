﻿using System;
namespace NS_Video_Controller_Common.Interfaces
{
    public interface IMil10Base
    {
        log4net.ILog MatroxImageLog { get; }
        Matrox.MatroxImagingLibrary.MIL_ID MilApplication { get; }
        Matrox.MatroxImagingLibrary.MIL_ID MilSystem { get; }
        int NumberOfCameras { get; set; }
        void ReleaseMyOwnedObjects();
        bool SimulationMode { get; }
    }
}
