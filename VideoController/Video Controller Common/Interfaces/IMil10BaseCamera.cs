﻿using System;
using System.Drawing;
using NS_Video_Controller_Common.Service;
using System.Collections.Generic;
using Navitar;
namespace NS_Video_Controller_Common.Interfaces
{
    public interface IMil10BaseCamera : SFW.IIODevice 
    {
        CameraIndexEnum CameraIndex { get; set; }
        int FocusValue { get; set; }
        string DefaultTargetPath { get; set; }
        void Dispose();
        long Gain { get; set; }
        bool GrabOnStart { get; set; }
        System.Drawing.Rectangle ImageCropRectangle { get; } 
        double PixelToMillimter { get; set; }
        void ReleaseMyOwnedObjects();
        void ResetZoomFactor();
        void SetVideoWindow(IntPtr display_form_handle);
        long Shutter { get; set; }
        bool SimulationMode { get; }
        void StartGrabbing();
        void StartGrabbing(IntPtr display_form_handle);
        void StopGrabbing();

        int MILComputeFocus(List<Rectangle> FocusArea);
        List<int> MILComputeMultiFocus(List<Rectangle> FocusArea, int MultiFocusStage);
            void CreateComposedFocusedImage(List<int> MaxFocusIndexList,List<Rectangle> FocusArea);
        void StopImageSavingThread();
        void UpdateImageCropRectangle(int image_crop_rectangle_left, int image_crop_rectangle_top, int image_crop_rectangle_width, int image_crop_rectangle_height);
        void UpdatePanScroll(float update_horizontal_pan_factor, float update_vertical_pan_factor);
        void UpdateZoomFactor(float update_zoom_factor);
        IntPtr WindowHandle { get; }
        void ReadLastUsageDataSetup();
        //int CaptureAndSaveImage(string target_path);
        int CaptureAndSaveImage(string target_path, string target_file_name);
        int SaveImage(string target_path, string target_file_name);
        int CaptureAndSaveImage(string target_path, string target_file_name, RectangleF rectangle);
         int CaptureMultiFocusImage(string target_path, string target_file_name, List<Rectangle> FocusArea, int numOfSteps, int baseFocusPosition, int FocusStep);
        void DrawCross(int circSize,int lineThickness);
    }
}
