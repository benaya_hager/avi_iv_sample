﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFW;

namespace NS_Video_Controller_Common.Interfaces
{
    public interface IVideoControllerComponentsBuilder
    {
        System.Collections.Generic.Dictionary<string, SFW.IComponent> ComponentsList { get; set; }
        Microsoft.Practices.Unity.IUnityContainer Container { get; }
    }
}
