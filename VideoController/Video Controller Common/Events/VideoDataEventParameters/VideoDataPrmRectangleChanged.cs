﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NS_Video_Controller_Common.Service;

namespace NS_Video_Controller_Common.Events.NS_VideoDataEventParameters
{
    public class VideoDataPrmRectangleChanged  : VideoDataEventParameters
    {
        #region Public Properties

                /// <summary>
        ///the new value of Rectangle Parameter
        /// </summary>
        private Rectangle m_Parameter;
        /// <summary>
        ///Get/Set the new value of Rectangle Parameter
        /// </summary>
        public Rectangle RectangleParameter
        {
            get { return m_Parameter; }
            set
            {
                if (m_Parameter != value)
                {
                    m_Parameter = value;
                }
            }
        }

         
        #endregion

        #region Constructors
        public VideoDataPrmRectangleChanged(CameraIndexEnum camera_index)
            : base(camera_index)
        { }

        public VideoDataPrmRectangleChanged(CameraIndexEnum camera_index,
                                                  Rectangle Rectangle_parameter)
            : base( camera_index)
        {
            m_Parameter = Rectangle_parameter; 
        }
        #endregion

    }
}
