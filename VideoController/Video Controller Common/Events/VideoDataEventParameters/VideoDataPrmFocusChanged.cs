﻿using System;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NS_Video_Controller_Common.Service;
using SFW;

namespace NS_Video_Controller_Common.Events.NS_VideoDataEventParameters
{
   public  class VideoDataPrmFocusChanged : VideoDataEventParameters
    {
         #region Public Properties

                /// <summary>
        ///the new value of Focus Parameter
        /// </summary>
       private long m_Parameter;
        /// <summary>
        ///Get/Set the new value of Focus Parameter
        /// </summary>
        public long FocusValueParameter
        {
            get { return m_Parameter; }
            set
            {
                if (m_Parameter != value)
                {
                    m_Parameter = value;
                }
            }
        }

       /// <summary>
       /// if true then do fine focus else do coarse focus
       /// </summary>
 
       private bool m_FineFocus;

       public bool FineFocus
        {
            get { return m_FineFocus; }
            set { m_FineFocus = value; }
        }
        


        ///the new value of Rectangle Parameter
        /// </summary>
        private List<Rectangle> m_RectParameter;
        /// <summary>
        ///Get/Set the new value of Rectangle Parameter
        /// </summary>
        public List<Rectangle> RectangleParameter
        {
            get { return m_RectParameter; }
            set
            {
                if (m_RectParameter != value)
                {
                    m_RectParameter = value;
                }
            }
        }

        private List<int> m_FocusREsultsList;

        public List<int> FocusREsultsList
        {
            get { return m_FocusREsultsList; }
            set { m_FocusREsultsList = value; }
        }
        
        #endregion

        #region Constructors
        public VideoDataPrmFocusChanged(CameraIndexEnum camera_index)
            : base(camera_index)
        { }

        public VideoDataPrmFocusChanged(CameraIndexEnum camera_index,
                                                  long Focus_parameter, List<Rectangle> Rect_Parameter,bool isFineFocus)
            : base( camera_index)
        {
            m_Parameter = Focus_parameter;
            m_RectParameter = Rect_Parameter;
            m_FineFocus = isFineFocus;
        }

        public VideoDataPrmFocusChanged(CameraIndexEnum camera_index,
                                                  long Focus_parameter, List<Rectangle> Rect_Parameter,List<int> FocusResults, bool isFineFocus)
            : base(camera_index)
        {
            m_Parameter = Focus_parameter;
            m_RectParameter = Rect_Parameter;
            m_FineFocus = isFineFocus;
            m_FocusREsultsList = FocusResults;
        }
        #endregion
    }
}
