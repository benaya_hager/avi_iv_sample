﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NS_Video_Controller_Common.Service;

namespace NS_Video_Controller_Common.Events.NS_VideoDataEventParameters
{
    public class VideoDataEventParameters
    {
        #region Public Properties

        private CameraIndexEnum m_CameraIndex = CameraIndexEnum.ALL;
        public CameraIndexEnum CameraIndex
        {
            get { return m_CameraIndex; }
            set { m_CameraIndex = value; }
        }


        protected Object m_Sender;
        /// <summary>
        /// The object raised event
        /// </summary>
        public Object Sender
        {
            get { return m_Sender; }
            set { m_Sender = value; }
        }

        #endregion

        #region Constructors

        public VideoDataEventParameters(CameraIndexEnum camera_index)
        {
            m_CameraIndex = camera_index;
        }

        public VideoDataEventParameters(Object sender, CameraIndexEnum camera_index)
            : this(camera_index)
        {
            m_Sender = sender;
        }
        #endregion
    }
}
