﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 
using NS_Video_Controller_Common.Service;

namespace NS_Video_Controller_Common.Events.NS_VideoDataEventParameters
{
    public class VideoDataPrmDoubleChanged  : VideoDataEventParameters
    {
        #region Public Properties

        protected CameraIndexEnum m_CameraIndex = CameraIndexEnum.NOT_SET;
        public CameraIndexEnum CameraIndex
        {
            get { return m_CameraIndex; }
            set { m_CameraIndex = value; }
        }
        /// <summary>
        ///the new value of Double Parameter
        /// </summary>
        private Double m_Parameter = -1;
        /// <summary>
        ///Get/Set the new value of Double Parameter
        /// </summary>
        public Double DoubleParameter
        {
            get { return m_Parameter; }
            set
            {
                if (m_Parameter != value)
                {
                    m_Parameter = value;
                }
            }
        }

         
        #endregion

        #region Constructors
        public VideoDataPrmDoubleChanged(CameraIndexEnum camera_index)
            : base(camera_index)
        { }

        public VideoDataPrmDoubleChanged (CameraIndexEnum camera_index,
                                                  Double Double_parameter)
            : base( camera_index)
        {
            m_CameraIndex = camera_index;
            m_Parameter = Double_parameter; 
        }
        #endregion

    }
}
