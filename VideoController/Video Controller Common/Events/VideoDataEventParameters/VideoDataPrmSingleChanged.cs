﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 
using NS_Video_Controller_Common.Service;

namespace NS_Video_Controller_Common.Events.NS_VideoDataEventParameters
{
    public class VideoDataPrmSingleChanged  : VideoDataEventParameters
    {
        #region Public Properties

        protected CameraIndexEnum m_CameraIndex = CameraIndexEnum.NOT_SET;
        public CameraIndexEnum CameraIndex
        {
            get { return m_CameraIndex; }
            set { m_CameraIndex = value; }
        }
        /// <summary>
        ///the new value of Single Parameter
        /// </summary>
        private Single m_Parameter = -1;
        /// <summary>
        ///Get/Set the new value of Single Parameter
        /// </summary>
        public Single SingleParameter
        {
            get { return m_Parameter; }
            set
            {
                if (m_Parameter != value)
                {
                    m_Parameter = value;
                }
            }
        }

         
        #endregion

        #region Constructors
        public VideoDataPrmSingleChanged(CameraIndexEnum camera_index)
            : base(camera_index)
        { }

        public VideoDataPrmSingleChanged (CameraIndexEnum camera_index,
                                                  Single Single_parameter)
            : base( camera_index)
        {
            m_CameraIndex = camera_index;
            m_Parameter = Single_parameter; 
        }
        #endregion

    }
}
