﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NS_Video_Controller_Common.Events.NS_VideoComandEventsParameters;
using SFW; 

namespace NS_Video_Controller_Common.Events
{
    public class VideoCommandEvent : IEventData
    {
        #region Public Properties

        private VideoCommandEventTypes m_cmdType;
        public VideoCommandEventTypes CmdType
        {
            get { return m_cmdType; }
            private set { m_cmdType = value; }
        }

        private VideoCommandEventParameters m_CommandParameters;
        public VideoCommandEventParameters CommandParameters
        {
            get { return m_CommandParameters; }
            private set { m_CommandParameters = value; }
        }

        #endregion Public Properties

        #region Constructors

        public VideoCommandEvent(VideoCommandEventTypes incmdType, VideoCommandEventParameters prms)
        {
            m_cmdType = incmdType;
            m_CommandParameters = prms;
        }

        #endregion Constructors
    }
}
