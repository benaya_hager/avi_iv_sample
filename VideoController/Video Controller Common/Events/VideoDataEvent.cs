﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFW;
using NS_Video_Controller_Common.Events.NS_VideoDataEventParameters;

namespace NS_Video_Controller_Common.Events
{ 
    public class VideoDataEvent : IEventData
    {
        #region Public Properties

        private VideoDataEventType m_DataType;
        public VideoDataEventType DataType
        {
            get { return m_DataType; }
            private set { m_DataType = value; }
        }

        private VideoDataEventParameters m_DataParameters;
        public VideoDataEventParameters DataParameters
        {
            get { return m_DataParameters; }
            private set { m_DataParameters = value; }
        }

        #endregion Public Properties

        #region Constructors

        public VideoDataEvent(VideoDataEventType data_type, VideoDataEventParameters prms)
        {
            m_DataType = data_type;
            m_DataParameters = prms;
        }

        #endregion Constructors
    }
}
