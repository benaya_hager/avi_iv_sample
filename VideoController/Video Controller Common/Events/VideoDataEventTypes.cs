﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NS_Video_Controller_Common.Events
{ 
    public enum VideoDataEventType : byte
    {
        [Tools.String_Enum.StringValue("Not Set")]
        ET_NOT_SET = 255,
        [Tools.String_Enum.StringValue("Target display window handle changed")]
        ET_TARGET_WINDOW_PTR_CHANGED = 1,
        [Tools.String_Enum.StringValue("Camera Program Shutter changed")]
        ET_SHUTTER_CHANGED = 2,
        [Tools.String_Enum.StringValue("Camera Program Gain changed")]
        ET_GAIN_CHANGED = 3,
        [Tools.String_Enum.StringValue("Grabbed Image Crop Rectangle changed")]
        ET_CROP_RECTANGLE_CHANGED = 4,
        [Tools.String_Enum.StringValue("Pixel To Millimeters changed")]
        ET_PXL_2_MM_CHANGED = 5,
         [Tools.String_Enum.StringValue("Focus Value changed")]
        ET_FOCUS_VALUE_CHANGED = 6,
         [Tools.String_Enum.StringValue("Multi Focus Value changed")]
         ET_MULTI__FOCUS_VALUE_CHANGED = 7
    }
}
