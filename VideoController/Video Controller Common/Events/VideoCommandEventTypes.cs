﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NS_Video_Controller_Common.Events
{
    public enum VideoCommandEventTypes
    {
        [Tools.String_Enum.StringValue("Not Set")]
        CT_NOT_SET,

        [Tools.String_Enum.StringValue("Release Components")]
        CT_RELEASE_COMPONENTS,

        [Tools.String_Enum.StringValue("Start Continuous Grabbing")]
        CT_START_GRABBING,
        [Tools.String_Enum.StringValue("Stop Automatic Grabbing")]
        CT_STOP_GRABBING, 

        [Tools.String_Enum.StringValue("Capture and Save Image")]
        CT_CAPTURE_AND_SAVE_IMAGE,

        [Tools.String_Enum.StringValue("Save Image")]
        CT_SAVE_IMAGE,

        [Tools.String_Enum.StringValue("Capture and Save Image Region")]
        CT_CAPTURE_AND_SAVE_IMAGE_REGION,

        [Tools.String_Enum.StringValue("Draw cross on overlay Image")]
        CT_DRAW_CROSS,


        #region Update Parameters

        [Tools.String_Enum.StringValue("Update Settings In Memory")]
        CT_UPDATE_SETTINGS_IN_MEMORY,
        [Tools.String_Enum.StringValue("Load Predefined Setup")]
        CT_LOAD_PREDEFINED_SETUP,
        [Tools.String_Enum.StringValue("Save Setup File ")]
        CT_SAVE_SETUP_FILE,


        [Tools.String_Enum.StringValue("Gets from registry Last session setup parameters")]
        CT_GET_LAST_SESSION_SETUP_PARAMETERS,
        [Tools.String_Enum.StringValue("Update Zoom Factor")]
        CT_UPDATE_ZOOM_FACTOR,
        [Tools.String_Enum.StringValue("Reset Zoom Factor")]
        CT_RESET_ZOOM_FACTOR,
        [Tools.String_Enum.StringValue("Update Pan Factor")]
        CT_UPDATE_PAN_FACTOR,
        [Tools.String_Enum.StringValue("Update Gain Factor")]
        CT_UPDATE_GAIN_FACTOR,
        [Tools.String_Enum.StringValue("Update Shutter Factor")]
        CT_UPDATE_SHUTTER_FACTOR,
        [Tools.String_Enum.StringValue("Compute Focus")]
        CT_COMPUTE_FOCUS,
        [Tools.String_Enum.StringValue("Compute Multi Focus")]
        CT_COMPUTE_MULTI_FOCUS,
        [Tools.String_Enum.StringValue("Capture Multi Focus")]
        CT_CAPTURE_MULTI_FOCUS,
        [Tools.String_Enum.StringValue("Set Active Display Window To Display Video Stream")]
        CT_SET_DISPLAY_WINDOW,

        [Tools.String_Enum.StringValue("Set Display Image Type")]
        CT_SET_DISPLAY_IMAGE_TYPE,

        [Tools.String_Enum.StringValue("Set Search Algorithm Type")]
        CT_SET_SEARCH_ALGORITHM_TYPE,

        [Tools.String_Enum.StringValue("Calculate Average Pixel To Millimeter Parameter")]
        CT_CALCULATE_AVERAGE_PIXEL_TO_MILLIMETER,
        [Tools.String_Enum.StringValue("Update Pixel To Millimeter Parameter")]
        CT_UPDATE_PIXEL_TO_MILLIMETER,

        [Tools.String_Enum.StringValue("Update Search Regions")]
        CT_UPDATE_SEARCH_REGIONS,
        [Tools.String_Enum.StringValue("Update Binarization Threshold")]
        CT_UPDATE_BINARIZATION_TRESHOLDS,
        [Tools.String_Enum.StringValue("Update Expected Width Of Stripe")]
        CT_UPDATE_EXPECTED_WIRTH_OF_STRIPE,
        [Tools.String_Enum.StringValue("Update Image Crop Rectangle")]
        CT_UPDATE_IMAGE_CROP_RECTANGLE,
        [Tools.String_Enum.StringValue("Update Number Of Edges")]
        CT_UPDATE_NUMBER_OF_EDGES,
        [Tools.String_Enum.StringValue("Update Regions Angle Delta")]
        CT_UPDATE_REGIONS_ANGLE_DELTA,
        [Tools.String_Enum.StringValue("Update Other Matrox Parameters")]
        CT_UPDATE_OTHER_MATROX_PARAMETERS,


        #endregion
         
    }
}
