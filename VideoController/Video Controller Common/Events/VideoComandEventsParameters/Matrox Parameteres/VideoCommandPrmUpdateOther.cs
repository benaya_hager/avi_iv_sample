﻿using System; 
using System.Drawing;
using NS_Video_Controller_Common.Service;
namespace NS_Video_Controller_Common.Events.NS_VideoComandEventsParameters
{
    public class VideoCommandPrmUpdateOther : VideoCommandEventParameters
    {
        #region Public Properties

        /// <summary>
        ///Local copy of parameter for the number frames to count before performing Image Processing. 
        /// </summary>
        private Int32 m_PerformImageProcessingEachFrames;
        /// <summary>
        /// Number frames to count before performing Image Processing. 
        /// </summary>
        public Int32 PerformImageProcessingEachFrames
        {
            get { return m_PerformImageProcessingEachFrames; }
            set { m_PerformImageProcessingEachFrames = value; }
        }
        

        /// <summary>
        ///Local copy of parameter for the number of times to iterate binary or gray scale closing-type morphological operation. 
        /// </summary>
        private int m_MimCloseNbIteration = 15;
        /// <summary>
        ///Specifies the number of times to iterate binary or gray-scale closing-type morphological operation. 
        /// </summary>
        public int MimCloseNbIteration
        {
            get { return m_MimCloseNbIteration; }
            set { m_MimCloseNbIteration = value; }
        }

        private int m_LowerTreshold4BlobSize = 250;
        /// <summary>
        /// The lower size of Blob
        /// </summary>
        public int LowerTreshold4BlobSize
        {
            get { return m_LowerTreshold4BlobSize; }
            set { m_LowerTreshold4BlobSize = value; }
        }

        private int m_HigherTreshold4BlobSize = 50000;
        /// <summary>
        /// The Higher size of Blob
        /// </summary>
        public int HigherTreshold4BlobSize
        {
            get { return m_HigherTreshold4BlobSize; }
            set { m_HigherTreshold4BlobSize = value; }
        }

        private Rectangle m_RegionRejectMargins = new Rectangle(10, 10, 10, 10);
        /// <summary>
        /// Get/Set the minimum region offset from bounds
        /// </summary>
        public Rectangle RegionRejectMargins
        {
            get { return m_RegionRejectMargins; }
            set { m_RegionRejectMargins = value; }
        }

        private int m_DefaultRegionHeight = 200;
        public int DefaultRegionHeight
        {
            get { return m_DefaultRegionHeight; }
            set { m_DefaultRegionHeight = value; }
        }

        /// <summary>
        ///Local copy of parameter for the Pixel To Millimeter
        /// </summary>
        private Double m_PixelToMillimter = 1;
        /// <summary>
        ///Specifies the Pixel To Millimeter Coefficient.
        /// </summary>
        public Double PixelToMillimter
        {
            get { return m_PixelToMillimter; }
            set { m_PixelToMillimter = value; }
        }
        #endregion

        #region Constructors
        public VideoCommandPrmUpdateOther(CameraIndexEnum camera_index) : base(camera_index ) { }


        public VideoCommandPrmUpdateOther(Int32 perform_image_processing_each_frames,
                                            Int32 close_Nb_iterations, 
                                            Int32 lower_treshold_for_blob_size, 
                                            Int32 higher_treshold_for_blob_size,
                                            Int32 default_region_height, 
                                            Double pixel_2_millimeter,
                                            Rectangle region_reject_margins,
                                            CameraIndexEnum camera_index)
            : this(camera_index )
        {
            m_PerformImageProcessingEachFrames = perform_image_processing_each_frames; 
            m_MimCloseNbIteration = close_Nb_iterations;
            m_LowerTreshold4BlobSize = lower_treshold_for_blob_size;
            m_HigherTreshold4BlobSize = higher_treshold_for_blob_size;
            m_DefaultRegionHeight = default_region_height;
            m_PixelToMillimter = pixel_2_millimeter;
            m_RegionRejectMargins = region_reject_margins; 
        }
        #endregion
    }
}