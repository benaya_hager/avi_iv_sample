﻿using System;
using System.Drawing;
using NS_Video_Controller_Common.Service;
namespace NS_Video_Controller_Common.Events.NS_VideoComandEventsParameters
{
    public class VideoCommandPrmUpdateNumberOfEdges : VideoCommandEventParameters
    {
        #region Public Properties
        private Int32 m_MarkersMinimumNumOfEdges = 7;
        /// <summary>
        /// Sets the minimum tolerance limit for the expected number of edges inside the stripe.
        /// </summary>
        public Int32 MarkersMinimumNumOfEdges
        {
            get { return m_MarkersMinimumNumOfEdges; }
            set { m_MarkersMinimumNumOfEdges = value; }
        }

        private Int32 m_MarkersLowNumOfEdges = 7;
        /// <summary>
        /// Sets the low tolerance limit for the expected number of edges inside the stripe.
        /// </summary>
        public Int32 MarkersLowNumOfEdges
        {
            get { return m_MarkersLowNumOfEdges; }
            set { m_MarkersLowNumOfEdges = value; }
        }

        private Int32 m_MarkersHighNumOfEdges = 1000;
        /// <summary>
        /// Sets the High tolerance limit for the expected number of edges inside the stripe.
        /// </summary>
        public Int32 MarkersHighNumOfEdges
        {
            get { return m_MarkersHighNumOfEdges; }
            set { m_MarkersHighNumOfEdges = value; }
        }

        private Int32 m_MarkersMaximumNumOfEdges = 1000;
        /// <summary>
        /// Sets the Maximum tolerance limit for the expected number of edges inside the stripe.
        /// </summary>
        public Int32 MarkersMaximumNumOfEdges
        {
            get { return m_MarkersMaximumNumOfEdges; }
            set { m_MarkersMaximumNumOfEdges = value; }
        }

        #endregion

        #region Constructors
        public VideoCommandPrmUpdateNumberOfEdges(CameraIndexEnum camera_index) : base (camera_index ){ }

        public VideoCommandPrmUpdateNumberOfEdges(Int32 minimum_num_of_edges, 
                                                  Int32 low_num_of_edges, 
                                                  Int32 high_num_of_edges, 
                                                  Int32 maximum_num_of_edges, 
                                                  CameraIndexEnum camera_index)
            : this(camera_index)
        {
            m_MarkersMinimumNumOfEdges = minimum_num_of_edges;
            m_MarkersLowNumOfEdges = low_num_of_edges;
            m_MarkersHighNumOfEdges = high_num_of_edges;
            m_MarkersMaximumNumOfEdges = maximum_num_of_edges;
        }
        #endregion
    }
}