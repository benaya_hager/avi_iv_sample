﻿using System;
using System.Drawing;
using NS_Video_Controller_Common.Service;
namespace NS_Video_Controller_Common.Events.NS_VideoComandEventsParameters
{
    public class VideoCommandPrmUpdateImageCropRectangle : VideoCommandEventParameters
    {
        #region Public Properties
        private System.Drawing.Rectangle m_CropRectangle;
        public System.Drawing.Rectangle CropRectangle
        {
            get { return m_CropRectangle; }
            set { m_CropRectangle = value; }
        }
        #endregion

        #region Constructors
        public VideoCommandPrmUpdateImageCropRectangle( CameraIndexEnum camera_index) 
            : base(camera_index)
        { m_CropRectangle = new Rectangle(); }

        public VideoCommandPrmUpdateImageCropRectangle(System.Drawing.Rectangle crop_rectangle, CameraIndexEnum camera_index)
            : this (camera_index )
        {
            m_CropRectangle = crop_rectangle;
        }

        public VideoCommandPrmUpdateImageCropRectangle(Int32 left, Int32 top, Int32 width, Int32 height, CameraIndexEnum camera_index)
            : this(camera_index)
        {
            m_CropRectangle = new Rectangle(left,top,width,height) ;
        }
        #endregion
    }
}