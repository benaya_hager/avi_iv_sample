﻿using System;
using NS_Video_Controller_Common.Service; 

namespace NS_Video_Controller_Common.Events.NS_VideoComandEventsParameters
{
    public class VideoCommandPrmUpdateBinarizationThresholds : VideoCommandEventParameters
    {
        #region Public Properties
        /// <summary>
        /// Local copy of parameter for Binarization Threshold of Searching Blobs
        /// </summary>
        private int m_BlobBinarizationTreshhold = 170;
        /// <summary>
        /// Binarization Threshold For Searching Blobs
        /// </summary>
        public int BlobBinarizationTreshhold
        {
            get { return m_BlobBinarizationTreshhold; }
            set { m_BlobBinarizationTreshhold = value; }
        }

        /// <summary>
        /// Local copy of parameter for Binarization Threshold of Searching Regions
        /// </summary>
        private int m_RegionsBinarizationTreshhold = 200;
        /// <summary>
        /// Binarization Threshold For Searching Regions
        /// </summary>
        public int RegionsBinarizationTreshhold
        {
            get { return m_RegionsBinarizationTreshhold; }
            set { m_RegionsBinarizationTreshhold = value; }
        }
        #endregion

        #region Constructors
        public VideoCommandPrmUpdateBinarizationThresholds(CameraIndexEnum camera_index) : base(camera_index ) { }

        public VideoCommandPrmUpdateBinarizationThresholds(Int32 blob_binarization_treshhold, Int32 regions_binarization_treshhold, CameraIndexEnum camera_index)
            : this(camera_index )
        {
            m_BlobBinarizationTreshhold = blob_binarization_treshhold;
            m_RegionsBinarizationTreshhold = regions_binarization_treshhold;
        }
        #endregion
    }
}