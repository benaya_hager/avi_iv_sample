﻿using System;
using NS_Video_Controller_Common.Service;


namespace NS_Video_Controller_Common.Events.NS_VideoComandEventsParameters
{
    public class VideoCommandPrmSetSearchAlorithmType : VideoCommandEventParameters
    {
        #region Public Properties

        private _SEARCH_ALGORITHM_TYPE m_SearchAlgorithmType;
        public _SEARCH_ALGORITHM_TYPE SearchAlgorithmType
        {
            get { return m_SearchAlgorithmType; }
            set
            {
                m_SearchAlgorithmType = value;
            }
        }

        #endregion


        #region Constructors
        public VideoCommandPrmSetSearchAlorithmType(_SEARCH_ALGORITHM_TYPE search_algorithm_type, CameraIndexEnum camera_index)
            :base(camera_index )
        {
            m_SearchAlgorithmType = search_algorithm_type;
        }  
        #endregion      
    }
}