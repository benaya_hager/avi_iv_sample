﻿using System;
using NS_Video_Controller_Common.Service; 
namespace NS_Video_Controller_Common.Events.NS_VideoComandEventsParameters
{
    public class VideoCommandPrmUpdateShutter : VideoCommandEventParameters
    {
        #region Public Properties
        private Int32 m_UpdateShutterFactor;
        public Int32 UpdateShutterFactor
        {
            get { return m_UpdateShutterFactor; }
            set { m_UpdateShutterFactor = value; }
        }
        #endregion

        #region Constructors
        public VideoCommandPrmUpdateShutter( CameraIndexEnum camera_index) : base (camera_index ) { }

        public VideoCommandPrmUpdateShutter(Int32 update_shutter_factor, CameraIndexEnum camera_index)
            : this(camera_index)
        {
            m_UpdateShutterFactor = update_shutter_factor;
        }
        #endregion
        
    }
}