﻿using System;
using NS_Video_Controller_Common.Service; 
namespace NS_Video_Controller_Common.Events.NS_VideoComandEventsParameters
{
    public class VideoCommandPrmUpdateGain : VideoCommandEventParameters
    {
        #region Public Properties

        private Int32 m_UpdateGainFactor;
        public Int32 UpdateGainFactor
        {
            get { return m_UpdateGainFactor; }
            set { m_UpdateGainFactor = value; }
        }

        #endregion

        #region Constructors
        
        public VideoCommandPrmUpdateGain(CameraIndexEnum camera_index)
                :base(camera_index)
        { }

        public VideoCommandPrmUpdateGain(Int32 update_gain_factor, CameraIndexEnum camera_index)
            : base(camera_index)
        {
            m_UpdateGainFactor = update_gain_factor;
        }
        
    #endregion
    
    }
}