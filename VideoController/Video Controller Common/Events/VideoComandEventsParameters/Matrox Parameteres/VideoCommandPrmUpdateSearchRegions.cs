﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using NS_Video_Controller_Common.Service; 

namespace NS_Video_Controller_Common.Events.NS_VideoComandEventsParameters
{
    public class VideoCommandPrmUpdateSearchRegions : VideoCommandEventParameters
    {
        #region Public Properties

        private Mil9BaseRegionBindingList m_ListOfRegions = new Mil9BaseRegionBindingList();
        public Mil9BaseRegionBindingList ListOfRegions
        {
            get { return m_ListOfRegions; }
            set { m_ListOfRegions = value; }
        }

        #endregion

        #region Constructors

        public VideoCommandPrmUpdateSearchRegions(CameraIndexEnum camera_index) : base (camera_index ) { }

        public VideoCommandPrmUpdateSearchRegions(Mil9BaseRegionBindingList list_of_regions, CameraIndexEnum camera_index)
            : this(camera_index)
        {
            m_ListOfRegions = list_of_regions;
        }

        #endregion

    }
}
