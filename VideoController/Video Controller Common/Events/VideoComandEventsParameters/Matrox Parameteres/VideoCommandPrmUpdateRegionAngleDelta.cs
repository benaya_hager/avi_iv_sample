﻿using System;
using NS_Video_Controller_Common.Service; 

namespace NS_Video_Controller_Common.Events.NS_VideoComandEventsParameters
{
    public class VideoCommandPrmUpdateRegionAngleDelta : VideoCommandEventParameters
    {
        #region Public properties
        private int m_MarkersPositiveAngleDelta = 10;
        /// <summary>
        /// Sets the positive range of angles within which to search for the marker, in degrees. 
        /// </summary>
        public int MarkersPositiveAngleDelta
        {
            get { return m_MarkersPositiveAngleDelta; }
            set { m_MarkersPositiveAngleDelta = value; }
        }

        private int m_MarkersNegativeAngleDelta = 10;
        /// <summary>
        /// Sets the negative range of angles within which to search for the marker, in degrees. 
        /// </summary>
        public int MarkersNegativeAngleDelta
        {
            get { return m_MarkersNegativeAngleDelta; }
            set { m_MarkersNegativeAngleDelta = value; }
        }

        private Double m_EdgeThreshold = 2;
        public Double EdgeThreshold
        {
            get { return m_EdgeThreshold; }
            set { m_EdgeThreshold = value; }
        }
        #endregion

        #region Constructors
        public VideoCommandPrmUpdateRegionAngleDelta(CameraIndexEnum camera_index) : base(camera_index) { }

        public VideoCommandPrmUpdateRegionAngleDelta(Int32 markers_positive_angle_delta,
                                                     Int32 markers_negative_angle_delta, 
                                                     Double edge_threshold,
                                                    CameraIndexEnum camera_index)
            : this(camera_index)
        {
            MarkersPositiveAngleDelta = markers_positive_angle_delta;
            m_MarkersNegativeAngleDelta = markers_negative_angle_delta;
            m_EdgeThreshold = edge_threshold;

        }
        #endregion
    }
}