﻿using System;
using System.Drawing;
using NS_Video_Controller_Common.Service;
namespace NS_Video_Controller_Common.Events.NS_VideoComandEventsParameters
{
    public class VideoCommandPrmUpdateExpectedWidthOfStripe : VideoCommandEventParameters
    {
        #region Public Properties
        private Int32 m_MarkersMinimumExpectedWidth = 50;
        /// <summary>
        /// Sets the minimum tolerance limit for the expected width. 
        /// </summary>
        public Int32 MarkersMinimumExpectedWidth
        {
            get { return m_MarkersMinimumExpectedWidth; }
            set { m_MarkersMinimumExpectedWidth = value; }
        }

        private Int32 m_MarkersLowExpectedWidth = 50;
        /// <summary>
        /// Sets the low tolerance limit for the expected width.
        /// </summary>
        public Int32 MarkersLowExpectedWidth
        {
            get { return m_MarkersLowExpectedWidth; }
            set { m_MarkersLowExpectedWidth = value; }
        }

        private Int32 m_MarkersHighExpectedWidth = 150;
        /// <summary>
        /// Sets the High tolerance limit for the expected number of edges inside the stripe.
        /// </summary>
        public Int32 MarkersHighExpectedWidth
        {
            get { return m_MarkersHighExpectedWidth; }
            set { m_MarkersHighExpectedWidth = value; }
        }

        private Int32 m_MarkersMaximumExpectedWidth = 150;
        /// <summary>
        /// Sets the Maximum tolerance limit for the expected number of edges inside the stripe.
        /// </summary>
        public Int32 MarkersMaximumExpectedWidth
        {
            get { return m_MarkersMaximumExpectedWidth; }
            set { m_MarkersMaximumExpectedWidth = value; }
        }
        #endregion

        #region Constructors
        public VideoCommandPrmUpdateExpectedWidthOfStripe(CameraIndexEnum camera_index) : base (camera_index ) { }

        public VideoCommandPrmUpdateExpectedWidthOfStripe(Int32 minimum_expected_width, 
                                                          Int32 low_expected_width, 
                                                          Int32 high_expected_width,
                                                          Int32 maximum_expected_width, 
                                                            CameraIndexEnum camera_index)
            :this (camera_index )

        {
            m_MarkersMinimumExpectedWidth = minimum_expected_width;
            m_MarkersLowExpectedWidth = low_expected_width;
            m_MarkersHighExpectedWidth = high_expected_width;
            m_MarkersMaximumExpectedWidth = maximum_expected_width;
        }

        #endregion
    }
}