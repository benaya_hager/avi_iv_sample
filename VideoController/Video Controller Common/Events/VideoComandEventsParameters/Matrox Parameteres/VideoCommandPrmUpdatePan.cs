﻿using System;
using NS_Video_Controller_Common.Service; 
namespace NS_Video_Controller_Common.Events.NS_VideoComandEventsParameters
{
    public class VideoCommandPrmUpdatePan : VideoCommandEventParameters
    {
        #region Public Properties
        private Single m_UpdateHorizontalPanFactor;
        public Single UpdateHorizontalPanFactor
        {
            get { return m_UpdateHorizontalPanFactor; }
            set { m_UpdateHorizontalPanFactor = value; }
        }

        private Single m_UpdateVerticalPanFactor;
        public Single UpdateVerticalPanFactor
        {
            get { return m_UpdateVerticalPanFactor; }
            set { m_UpdateVerticalPanFactor = value; }
        }
        #endregion

        #region Constructors

        public VideoCommandPrmUpdatePan( CameraIndexEnum camera_index):base (camera_index ) { }

        public VideoCommandPrmUpdatePan(Single update_horizontal_pan_factor, Single update_vertical_pan_factor, CameraIndexEnum camera_index)
            : this (camera_index )
        {
            m_UpdateHorizontalPanFactor = update_horizontal_pan_factor;
            m_UpdateVerticalPanFactor = update_vertical_pan_factor;
        }
        #endregion
        
    }
}