﻿using System;
using NS_Video_Controller_Common.Service; 
namespace NS_Video_Controller_Common.Events.NS_VideoComandEventsParameters
{
    public class VideoCommandPrmUpdateZoom : VideoCommandEventParameters
    {
        #region Public Properties
        private Single m_UpdateZoomFactor;
        public Single UpdateZoomFactor
        {
            get { return m_UpdateZoomFactor; }
            set { m_UpdateZoomFactor = value; }
        }
        #endregion

        #region Constructors
        public VideoCommandPrmUpdateZoom(CameraIndexEnum camera_index) : base(camera_index) { }

        public VideoCommandPrmUpdateZoom(Single update_zoom_factor, CameraIndexEnum camera_index)
            :this(camera_index )
        {
            m_UpdateZoomFactor = update_zoom_factor;
        }
        
        #endregion
    }
}