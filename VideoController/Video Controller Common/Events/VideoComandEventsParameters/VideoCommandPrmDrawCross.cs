﻿using NS_Video_Controller_Common.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NS_Video_Controller_Common.Events.NS_VideoComandEventsParameters
{
    public class VideoCommandPrmDrawCross : VideoCommandEventParameters
    {




        public VideoCommandPrmDrawCross(CameraIndexEnum camera_index,  int circleSize, int lineThikness)
            : base(camera_index) 
        {
            _CenterCircleSize = circleSize;
            _CrossThikness = lineThikness;

        }

        private int _CenterCircleSize;

        public int CenterCircleSize
        {
            get { return _CenterCircleSize; }
            set { _CenterCircleSize = value; }
        }

        private int _CrossThikness;

        public int CrossThikness
        {
            get { return _CrossThikness; }
            set { _CrossThikness = value; }
        }

    }



}
