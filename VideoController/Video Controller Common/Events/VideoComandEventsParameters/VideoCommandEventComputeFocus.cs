﻿using System;
using NS_Video_Controller_Common.Service;
using System.Drawing;
using NS_Video_Controller_Common.Events.NS_VideoComandEventsParameters;
using System.Collections.Generic;

namespace NS_Video_Controller_Common.Events.VideoComandEventsParameters
{
    public class VideoCommandEventComputeFocus : VideoCommandEventParameters
    {
         #region Public Properties

        private CameraIndexEnum m_CameraIndex = CameraIndexEnum.ALL;
        public CameraIndexEnum CameraIndex
        {
            get { return m_CameraIndex; }
            set { m_CameraIndex = value; }
        }

        private List<Rectangle> m_CropRectangle;
        public List<Rectangle> CropRectangle
        {
            get { return m_CropRectangle; }
            set { m_CropRectangle = value; }
        }

        private bool m_IsFineFocus;

        public bool IsFineFocus
        {
            get { return m_IsFineFocus; }
            set { m_IsFineFocus = value; }
        }

        private int m_MultiFocusStage;

        public int MultiFocusStage
        {
            get { return m_MultiFocusStage; }
            set { m_MultiFocusStage = value; }
        }

        private string m_TargetDirectory;

        public string TargetDirectory
        {
            get { return m_TargetDirectory; }
            set { m_TargetDirectory = value; }
        }

        private string m_TargetFileName;

        public string TargetFileName
        {
            get { return m_TargetFileName; }
            set { m_TargetFileName = value; }
        }

        private int m_BestFocusPosition;

        public int BestFocusPosition
        {
            get { return m_BestFocusPosition; }
            set { m_BestFocusPosition = value; }
        }
        

        #endregion

        #region Constructors
        public VideoCommandEventComputeFocus(CameraIndexEnum camera_index, List<Rectangle> rect,bool isFineFocus, int MFstage = 1, string dir ="", string fname="", int bstFocusPosition = 0)
            : base(camera_index) 
        {
            m_CameraIndex = camera_index;
            m_CropRectangle = rect;
            m_IsFineFocus = isFineFocus;
            m_MultiFocusStage = MFstage;
            m_TargetDirectory = dir;
            m_TargetFileName = fname;
            m_BestFocusPosition = bstFocusPosition;
        }

      
        #endregion
    }
}
