﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NS_Video_Controller_Common.Service;

namespace NS_Video_Controller_Common.Events.NS_VideoComandEventsParameters
{
    public class VideoCommandPrmCaptureImageRegion : VideoCommandEventParameters
    {
         #region Public Properties
 
            private String m_TargetDirectory;
	        public String TargetDirectory
	        {
		        get { return m_TargetDirectory;}
		        set { m_TargetDirectory = value;}
	        }

	
            private String  m_TargetFileName;

	        public String  TargetFileName
	        {
		        get { return m_TargetFileName;}
		        set { m_TargetFileName = value;}
	        }

            private RectangleF m_CropRectangle;

            public RectangleF CropRectangle
            {
                get { return m_CropRectangle; }
                set { m_CropRectangle = value; }
            }
            

        #endregion Public Properties

        #region Constructors

        public VideoCommandPrmCaptureImageRegion(CameraIndexEnum camera_index,
                                           String target_path,
                                           String target_file_name,
                                           RectangleF rect)
                : base(camera_index) 
        {
            m_TargetDirectory = target_path;
            m_TargetFileName = target_file_name;
            m_CropRectangle = rect; 
        }
         
        #endregion Constructors
    }
}
