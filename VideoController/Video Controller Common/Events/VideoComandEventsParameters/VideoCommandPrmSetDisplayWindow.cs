﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NS_Video_Controller_Common.Service;

namespace NS_Video_Controller_Common.Events.NS_VideoComandEventsParameters
{
    public class VideoCommandPrmSetDisplayWindow : VideoCommandEventParameters
    {
        #region Public Properties
        private IntPtr m_DisplayWindowHandle;
        public IntPtr DisplayWindowHandle
        {
            get { return m_DisplayWindowHandle; }
            set { m_DisplayWindowHandle = value; }
        }

        #endregion

        #region Constructors
        public VideoCommandPrmSetDisplayWindow(IntPtr display_window_handle, CameraIndexEnum camera_index)
            : base(camera_index)
        {
            m_DisplayWindowHandle = display_window_handle;
        }
        #endregion

    }
}
