﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NS_Video_Controller_Common.Service;

namespace NS_Video_Controller_Common.Events.NS_VideoComandEventsParameters
{
    public class VideoCommandPrmCaptureImage : VideoCommandEventParameters
    {
         #region Public Properties
 
            private String m_TargetDirectory;
	        public String TargetDirectory
	        {
		        get { return m_TargetDirectory;}
		        set { m_TargetDirectory = value;}
	        }

	
            private String  m_TargetFileName;

	        public String  TargetFileName
	        {
		        get { return m_TargetFileName;}
		        set { m_TargetFileName = value;}
	        }
	

        #endregion Public Properties

        #region Constructors

            public VideoCommandPrmCaptureImage(CameraIndexEnum camera_index,
                                           String target_path,
                                           String target_file_name )
                : base(camera_index) 
        {
            m_TargetDirectory = target_path;
            m_TargetFileName = target_file_name; 
        }
         
        #endregion Constructors
    }
}
