﻿using System;
using NS_Video_Controller_Common.Service;

namespace NS_Video_Controller_Common.Events.NS_VideoComandEventsParameters
{
    public class VideoCommandEventParameters
    {
        #region Public Properties

        private CameraIndexEnum m_CameraIndex = CameraIndexEnum.ALL;
        public CameraIndexEnum CameraIndex
        {
            get { return m_CameraIndex; }
            set { m_CameraIndex = value; }
        }

        #endregion

        #region Constructors
        public VideoCommandEventParameters(CameraIndexEnum camera_index)
        {
            m_CameraIndex = camera_index;
        }
        #endregion
    }
}