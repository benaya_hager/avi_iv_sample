﻿using System; 
using System.IO;
using NS_Video_Controller_Common.Service;

namespace NS_Video_Controller_Common.Events.NS_VideoComandEventsParameters
{
    public class VideoCommandPrmLoadPredefinedSetup : VideoCommandEventParameters
    {
        #region Public Properties

        private String m_TargetPath = @"C:\Medinol\AVI IV\Setups\";
        /// <summary>
        /// Get/Set the Target Path.
        /// </summary>
        public String TargetPath
        {
            get { return m_TargetPath; }
            private  set { m_TargetPath = value; }
        }

        private String m_TargetFileName = @"First Camera" /*Video Parameters*/;
        /// <summary>
        /// Get/Set the Target Path.
        /// </summary>
        public String TargetFileName
        {
            get { return m_TargetFileName; }
            private set { m_TargetFileName = value; }
        }

        private String m_TargetFilePattern = @"VPrms" /*Video Parameters*/;
        /// <summary>
        /// Get/Set the Target file Pattern.
        /// </summary>
        public String TargetFilePattern
        {
            get { return m_TargetFilePattern; }
            private set { m_TargetFilePattern = value; }
        }


        #endregion

        #region Constructors

        public VideoCommandPrmLoadPredefinedSetup(CameraIndexEnum camera_index,
                                                  String BaseSetupFullPathFileName )
            : base(camera_index)
        { 
            String filename = Path.GetFileNameWithoutExtension(BaseSetupFullPathFileName);
            String dir = Path.GetDirectoryName(BaseSetupFullPathFileName) ;
            String m_PathWithSetup = Path.Combine(dir,filename);
            if (!Directory.Exists(m_PathWithSetup))
            {
                Directory.CreateDirectory(m_PathWithSetup);
            }
                 
            m_TargetPath = m_PathWithSetup;
            m_TargetFileName = Path.Combine(m_PathWithSetup, Tools.String_Enum.StringEnum.GetStringValue(camera_index) + ".VPRMS" /*Video Parameters*/  );
        }

        public VideoCommandPrmLoadPredefinedSetup(CameraIndexEnum camera_index , 
                                                  String config_files_path,  
                                                  String config_file_name )
            : base(camera_index)
         {

             m_TargetPath = config_files_path;
             m_TargetFileName = config_file_name;
             m_TargetFilePattern = "VPRMS";


             String filename = Path.GetFileNameWithoutExtension(m_TargetFileName);
             String dir = Path.GetDirectoryName(m_TargetPath);
             String m_PathWithSetup = Path.Combine(dir, filename);
             if (!Directory.Exists(m_PathWithSetup))
             {
                 Directory.CreateDirectory(m_PathWithSetup);
             }

             m_TargetPath = m_PathWithSetup;
             m_TargetFileName = Tools.String_Enum.StringEnum.GetStringValue(camera_index); // +"." + m_TargetFilePattern   /*Video Parameters*/  ;
        
        }

        #endregion
        
    }
}