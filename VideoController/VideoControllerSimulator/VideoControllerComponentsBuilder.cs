﻿using log4net; 
using Microsoft.Practices.Unity; 
using SFW;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using NS_Video_Controller_Common.Interfaces;
using NS_VideoControllerSimulator;
using NS_VideoControllerBase;
using NS_Video_Controller_Common.Service;
using Tools.CustomRegistry;

namespace VideoControllerSimulator
{

    public class VideoControllerComponentsBuilder : IVideoControllerComponentsBuilder
    {
        #region Private Data Members

        private ILog m_SystemLog;
        private int m_VideoType = 1;
        private bool isSimulateMode = false;

        #endregion Private Data Members

        #region Public properties


        private IUnityContainer m_Container;
        public IUnityContainer Container
        {
            get { return m_Container; }
        }

        protected Dictionary<string, IComponent> m_Components;
        public Dictionary<string, IComponent> ComponentsList
        {
            get { return m_Components; }
            set { m_Components = value; }
        }



        #endregion Public properties

        public VideoControllerComponentsBuilder(IUnityContainer container)
        {
            InitRegistryData();
            m_Container = container;

            SetApplicationUnhandledExeptionsHandle();

            log4net.Config.XmlConfigurator.Configure(new System.IO.FileInfo(@"Video Controller.config"));
            m_SystemLog = LogManager.GetLogger("SystemLog");

            m_Components = new Dictionary<string, IComponent>();


            if (!isSimulateMode)
                CreateVideoControllerObjects();
      
        }

        public VideoControllerComponentsBuilder(IUnityContainer container, int appID)
        {
            InitRegistryData();
            m_Container = container;

            SetApplicationUnhandledExeptionsHandle();

            log4net.Config.XmlConfigurator.Configure(new System.IO.FileInfo(@"Video Controller.config"));
            m_SystemLog = LogManager.GetLogger("SystemLog");

            m_Components = new Dictionary<string, IComponent>();

            if (!isSimulateMode)
                CreateVideoControllerObjects(appID);

        }

        ~VideoControllerComponentsBuilder()
        { 
        
        }

        #region Public Methods



        #endregion

        #region Private Methods

        #region Create Objects

        protected virtual void CreateVideoControllerObjects()
        {
            m_SystemLog.Info("Started Video Controller Objects Initialization");

            #region Video Controller Object 
            IMil10Base mil_base;

            m_Container.RegisterType<IMil10Base, Mil10Base>();
            mil_base = m_Container.Resolve<IMil10Base>();
            m_Container.RegisterInstance<IMil10Base>(mil_base);
            

            m_Container.RegisterType<IMil10BaseCamera, Mil10BaseCamera>("Camera", 
                                                                        new InjectionConstructor(CameraIndexEnum.First, mil_base, 0, "M_DEFAULT"));
            IMil10BaseCamera camera = m_Container.Resolve<IMil10BaseCamera>("Camera");
            IActionSyncManager SyncMan = m_Container.Resolve<IActionSyncManager>();

            lock (m_Components)
            {
                m_Components.Add("MilBase", (Mil10Base)mil_base);
                m_Components.Add("Camera", (IMil10BaseCamera)camera);
            }
            #endregion

            #region Video Listener

            IVideoControllerListener listener;
            m_Container.RegisterType<IVideoControllerListener, VideoControllerListener>("VideoControllerListener",
                                                              new InjectionConstructor(camera));
            listener = (VideoControllerListener)m_Container.Resolve<IVideoControllerListener>("VideoControllerListener");

            //if (!m_Container.IsRegistered<IVideoControllerListener>())
            //{
                m_Container.RegisterInstance<IVideoControllerListener>(listener);
            //}
            lock (m_Components)
            {
                m_Components.Add("VideoControllerListener", (VideoControllerListener)listener);
            }
            #endregion


            #region Video Command Logic

            IVideoControllerLogic logic;
            m_Container.RegisterType<IVideoControllerLogic, VideoControllerLogic>("VideoControllerLogic",
                                                              new InjectionConstructor(camera, listener, SyncMan));
            logic = (VideoControllerLogic)m_Container.Resolve<IVideoControllerLogic>("VideoControllerLogic");

            //if (!m_Container.IsRegistered<IVideoControllerLogic>())
            //{
                m_Container.RegisterInstance<IVideoControllerLogic>(logic);
            //}
            lock (m_Components)
            {
                m_Components.Add("VideoControllerLogic", (VideoControllerLogic)logic);
            }
            #endregion

            m_SystemLog.Info("Finished Video Controller Objects Initialization");
        }

        protected virtual void CreateVideoControllerObjects(int appID)
        {
            m_SystemLog.Info("Started Video Controller Objects Initialization");

            #region Video Controller Object
            IMil10Base mil_base;
            //m_Container.RegisterType<IMil10Base, Mil10Base>();
            m_Container.RegisterType<IMil10Base, Mil10Base>("BaseCamera",
                                                                       new InjectionConstructor(appID));
            mil_base = m_Container.Resolve<IMil10Base>("BaseCamera");
            // m_Container.RegisterInstance<IMil10Base>(mil_base);

            if (m_VideoType == 2) // Gige Vision Video
                m_Container.RegisterType<IMil10BaseCamera, Mil10BaseCamera>("Camera",
                                                                       new InjectionConstructor(CameraIndexEnum.First, mil_base, 0, "M_2452X2054_Y_FORMAT_7_0"));
            else // IIDC 1394 - FireWire
                m_Container.RegisterType<IMil10BaseCamera, Mil10BaseCamera>("Camera",
                                                                   new InjectionConstructor(CameraIndexEnum.First, mil_base, 0, "M_DEFAULT"));

            IMil10BaseCamera camera = m_Container.Resolve<IMil10BaseCamera>("Camera");
            IActionSyncManager SyncMan = m_Container.Resolve<IActionSyncManager>();

            lock (m_Components)
            {
                m_Components.Add("MilBase", (Mil10Base)mil_base);
                m_Components.Add("Camera", (IMil10BaseCamera)camera);
            }

            #endregion

            #region Video Listener

            IVideoControllerListener listener;
            m_Container.RegisterType<IVideoControllerListener, VideoControllerListener>("VideoControllerListener",
                                                              new InjectionConstructor(camera));
            listener = (VideoControllerListener)m_Container.Resolve<IVideoControllerListener>("VideoControllerListener");

            //if (!m_Container.IsRegistered<IVideoControllerListener>())
            //{
            m_Container.RegisterInstance<IVideoControllerListener>(listener);
            //}
            lock (m_Components)
            {
                m_Components.Add("VideoControllerListener", (VideoControllerListener)listener);
            }
            #endregion


            #region Video Command Logic

            IVideoControllerLogic logic;
            m_Container.RegisterType<IVideoControllerLogic, VideoControllerLogic>("VideoControllerLogic",
                                                              new InjectionConstructor(camera, listener, SyncMan));
            logic = (VideoControllerLogic)m_Container.Resolve<IVideoControllerLogic>("VideoControllerLogic");

            //if (!m_Container.IsRegistered<IVideoControllerLogic>())
            //{
            m_Container.RegisterInstance<IVideoControllerLogic>(logic);
            //}
            lock (m_Components)
            {
                m_Components.Add("VideoControllerLogic", (VideoControllerLogic)logic);
            }
            #endregion

            m_SystemLog.Info("Finished Video Controller Objects Initialization");
        }

        protected int InitRegistryData()
        {
            try
            {
                //Initialize application registry data
                cRegistryFunc objRegistry = new cRegistryFunc("Video Controller",
                                                              "VideoControllerBase",
                                                              cRegistryFunc._REG_BASE_KEY_TYPES.HKEY_LOCAL_MACHINE);


               // m_VideoType = (int)objRegistry.GetRegKeyValue("", "Video Type", 1);
                //objRegistry.SetRegKeyValue("", "Video Type", (int)m_VideoType);
                m_VideoType = (int)AVI4S.Configuration.Configuration.GetValue("Video Controller/VideoControllerBase/Video Type", 2);

                cRegistryFunc objRegistry1 = new cRegistryFunc("AVI4S",
                                                    "",
                                                    cRegistryFunc._REG_BASE_KEY_TYPES.HKEY_LOCAL_MACHINE);

                isSimulateMode = Convert.ToBoolean(objRegistry1.GetRegKeyValue("", "SIMULATION_MODE", 0));

            }
            catch { };
            return 1;
        }
        #endregion

        #region Error handling

        private void SetApplicationUnhandledExeptionsHandle()
        {
 
        }

           

        #endregion Error handling

        #endregion Private Methods


    }
}

 