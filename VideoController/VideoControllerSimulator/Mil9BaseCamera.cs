﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SFW;
using Matrox.MatroxImagingLibrary;
using System.Threading;
using System.IO; 
using System.Drawing;
using NS_Video_Controller_Common.Interfaces;
using NS_Common;
using NS_Common.ViewModels.Common;
using NS_Video_Controller_Common.Service; 

namespace NS_Mil9Base
{
    public class Mil9BaseCamera : ViewModelBase, IMil9BaseCamera, IDisposable
    {
        #region protected Constant Values
            // Default image dimensions.
            protected const int DEFAULT_IMAGE_SIZE_X = 2452;
            protected const int DEFAULT_IMAGE_SIZE_Y = 2056;
            protected const int DEFAULT_IMAGE_SIZE_BAND = 3;
            static Object SynchronizationSaveImageObj = new object();

           
        
        #endregion

        #region Protected Members

        // Flag: Has Dispose already been called? 
        bool disposed = false;


        protected MIL_ID m_MilDisplay = MIL.M_NULL;      // MIL Display identifier.
        protected MIL_ID m_MilImage = MIL.M_NULL;        // MIL Image buffer identifier.
        protected MIL_ID m_MilDigitizer = MIL.M_NULL;    // MIL Digitizer identifier.

        protected MIL_ID m_MilOverlayImage = MIL.M_NULL;
        protected MIL_ID m_MilDrawingImage = MIL.M_NULL;
        protected MIL_ID m_MilProcessingImage = MIL.M_NULL;


        protected MIL_INT m_BufSizeX = DEFAULT_IMAGE_SIZE_X;
        protected MIL_INT m_BufSizeY = DEFAULT_IMAGE_SIZE_Y;
        protected MIL_INT m_BufSizeBand = DEFAULT_IMAGE_SIZE_BAND;

        protected long m_DefaultBufferAttributes;

        /// <summary>
        /// The reference to Store grabbed images thread. Used to store grabbed images
        /// on disk Or/And Message Queue
        /// </summary>
        protected static Thread StoreGrabbedImagesThread;
        protected CancellationTokenSource m_StoreGrabbedImagesTokenSource;

        protected double m_ZoomFactor = 1;
        #endregion

        #region Public Properties

        protected IMil9Base m_myBaseObjects;
        public IMil9Base myBaseObjects
        {
            get { return m_myBaseObjects; }
            protected set { m_myBaseObjects = value; }
        }


        protected CameraIndexEnum m_CameraIndex = CameraIndexEnum.NOT_SET;
        public CameraIndexEnum CameraIndex
        {
            get { return m_CameraIndex; }
            set { m_CameraIndex = value; }
        }

          

        /// <summary>
        /// Indicate if the system running in simulation mode,
        /// and not necessary  to raise error events
        /// </summary>
        protected Boolean m_SimulationMode = false;
        public Boolean SimulationMode
        {
            get { return m_SimulationMode; }
            protected set { m_SimulationMode = value; }
        }

         
        /// <summary>
        /// Used to refer window control from different than creation thread. ()
        /// </summary>
        protected IntPtr m_WindowHandle; 
        /// <summary>
        /// Set the active window to display grabbed images. Use Form.Handle in case of forms
        /// </summary>
        public IntPtr WindowHandle
        {
            get { return m_WindowHandle; }
            protected set
            {
                //m_DisplayForm = value;
                if (value != m_WindowHandle)
                {
                    m_WindowHandle = value;//.Handle;
                    RaisePropertyChanged("WindowHandle");
                }

            }
        }

        /// <summary>
        /// Indicate if the system shell start grabbing video on start
        /// </summary>
        protected Boolean m_GrabOnStart = true;
        public Boolean GrabOnStart
        {
            get { return m_GrabOnStart; }
            set { m_GrabOnStart = value; }
        }


        #region Camera properties

        protected long m_Shutter = 350;
        public long Shutter
        {
            get { return m_Shutter; }
            set
            {
                if (m_Shutter != value)
                {
                    m_Shutter = value;
                    if (m_MilDigitizer != MIL.M_NULL)
                    {
                        MIL.MdigControl(m_MilDigitizer, MIL.M_SHUTTER, m_Shutter);
                    }
                    RaisePropertyChanged("Shutter");
                }
            }
        }
         
        protected long m_Gain = 0;
        public long Gain
        {
            get { return m_Gain; }
            set
            {
                if (m_Gain != value)
                {
                    m_Gain = value;
                    if (m_MilDigitizer != MIL.M_NULL)
                    {
                        MIL.MdigControl(m_MilDigitizer, MIL.M_GAIN, m_Gain);
                        RaisePropertyChanged("Gain");
                    }
                }
            }
        }


        protected Rectangle m_ImageCropRectangle = new Rectangle(0, 0, DEFAULT_IMAGE_SIZE_X, DEFAULT_IMAGE_SIZE_Y);// {, new Rectangle(450, 1100, 1500, 600) };   
        public virtual Rectangle ImageCropRectangle
        {
            get { return m_ImageCropRectangle; }
            protected set
            {
                if (m_ImageCropRectangle.Left != value.Left || m_ImageCropRectangle.Top != value.Top ||
                    m_ImageCropRectangle.Width != value.Width || m_ImageCropRectangle.Height != value.Height)
                {
                    m_ImageCropRectangle = value;
                    RaisePropertyChanged("ImageCropRectangle");
                }
            }
        }


        /// <summary>
        ///Local copy of parameter for the Pixel To Millimeter
        /// </summary>
        protected Double m_PixelToMillimter = 1;
        /// <summary>
        ///Specifies the Pixel To Millimeter Coefficient.
        /// </summary>
        public Double PixelToMillimter
        {
            get { return m_PixelToMillimter; }
            set
            {
                if (m_PixelToMillimter != value)
                {
                    m_PixelToMillimter = value;
                    RaisePropertyChanged("PixelToMillimter");
                }
            }
        }


        #endregion



        protected String m_DefaultTargetPath;
        /// <summary>
        /// Get/Set the Target Path.
        /// </summary>
        public String DefaultTargetPath
        {
            get { return m_DefaultTargetPath; }
            set { m_DefaultTargetPath = value; }
        }

        protected Int32 m_SavingTimeout = 30000;
        public Int32 SavingTimeout
        {
            get { return m_SavingTimeout; }
            set { m_SavingTimeout = value; }
        }

        #endregion

        #region Constructors

        public Mil9BaseCamera(IMil9Base base_objects) : this(base_objects,0,"M_DEFAULT") { }

        public  Mil9BaseCamera(IMil9Base base_objects  , 
                                Int32 digitizer_number ,
                                String digitizer_name  )
        {
            m_SimulationMode = base_objects.SimulationMode;
            m_DefaultTargetPath = @"C:\Medinol\Images\";
            m_myBaseObjects = base_objects;

            if (!m_SimulationMode)
            {
                #region Initialize camera Matrox Classes
                    // Allocate a MIL display.
                    MIL.MdispAlloc(base_objects.MilSystem, MIL.M_DEFAULT, "M_DEFAULT", MIL.M_WINDOWED, ref m_MilDisplay);

                    MIL_INT  number_of_availible_digitizers = MIL.MsysInquire(base_objects.MilSystem, MIL.M_DIGITIZER_NUM, MIL.M_NULL);
                    if (number_of_availible_digitizers > 0)
                    {
                        MIL.MdigAlloc(base_objects.MilSystem, digitizer_number, digitizer_name, MIL.M_DEFAULT, ref m_MilDigitizer);
                    }

                    // Allocate a MIL buffer.
                    m_DefaultBufferAttributes = MIL.M_IMAGE + MIL.M_DISP + MIL.M_PROC;
                    if (m_MilDigitizer != MIL.M_NULL)
                    {
                        // Add M_GRAB attribute if a digitizer is allocated.
                        m_DefaultBufferAttributes |= MIL.M_GRAB;
                    }

                    MIL.MbufAlloc2d(base_objects.MilSystem, DEFAULT_IMAGE_SIZE_X, DEFAULT_IMAGE_SIZE_Y, 8 + MIL.M_UNSIGNED, m_DefaultBufferAttributes, ref m_MilImage);

                    MIL.MbufAlloc2d(base_objects.MilSystem, DEFAULT_IMAGE_SIZE_X, DEFAULT_IMAGE_SIZE_Y, 8 + MIL.M_UNSIGNED, m_DefaultBufferAttributes, ref m_MilProcessingImage);

                    // Clear the buffer.
                    MIL.MbufClear(m_MilImage, 0);

                    // Clear the buffer.
                    MIL.MbufClear(m_MilProcessingImage, 0);

                    long Attributes = MIL.M_IMAGE + MIL.M_DISP + MIL.M_DIB + MIL.M_GDI;

                    MIL.MbufAllocColor(base_objects.MilSystem, m_BufSizeBand, m_BufSizeX, m_BufSizeY, 8 + MIL.M_UNSIGNED, Attributes, ref m_MilDrawingImage);


                    // Clear the buffer.
                    MIL.MbufClear(m_MilDrawingImage, 0);
                #endregion
            }
        }

        ~Mil9BaseCamera()
        {
            if (!m_SimulationMode)
            {
                Dispose(false);
            }
        }
        #endregion

        #region Protected Methods

        /// <summary>
        ///Public implementation of Dispose pattern callable by consumers.  
        /// </summary>
        public virtual void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///  Protected implementation of Dispose pattern. 
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
               // managed area releases
                ReleaseMyOwnedObjects();
            }

            //unmanaged area releases
            //if (_mtx != null) _mtx.Dispose();

            disposed = true;
        } 

        public virtual void ReleaseMyOwnedObjects()
        {
            FreeMatroxReferences();
        }

        protected virtual void FreeMatroxReferences()
        {
            if (!m_SimulationMode)
            {
                #region Free Default Matrox references
                if (m_MilImage != MIL.M_NULL)
                {
                    //// Clear the buffer.
                    MIL.MbufClear(m_MilImage, 0);
                    // Free allocated objects.
                    MIL.MbufFree(m_MilImage);
                    m_MilImage = MIL.M_NULL;
                }

                if (m_MilProcessingImage != MIL.M_NULL)
                {
                    // Clear the buffer.
                    MIL.MbufClear(m_MilProcessingImage, 0);
                    MIL.MbufFree(m_MilProcessingImage);

                    m_MilProcessingImage = MIL.M_NULL;
                }

                if (m_MilDrawingImage != MIL.M_NULL)
                {
                    //// Clear the buffer.
                    MIL.MbufClear(m_MilDrawingImage, 0);
                    MIL.MbufFree(m_MilDrawingImage);
                    m_MilDrawingImage = MIL.M_NULL;
                }

                if (m_MilDigitizer > 0)
                {
                    MIL.MdigFree(m_MilDigitizer);
                    m_MilDigitizer = MIL.M_NULL;
                }


                if (m_MilDisplay != MIL.M_NULL)
                {
                    MIL.MdispFree(m_MilDisplay);
                    m_MilDisplay = MIL.M_NULL;
                }
                #endregion
            }
        }

        #endregion

        #region Public Members

        public virtual void StartGrabbing()
        {
            if (m_MilDigitizer != MIL.M_NULL)
            {
                MIL_INT is_grabbing = MIL.M_NO;
                MIL.MdigInquire(m_MilDigitizer, MIL.M_GRAB_IN_PROGRESS, ref is_grabbing);
                if (MIL.M_NO == is_grabbing) // Grab continuously.
                    MIL.MdigGrabContinuous(m_MilDigitizer, m_MilImage);
            }
        }

        public virtual void StartGrabbing(IntPtr display_form_handle)
        {
            SetVideoWindow(display_form_handle);
            if (m_MilDigitizer != MIL.M_NULL)
            {
                MIL_INT is_grabbing = MIL.M_NO;
                MIL.MdigInquire(m_MilDigitizer, MIL.M_GRAB_IN_PROGRESS, ref is_grabbing);
                if (MIL.M_NO == is_grabbing) // Grab continuously.
                    MIL.MdigGrabContinuous(m_MilDigitizer, m_MilImage);
            }
        }

        public virtual void StopGrabbing()
        {
            if (m_MilDigitizer != MIL.M_NULL)
            {
                MIL_INT is_grabbing = MIL.M_NO;
                MIL.MdigInquire(m_MilDigitizer, MIL.M_GRAB_IN_PROGRESS, ref is_grabbing);
                if (MIL.M_YES == is_grabbing)
                    MIL.MdigHalt(m_MilDigitizer);
            }
        }

        public virtual void SetVideoWindow(IntPtr display_form_handle)
        {
            if (!m_SimulationMode)
            {
                #region Initialize Display Matrox Form


                // Select the MIL buffer to be displayed in the user-specified window.
                MIL.MdispSelectWindow(m_MilDisplay, m_MilImage, display_form_handle);

                MIL.MdispInquire(m_MilDisplay, MIL.M_OVERLAY_ID, ref m_MilOverlayImage);

                // Grab in the user window if supported.
                if (m_MilDigitizer != MIL.M_NULL && m_GrabOnStart)
                {
                    // Grab continuously.
                    MIL.MdigGrabContinuous(m_MilDigitizer, m_MilImage);
                }


                #endregion
            }
        }

   

        #region Automatic saving grabbed images thread functions

        public virtual void StopImageSavingThread()
        {
            Int32 iCount = 5;
            if (null != m_StoreGrabbedImagesTokenSource)
            {
                m_StoreGrabbedImagesTokenSource.Cancel();
                while (--iCount > 0)
                {
                    System.Threading.Thread.Sleep(100);
                }
            }
        }

        public virtual void StartImageSavingThread()
        {
            m_StoreGrabbedImagesTokenSource = new CancellationTokenSource();
            StoreGrabbedImagesThread = new Thread(new ParameterizedThreadStart(StoreGrabbedImageThreadLoop));
            StoreGrabbedImagesThread.IsBackground = true;
            StoreGrabbedImagesThread.SetApartmentState(ApartmentState.STA);
            StoreGrabbedImagesThread.Name = "Mil 9 Device - Store Grabbed Images Thread";
            StoreGrabbedImagesThread.Priority = ThreadPriority.Lowest;
            StoreGrabbedImagesThread.Start(m_StoreGrabbedImagesTokenSource.Token);
        }

        protected virtual void StoreGrabbedImageThreadLoop(object token)
        {
            try
            {
                while (true)
                {
                    if (((CancellationToken)token).IsCancellationRequested) return;
                    lock (SynchronizationSaveImageObj)
                    {
                        try
                        {
                            MIL.MbufExport(Path.Combine(m_DefaultTargetPath, Tools.String_Enum.StringEnum.GetStringValue(m_CameraIndex) + "_" +
                                                                                Convert.ToString(DateTime.Now.Hour) + "_" +
                                                                             Convert.ToString(DateTime.Now.Minute) + "_" +
                                                                             Convert.ToString(DateTime.Now.Second) + ".jpg"),
                                           MIL.M_JPEG_LOSSY,
                                           m_MilImage);
                        }
                        catch { }
                    }
                    if (((CancellationToken)token).IsCancellationRequested) return;
                    System.Threading.Thread.Sleep(m_SavingTimeout);
                }
            }
            catch (ThreadAbortException exc)
            {

                Console.WriteLine("Thread aborting, code is " +
                 exc.ExceptionState);
            }
        }
         
        #endregion

        public virtual void UpdateImageCropRectangle(Int32 image_crop_rectangle_left,
                                                     Int32 image_crop_rectangle_top,
                                                     Int32 image_crop_rectangle_width,
                                                     Int32 image_crop_rectangle_height)
        {

            ImageCropRectangle = new Rectangle(image_crop_rectangle_left, image_crop_rectangle_top, image_crop_rectangle_width, image_crop_rectangle_height);
        }


        public virtual void UpdateZoomFactor(float update_zoom_factor)
        {
            m_ZoomFactor += update_zoom_factor;

            MIL.MdispZoom(m_MilDisplay, m_ZoomFactor, m_ZoomFactor);
        }

        public virtual void ResetZoomFactor()
        {
            m_ZoomFactor = 1;

            MIL.MdispZoom(m_MilDisplay, m_ZoomFactor, m_ZoomFactor);
        }

        public virtual void UpdatePanScroll(Single update_horizontal_pan_factor, Single update_vertical_pan_factor)
        {
            MIL.MdispPan(m_MilDisplay, update_horizontal_pan_factor, update_vertical_pan_factor);
        }


        public virtual int CaptureAndSaveImage(string target_path)
        {
            if (m_SimulationMode)
            {
                return CaptureAndSaveImage(Path.GetDirectoryName(target_path), Path.GetTempFileName(), new RectangleF(0, 0, 1000, 1000));
            }
            else
            {
                try
                {
                    MIL.MbufExport(Path.Combine(target_path, Convert.ToString(DateTime.Now.Hour) + "_" +
                                                             Convert.ToString(DateTime.Now.Minute) + "_" +
                                                             Convert.ToString(DateTime.Now.Second) + ".jpg"),
                                   MIL.M_JPEG_LOSSY,
                                   m_MilImage);
                }
                catch (MILException exception)
                {
                    m_myBaseObjects.MatroxImageLog.Error(String.Format("Image saving Error : {0}", exception.Message));
                    return ErrorCodesList.FAILED;
                }
                return ErrorCodesList.OK;
            }
        }

        

        public virtual int CaptureAndSaveImage(string target_path, string target_file_name)
        {
            return CaptureAndSaveImage(target_path, target_file_name, new RectangleF(0, 0, 1000, 1000)); // Screen.PrimaryScreen.WorkingArea.Width, Screen.PrimaryScreen.WorkingArea.Height));
        }

        public virtual int CaptureAndSaveImage(string target_path, string target_file_name, RectangleF rectangle)
        {

            try
            {
                using (Bitmap bmpScreenCapture = new Bitmap((int)rectangle.Width,
                                                (int)rectangle.Height))
                {
                    using (Graphics g = Graphics.FromImage(bmpScreenCapture))
                    {
                        g.CopyFromScreen((int)rectangle.X,
                                            (int)rectangle.Y,
                                            0, 0,
                                            bmpScreenCapture.Size,
                                            CopyPixelOperation.SourceCopy);
                    }
                    String FileName = Path.Combine(target_path, target_file_name);
                    bmpScreenCapture.Save(FileName);
                    return ErrorCodesList.OK;
                }
            }catch
            {
                return ErrorCodesList.FAILED;  
            }

        } 
         

        #endregion

        #region IInputDevice Members

        public event DataEventHandler DataReceived;

        #endregion IInputDevice Members

        #region IOutputDevice Members

        public void Send(string data)
        {
            throw new NotImplementedException();
        }

        #endregion IOutputDevice Members

        #region IComponent Members

        public string Name
        {
            get { return "Mil 9 Base Camera Device"; }
        }

        #endregion IComponent Members


         
    }
}
