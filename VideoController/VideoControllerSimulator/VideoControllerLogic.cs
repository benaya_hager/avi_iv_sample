﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using NS_Video_Controller_Common.Events;
using NS_Video_Controller_Common.Events.NS_VideoComandEventsParameters;
using NS_Video_Controller_Common.Interfaces;
using NS_Video_Controller_Common.Service;
using SFW;
using VideoControllerSimulator;

namespace NS_VideoControllerSimulator
{
    public class VideoControllerLogicSym : VideoControllerLogic
    {
        public VideoControllerLogicSym(IMil10BaseCamera camera_device, IVideoControllerListener listener, IActionSyncManager actionSyncManager)
            : base(camera_device, listener, actionSyncManager)
        { 
        }

        public VideoControllerLogicSym(IVideoControllerListener listener, IActionSyncManager actionSyncManager, params IMil10BaseCamera[] cameras_list)
            : base(listener, actionSyncManager,cameras_list)
        { 
        }

        public VideoControllerLogicSym(Dictionary<CameraIndexEnum, IMil10BaseCamera> device_list, IVideoControllerListener listener, IActionSyncManager actionSyncManager)
            : base(device_list, listener, actionSyncManager)
        { 
        }
    }
}
