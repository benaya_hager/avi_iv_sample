﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using NS_Common;
using NS_Video_Controller_Common.Interfaces;
using SFW;
using Tools.CustomRegistry;

namespace VideoControllerSimulator
{
    ///// <summary>
    ///// Video Controller Simulator, will be replaced by Matrox Objects
    ///// </summary>
    //public class VideoController : IIODevice, IVideoController
    //{
    //    #region Protected Members

    //    protected IMil9Base m_BaseMatroxObjects;

    //    #endregion
        
    //    #region Constructors

    //    /// <summary>
    //    /// Base Empty constructor. protected for singleton implementation
    //    /// </summary>
    //    public VideoController(IUnityContainer container, IMil9Base base_matrox_objects)
    //    {
    //        InitRegistryData();
    //        m_BaseMatroxObjects = base_matrox_objects; 
    //    }

    //    #endregion 

    //    #region Protected functions

    //    /// <summary>
    //    /// Get base data from registry
    //    /// </summary>
    //    protected int InitRegistryData()
    //    {
    //        try
    //        {
    //            //Initialize application registry data
    //            cRegistryFunc objRegistry = new cRegistryFunc("Video Controller",
    //                                                          System.Reflection.Assembly.GetExecutingAssembly().GetName().ToString(),
    //                                                          cRegistryFunc._REG_BASE_KEY_TYPES.HKEY_LOCAL_MASHINE);
    //        }
    //        catch { };
    //        return ErrorCodesList.OK;
    //    }

    //    #endregion

    //    #region IIODevice implementations

    //    public string Name
    //    {
    //        get { return "VideoController"; }
    //    }

    //    public event DataEventHandler DataReceived;

    //    public void Send(string data)
    //    {
    //        throw new NotImplementedException();
    //    }
    //    #endregion

    //    #region Public Properties

    //    private Boolean m_isSimulationMode;
    //    /// <summary>
    //    /// // Flag: Is system running in simulation mode? 
    //    /// </summary>
    //    public Boolean isSimulationMode
    //    {
    //        get { return m_isSimulationMode; }
    //        protected set { m_isSimulationMode = value; }
    //    }
        
    //    #endregion

    //    #region Public Methods

    //    public virtual int SaveImage(int deviceID, string target_path, string target_file_name)
    //    {
    //        return SaveImage(deviceID ,target_path,  target_file_name,new RectangleF(0, 0, Screen.PrimaryScreen.WorkingArea.Width, Screen.PrimaryScreen.WorkingArea.Height));
    //    }

    //    public virtual int SaveImage(int deviceID, string target_path, string target_file_name, RectangleF rectangle)
    //    {
    //        try
    //        {
    //            using (Bitmap bmpScreenCapture = new Bitmap((int)rectangle.Width,
    //                                           (int)rectangle.Height))
    //            {
    //                using (Graphics g = Graphics.FromImage(bmpScreenCapture))
    //                {
    //                    g.CopyFromScreen((int)rectangle.X,
    //                                     (int)rectangle.Y,
    //                                     0, 0,
    //                                     bmpScreenCapture.Size,
    //                                     CopyPixelOperation.SourceCopy);
    //                }
    //                String FileName = Path.Combine(target_path, target_file_name);
    //                bmpScreenCapture.Save(FileName);
    //                return ErrorCodesList.OK;
    //            }
    //        }catch
    //        {
    //            return ErrorCodesList.FAILED;  
    //        }

    //    }

    //    #endregion
    //}
}
