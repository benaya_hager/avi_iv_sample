﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using Tools.CustomRegistry;
using Matrox.MatroxImagingLibrary;
using log4net;
using SFW;
using System.ComponentModel;
using System.IO;
using System.Threading;
using NS_Common;

namespace NS_Mil9Base
{
    public class Mil9Base : NS_Video_Controller_Common.Interfaces.IMil9Base, IDisposable
    {
        #region Protected Members

        // Flag: Has Dispose already been called? 
        protected bool disposed = false;

        #endregion

        #region Public Properties
         
        protected ILog m_MatroxImageLog;
        public ILog MatroxImageLog
        {
            get { return m_MatroxImageLog; }
            private set { m_MatroxImageLog = value; }
        }
        

        // MIL variables
        protected MIL_ID m_MilApplication = MIL.M_NULL;  // MIL Application identifier.
        public MIL_ID MilApplication
        {
            get { return m_MilApplication; }
            protected set { m_MilApplication = value; }
        }

        protected MIL_ID m_MilSystem = MIL.M_NULL;       // MIL System identifier.
        public MIL_ID MilSystem
        {
            get { return m_MilSystem; }
            protected set { m_MilSystem = value; }
        }
        


        /// <summary>
        /// Indicate if the system running in simulation mode,
        /// and not necessary  to raise error events
        /// </summary>
        protected Boolean m_SimulationMode = false;
        public Boolean SimulationMode
        {
            get { return m_SimulationMode; }
            protected set { m_SimulationMode = value; }
        }


        protected int m_TheNumberOfCameras = 1;
        /// <summary>
        /// Number of available cameras in system.
        /// </summary>
        public int NumberOfCameras
        {
            get { return m_TheNumberOfCameras; }
            set { m_TheNumberOfCameras = value; }
        }

        #endregion

        #region Constructors \ Destructor

        public Mil9Base()
        {
            InitRegistryData();

            m_MatroxImageLog = LogManager.GetLogger("MatroxImageLog");

           
            if (!m_SimulationMode)
            {
                #region Initialize base Matrox Classes
                // Allocate a MIL application.
                MIL.MappAlloc(MIL.M_DEFAULT, ref m_MilApplication);

                // Allocate a MIL system.
                MIL.MsysAlloc("M_DEFAULT", MIL.M_DEFAULT, MIL.M_DEFAULT, ref m_MilSystem);

                // Tell MIL to throw exceptions when an error occurs.
                // The type of Exception thrown by MIL functions is MILException
                MIL.MappControl(MIL.M_ERROR, MIL.M_THROW_EXCEPTION);

                #endregion
            }
        } 
      
      
        ~Mil9Base()
        {
            Dispose(false); 
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Get base data from registry
        /// </summary>
        protected int InitRegistryData()
        {
            try
            {
                //Initialize application registry data
                cRegistryFunc objRegistry = new cRegistryFunc("Video Controller",
                                                              System.Reflection.Assembly.GetExecutingAssembly().GetName().ToString(),
                                                              cRegistryFunc._REG_BASE_KEY_TYPES.HKEY_LOCAL_MASHINE);

                m_SimulationMode = (objRegistry.GetRegKeyValue("", "Is Simulation Mode", (m_SimulationMode == true ? 1 : 0)) == 1 ? true : false);
                objRegistry.SetRegKeyValue("", "Is Simulation Mode", m_SimulationMode == true ? 1 : 0);

                   
            }
            catch { };
            return ErrorCodesList.OK;
        }

        protected virtual void FreeMatroxReferences()
        {
            if (!m_SimulationMode)
            {
                #region Free Default Matrox references
                
                if (m_MilSystem != MIL.M_NULL)
                {
                    MIL.MsysFree(m_MilSystem);
                    m_MilSystem = MIL.M_NULL;
                }

                if (m_MilApplication != MIL.M_NULL)
                {
                    MIL.MappFree(m_MilApplication);
                    m_MilApplication = MIL.M_NULL;
                }
                #endregion
            }
        }

        #endregion

        #region Public Members

        public virtual void ReleaseMyOwnedObjects()
        {
            FreeMatroxReferences();
        }

        #endregion

        #region IDisposable

        // Public implementation of Dispose pattern callable by consumers. 
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern. 
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
               // managed area releases
            }

            //unmanaged area releases
            FreeMatroxReferences();

            disposed = true;
        }
 

        #endregion

    }
}
