﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections.Concurrent;
using SFW;
using Zaber_Common.Events.Zaber;
using Zaber_Common.Events.Zaber.NS_ZaberDataEventParameters;
using Zaber_Common.Events.Zaber.NS_ZaberComandEventsParameters;
using Zaber_Common;

namespace AVI_IV_Sample
{
    public partial class AxisControlView : UserControl, SFW.IComponent
    {
        #region Private Members

        private ConcurrentDictionary<System.Type, Action<IEventData>> m_Handlers;

        
        #endregion

        #region Public members

        public event EventGroupHandler EventGroupReady;


        private Byte  m_MyAxisID;

        public Byte MyAxisID
        {
            get { return m_MyAxisID; }
            set 
            {
                if (value != m_MyAxisID)
                {
                    m_MyAxisID = value;
                    grbxControlAxis.Text = String.Format("Axis: {0}", m_MyAxisID); 
                    SendEvent(new ZaberCommandEvent(ZaberCommandEventTypes.CT_START_AXIS_STATUS_MONITORING, new ZaberCommandPrmAxisMonitoringThread(m_MyAxisID, Convert.ToInt32(100))));
                }
            }
        }
        

        #endregion Public members

        #region Constructors

        public AxisControlView()
        {
            InitializeComponent();

            m_Handlers = new System.Collections.Concurrent.ConcurrentDictionary<Type, Action<IEventData>>();

            MapHandlers();
        }

        ~AxisControlView()
        {
            ClearHandlers();
        }

        #endregion

        #region Private methods



        private void SendEvent(IEventData Event)
        {
            EventGroupData eventGroupData = new EventGroupData();
            eventGroupData.Enqueue(Event);

            if (EventGroupReady != null)
                EventGroupReady(this, eventGroupData);
        }

        private void MapHandlers()
        {
            m_Handlers.TryAdd(typeof(ZaberDataEvent), HandleZaberDataEvent);
        }

        private void ClearHandlers()
        {
            Action<IEventData> res;
            if (null != m_Handlers)
            {
                m_Handlers.TryRemove(typeof(ZaberDataEvent), out res);
            }
        }

        
        #region IEventGroupConsumer Members

        public void OnProcessEventGroup(object sender, EventGroupData data)
        {
            foreach (IEventData singleEvent in data)
            {
                if (singleEvent != null)
                {
                    if (m_Handlers == null || !m_Handlers.ContainsKey(singleEvent.GetType()))
                    {
                        //throw new NullReferenceException("No handler mapped to this type of event " + singleEvent.GetType());
                    }
                    else
                    {
                        m_Handlers[singleEvent.GetType()](singleEvent);
                    }
                }
            }
        }

        #endregion IEventGroupConsumer Members

        #endregion


        #region GUI events handlers

        

        private void btnGo_Click(object sender, EventArgs e)
        {   

            SendEvent(new ZaberCommandEvent(ZaberCommandEventTypes.CT_MOVE_AXIS_ABSOLUTE, new ZaberCommandPrmMoveAbsMM(m_MyAxisID ,
                                                                                                                       Convert.ToSingle(nudTargetPosition.Value),
                                                                                                                       Convert.ToSingle(nudSpeed.Value),
                                                                                                                       Convert.ToSingle(nudAcceleration.Value))));
        }

        private void btnStartRelativeMotion_Click(object sender, EventArgs e)
        {

            SendEvent(new ZaberCommandEvent(ZaberCommandEventTypes.CT_MOVE_AXIS_RELATIVE, new ZaberCommandPrmMoveRelMM(m_MyAxisID,
                                                                                                                       Convert.ToSingle(nudRelativeMotionOffset.Value),
                                                                                                                       Convert.ToSingle(nudSpeed.Value),
                                                                                                                       Convert.ToSingle(nudAcceleration.Value))));
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            SendEvent(new ZaberCommandEvent(ZaberCommandEventTypes.CT_STOP_AXIS_MOTION, new ZaberCommandPrmStopAxisMotion(m_MyAxisID)));
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            SendEvent(new ZaberCommandEvent(ZaberCommandEventTypes.CT_SEND_AXIS_TO_HOME_POSITION, new ZaberCommandEventParameters(m_MyAxisID)));
        }


        #endregion


        private delegate void ZaberDataFormUpdateHandler(IEventData data);
        private void HandleZaberDataEvent(IEventData data)
        {
            if ((((ZaberDataEvent)data).DataParameters).DeviceID != m_MyAxisID) return;  

            if (this.InvokeRequired)
            {
                this.Invoke(new ZaberDataFormUpdateHandler(HandleZaberDataEvent), new Object[] { data });
            }
            else
            {

                switch (((ZaberDataEvent)data).DataType)
                {
                    case ZaberDataEventType.ET_CURRENT_POSITION_CHANGED:
                        {
                            CurrentPositionChanged((ZaberDataPrmCurPosChanged)((ZaberDataEvent)data).DataParameters);
                            break;
                        }
                    //case ZaberDataEventType.ET_DEVICE_MONITORING_THREAD_STATUS_CHANGED:
                    //    {
                    //        UpdateCurrentStateOfMonitoringThread((ZaberDataPrmDeviceMonitoringThreadStatus)((ZaberDataEvent)data).DataParameters);
                    //        break;
                    //    }
                    case ZaberDataEventType.ET_DEVICE_STATUS_CHANGED:
                        {
                            UpdateDeviceCurrentState((ZaberDataPrmDeviceStatus)((ZaberDataEvent)data).DataParameters);
                            break;
                        }
                    case ZaberDataEventType.ET_DEVICE_HOMED_STATUS_CHANGED:
                        {
                            UpdateAxisHomedStatus((ZaberDataPrmBoolean)((ZaberDataEvent)data).DataParameters);
                            break;
                        }
                    case ZaberDataEventType.ET_DEVICE_MOTION_COMPLETE:
                        {
                            UpdateInMotionStatus((ZaberDataPrmBoolean)((ZaberDataEvent)data).DataParameters);
                            break;
                        }
                }
            }
        }


        private void UpdateInMotionStatus(ZaberDataPrmBoolean zaberDataPrmBoolean)
        {
            chckbxMotionComplete.Checked = zaberDataPrmBoolean.BooleanParameter;
            //lblInMotion.Text = "Device Current State: \n" + Convert.ToString(zaberDataPrmBoolean.BooleanParameter);
        }

        private void UpdateAxisHomedStatus(ZaberDataPrmBoolean zaberDataPrmBoolean)
        {
            lblAxisHomedStatus.Text = String.Format("Axis Homed: ({0})", zaberDataPrmBoolean.BooleanParameter);
        }

        private void UpdateDeviceCurrentState(ZaberDataPrmDeviceStatus zaberDataPrmDeviceStatus)
        {
            lblDeviceCurrentState.Text = "Device Current State: \n" + Tools.String_Enum.StringEnum.GetStringValue(zaberDataPrmDeviceStatus.DeviceStatus);
        }

        //private void UpdateCurrentStateOfMonitoringThread(ZaberDataPrmDeviceMonitoringThreadStatus zaberDataPrmDeviceMonitoringThreadStatus)
        //{
        //    if (zaberDataPrmDeviceMonitoringThreadStatus.DeviceMonitoringThreadStatus)
        //    {
        //        btnMonitoringState.BackgroundImage = global::ZaberTest.Properties.Resources.Stop_Monitoring;
        //        btnMonitoringState.Tag = 1;
        //    }
        //    else
        //    {
        //        btnMonitoringState.BackgroundImage = global::ZaberTest.Properties.Resources.Start_Monitoring;
        //        btnMonitoringState.Tag = 0;
        //    }
        //}

        private void CurrentPositionChanged(ZaberDataPrmCurPosChanged zaberDataPrmCurPosChanged)
        {
            lblCurrentPosition.Text = String.Format("Current Position: \n {0}", zaberDataPrmCurPosChanged.CurrentPosition);
        }
    }
}
