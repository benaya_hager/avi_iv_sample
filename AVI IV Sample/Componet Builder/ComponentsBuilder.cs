﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Components;
using Microsoft.Practices.Unity;
using NS_Routines_Controller_Common.Interfaces;
using NS_Video_Controller_Common.Interfaces;
using Routines_Controller_UI;
using SFW;
using VideoControllerSimulator;
using Zaber_Common.Interfaces;
using Zaber_Controller_UI;

namespace AVI_IV_Sample.Componet_Builder
{
    public class ComponentsBuilder : IComponentBuilder
    {  
       private const string GlobalContainerKey = "Your global Unity container";
       Dictionary<string, SFW.IComponent> m_Components;
       GUIFactory m_GuiFactory;
       public IUnityContainer m_Container { set; get; }


        public void Bulid(IUnityContainer container)
        { 
            m_Container = container;
            m_Components = new Dictionary<string, IComponent>();

            //if (!m_Container.IsRegistered<IUnityContainer>())
            //{
                m_Container.RegisterInstance<IUnityContainer>(m_Container);
            //}
            m_GuiFactory = new GUIFactory(m_Components);

            RegisterComponents();

            CreateComponents();



            #region Fill Routine Controller objects

            ((IRoutinesController)(m_Components["RoutinesController"])).SetDeviceList(m_Components);

            #endregion Fill Routine Contoller objects
        }


      

        #region ICompBuilder Members
         

        public virtual void Release()
        {
            ReleaseComponents();
        }
 
        
      

       

        public virtual void ReleaseComponents() { }

        #endregion

        public string Name
        {
            get { return "AVI IV Components Builder"; }
        }

        public void CreateMainWindow()
        {
            m_GuiFactory.CreateItem("frmMainForm");
        }

        public void CreateComponents()
        {
            IVideoControllerComponentsBuilder videoBuilder;
            IRoutinesControllerComponentsBuilder routinesBuilder;
            IZaberControllerComponentsBuilder zaiberBilder;
            //using (m_Container)
            //{
                ParallelProcessor.ExecuteParallel(() =>
                {
                    //if (m_Container.IsRegistered<IRoutinesControllerComponentsBuilder>())
                    //{
                        routinesBuilder = m_Container.Resolve<IRoutinesControllerComponentsBuilder>("RoutinesControllerComponentsBuilder");
                        m_Container.RegisterInstance<IRoutinesControllerComponentsBuilder>(routinesBuilder);
                    //}
                }, () =>
                {
                    //if (m_Container.IsRegistered<IZaberControllerComponentsBuilder>())
                    //{
                        zaiberBilder = m_Container.Resolve<IZaberControllerComponentsBuilder>("ZaberControllerComponentsBuilder");
                        m_Container.RegisterInstance<IZaberControllerComponentsBuilder>(zaiberBilder);
                    //}
                }, () =>
                {
                    //if (m_Container.IsRegistered<IVideoControllerComponentsBuilder>())
                    //{
                    videoBuilder = m_Container.Resolve<IVideoControllerComponentsBuilder>("VideoControllerComponentsBuilder");
                    m_Container.RegisterInstance<IVideoControllerComponentsBuilder>(videoBuilder);
                    //}
                });
            //}

            FillLocalComponentsList();
            

        }
         
        public void RegisterComponents()
        {

            m_Container.RegisterType<IRoutinesControllerComponentsBuilder, RoutinesControllerComponentsBuilder>("RoutinesControllerComponentsBuilder",
                                                                                new InjectionConstructor(m_Container));
            m_Container.RegisterType<IZaberControllerComponentsBuilder, ZaberControllerComponentsBuilder>("ZaberControllerComponentsBuilder",
                                                                                new InjectionConstructor(m_Container));
            m_Container.RegisterType<IVideoControllerComponentsBuilder, VideoControllerComponentsBuilder>("VideoControllerComponentsBuilder",
                                                                                new InjectionConstructor(m_Container));
            
        }

        public Dictionary<string, SFW.IComponent> ComponentsList
        {
            get { return m_Components; } 
        }


        private void FillLocalComponentsList()
        {
            ResolveRoutineControllerObjects();
            ResolveZaberControllerObjects();
            ResolveVideoControllerObjects();
        }

        private void ResolveVideoControllerObjects()
        {
            lock (m_Components)
            {
                //m_Components.Add("Camera", (IInputDevice)m_Container.Resolve<IMil9BaseCamera>());

                m_Components.Add("VideoControllerListener", (Listener)m_Container.Resolve<IVideoControllerListener>());

                m_Components.Add("VideoControllerLogic", (Logic)m_Container.Resolve<IVideoControllerLogic>());
            }
        }

        private void ResolveZaberControllerObjects()
        {
            lock (m_Components)
            {
                m_Components.Add("ZaberMotionController", (IIODevice)m_Container.Resolve<IZaberMotionController>());

                m_Components.Add("ZaberControllerListener", (Listener)m_Container.Resolve<IZaberControllerListener>());

                m_Components.Add("ZaberControllerLogic", (Logic)m_Container.Resolve<IZaberControllerLogic>());
            }
        }

        private void ResolveRoutineControllerObjects()
        {
            lock (m_Components)
            {
                m_Components.Add("RoutinesController", (IIODevice)m_Container.Resolve<IRoutinesController>());

                m_Components.Add("RoutineFactoryLogic", (Logic)m_Container.Resolve<IRoutineControllerLogic>());

                m_Components.Add("RoutineControllerListener", (Listener)m_Container.Resolve<IRoutineControllerListener>());
            }
        }


        #region Error handling

        private void SetApplicationUnhandledExeptionsHandle()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // Add the event handler for handling UI thread exceptions to the event.
            Application.ThreadException += new ThreadExceptionEventHandler(UIThreadException);

            // Set the not handled exception mode to force all Windows Forms errors to go through
            // our handler.
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);

            // Add the event handler for handling non-UI thread exceptions to the event.
            AppDomain.CurrentDomain.UnhandledException +=
                new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
        }


        //Handle the UI exceptions by showing a dialog box
        /// <summary>
        /// Handle the UI exceptions by showing a dialog box, and asking the user whether
        /// or not they wish to abort execution.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="t"></param>
        public static void UIThreadException(object sender, ThreadExceptionEventArgs t)
        {
            DialogResult result = DialogResult.Cancel;
            try
            {
                result = ShowThreadExceptionDialog("Windows Forms Error", t.Exception);
            }
            catch
            {
                try
                {
                    MessageBox.Show("Fatal Windows Forms Error",
                        "Fatal Windows Forms Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                finally
                {
                    Application.Exit();
                }
            }

            // Exits the program when the user clicks Abort.
            //if (result == DialogResult.Abort)
            Application.Exit();
        }

        //Handle the Domain UI exceptions by showing a dialog box
        /// <summary>
        /// Handle the UI exceptions by showing a dialog box, and asking the user whether
        /// or not they wish to abort execution.
        /// NOTE: This exception cannot be kept from terminating the application - it can only
        /// log the event, and inform the user about it.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            try
            {
                Exception ex = (Exception)e.ExceptionObject;
                string errorMsg = "An application error occurred. Please contact the administrator " +
                    "with the following information:\n\n";

                // Since we can't prevent the application from terminating, log this to the event log.
                if (!EventLog.SourceExists("ThreadException"))
                {
                    EventLog.CreateEventSource("ThreadException", "Application");
                }

                // Create an EventLog instance and assign its source.
                EventLog myLog = new EventLog();
                myLog.Source = "ThreadException";
                myLog.WriteEntry(errorMsg + ex.Message + "\n\nStack Trace:\n" + ex.StackTrace);
            }
            catch (Exception exc)
            {
                try
                {
                    MessageBox.Show("Fatal Non-UI Error",
                        "Fatal Non-UI Error. Could not write the error to the event log. Reason: "
                        + exc.Message, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                finally
                {
                    Application.Exit();
                }
            }
        }


        // Creates the error message and displays it.
        /// <summary>
        /// Creates the error message and displays it.
        /// </summary>
        /// <param name="title"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public static DialogResult ShowThreadExceptionDialog(string title, Exception e)
        {
            string errorMsg = "An application error occurred. Please contact the administrator " +
                    "with the following information:\n\n";

            errorMsg += e.Message + "\n\nStack Trace:\n" + e.StackTrace;

            return MessageBox.Show(errorMsg, title, MessageBoxButtons.OK,
                MessageBoxIcon.Stop);
        }

        #endregion Error handling
    }
}
