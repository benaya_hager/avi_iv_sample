﻿namespace AVI_IV_Sample
{
    partial class frmMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCreateRoutine = new System.Windows.Forms.Button();
            this.btnStartRoutinePerformance = new System.Windows.Forms.Button();
            this.btnShowVideo = new System.Windows.Forms.Button();
            this.tlpnlAllControlos = new System.Windows.Forms.TableLayoutPanel();
            this.axisControl_Y = new AVI_IV_Sample.AxisControlView();
            this.axisControl_X = new AVI_IV_Sample.AxisControlView();
            this.tlpnlAllControlos.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCreateRoutine
            // 
            this.btnCreateRoutine.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCreateRoutine.Location = new System.Drawing.Point(10, 4);
            this.btnCreateRoutine.Name = "btnCreateRoutine";
            this.btnCreateRoutine.Size = new System.Drawing.Size(79, 51);
            this.btnCreateRoutine.TabIndex = 0;
            this.btnCreateRoutine.Text = "Create Routine";
            this.btnCreateRoutine.UseVisualStyleBackColor = true;
            this.btnCreateRoutine.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnStartRoutinePerformance
            // 
            this.btnStartRoutinePerformance.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnStartRoutinePerformance.Location = new System.Drawing.Point(10, 101);
            this.btnStartRoutinePerformance.Name = "btnStartRoutinePerformance";
            this.btnStartRoutinePerformance.Size = new System.Drawing.Size(79, 47);
            this.btnStartRoutinePerformance.TabIndex = 1;
            this.btnStartRoutinePerformance.Text = "Start Routine";
            this.btnStartRoutinePerformance.UseVisualStyleBackColor = true;
            this.btnStartRoutinePerformance.Click += new System.EventHandler(this.btnStartRoutinePerformance_Click);
            // 
            // btnShowVideo
            // 
            this.btnShowVideo.Location = new System.Drawing.Point(675, 3);
            this.btnShowVideo.Name = "btnShowVideo";
            this.btnShowVideo.Size = new System.Drawing.Size(87, 51);
            this.btnShowVideo.TabIndex = 2;
            this.btnShowVideo.Text = "Show Video";
            this.btnShowVideo.UseVisualStyleBackColor = true;
            this.btnShowVideo.Click += new System.EventHandler(this.btnShowVideo_Click);
            // 
            // tlpnlAllControlos
            // 
            this.tlpnlAllControlos.ColumnCount = 5;
            this.tlpnlAllControlos.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tlpnlAllControlos.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 239F));
            this.tlpnlAllControlos.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpnlAllControlos.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 263F));
            this.tlpnlAllControlos.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tlpnlAllControlos.Controls.Add(this.btnCreateRoutine, 0, 0);
            this.tlpnlAllControlos.Controls.Add(this.btnShowVideo, 4, 0);
            this.tlpnlAllControlos.Controls.Add(this.btnStartRoutinePerformance, 0, 1);
            this.tlpnlAllControlos.Controls.Add(this.axisControl_Y, 3, 3);
            this.tlpnlAllControlos.Controls.Add(this.axisControl_X, 0, 3);
            this.tlpnlAllControlos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpnlAllControlos.Location = new System.Drawing.Point(0, 0);
            this.tlpnlAllControlos.Name = "tlpnlAllControlos";
            this.tlpnlAllControlos.RowCount = 4;
            this.tlpnlAllControlos.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tlpnlAllControlos.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.tlpnlAllControlos.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpnlAllControlos.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 334F));
            this.tlpnlAllControlos.Size = new System.Drawing.Size(772, 553);
            this.tlpnlAllControlos.TabIndex = 3;
            this.tlpnlAllControlos.Paint += new System.Windows.Forms.PaintEventHandler(this.tlpnlAllControlos_Paint);
            // 
            // axisControl_Y
            // 
            this.tlpnlAllControlos.SetColumnSpan(this.axisControl_Y, 2);
            this.axisControl_Y.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axisControl_Y.Location = new System.Drawing.Point(412, 222);
            this.axisControl_Y.MyAxisID = ((byte)(0));
            this.axisControl_Y.Name = "axisControl_Y";
            this.axisControl_Y.Size = new System.Drawing.Size(357, 328);
            this.axisControl_Y.TabIndex = 4;
            // 
            // axisControl_X
            // 
            this.tlpnlAllControlos.SetColumnSpan(this.axisControl_X, 2);
            this.axisControl_X.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axisControl_X.Location = new System.Drawing.Point(3, 222);
            this.axisControl_X.MyAxisID = ((byte)(0));
            this.axisControl_X.Name = "axisControl_X";
            this.axisControl_X.Size = new System.Drawing.Size(333, 328);
            this.axisControl_X.TabIndex = 3;
            // 
            // frmMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(772, 553);
            this.Controls.Add(this.tlpnlAllControlos);
            this.MinimumSize = new System.Drawing.Size(615, 470);
            this.Name = "frmMainForm";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMainForm_FormClosing);
            this.Shown += new System.EventHandler(this.frmMainForm_Shown);
            this.tlpnlAllControlos.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCreateRoutine;
        private System.Windows.Forms.Button btnStartRoutinePerformance;
        private System.Windows.Forms.Button btnShowVideo;


        private System.Windows.Forms.TableLayoutPanel tlpnlAllControlos;
        public AxisControlView axisControl_Y;
        public AxisControlView axisControl_X;
    }
}

