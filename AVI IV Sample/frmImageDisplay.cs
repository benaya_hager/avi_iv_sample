﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NS_Video_Controller_Common.Events;
using NS_Video_Controller_Common.Events.NS_VideoComandEventsParameters;
using NS_Video_Controller_Common.Service;
using SFW;

namespace AVI_IV_Sample
{
    public partial class frmImageDisplay : Form, SFW.IComponent, SFW.IEventGroupConsumer
    {
        #region Private Members

        private ConcurrentDictionary<System.Type, Action<IEventData>> m_Handlers;
        private Boolean m_FormShowen = false;
        private Boolean m_FormClosing = false;
        
        #endregion Private Members


        #region Public Members

        public event EventGroupHandler EventGroupReady;

        public Boolean IsGrabbing()
        {
            Boolean res = true;
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(
                                delegate
                                {
                                    //axMDigitizer1.GrabAbort();
                                    //res = m_GrabTimer.Enabled;
                                }
                            ));
            }
            else
            {
                //res = m_GrabTimer.Enabled;
            }
            return res;
        }

        private CameraIndexEnum m_myCameraIndex = CameraIndexEnum.First;
        public CameraIndexEnum myCameraIndex
        {
            get { return m_myCameraIndex; }
            set
            {
                m_myCameraIndex = value;
                this.Text = Tools.String_Enum.StringEnum.GetStringValue(m_myCameraIndex) + " Camera";
            }
        }

        #endregion Public Members

        #region Constructors

        public frmImageDisplay(Form parent_form,
                               String component_name,
                               CameraIndexEnum camera_index,
                               bool GrabOnStart = false)
        {
            InitializeComponent();

            this.Owner = parent_form;

            m_Handlers = new System.Collections.Concurrent.ConcurrentDictionary<Type, Action<IEventData>>();

            this.Text = Tools.String_Enum.StringEnum.GetStringValue(camera_index);


            myCameraIndex = camera_index;

            MapHandlers();
            this.MouseWheel += new MouseEventHandler(frmImageDisplay_MouseWheel);
            this.FormClosing +=frmImageDisplay_FormClosing;
        }

        #endregion

        #region Public Methods

        public void MapHandlers()
        {

        }

        public void ClearHandlers()
        {
            //    Action<IEventData> res;
            //    m_Handlers.TryRemove(typeof(VideoCommandEvent), out res);
        }


        #endregion Public Methods


        #region GUI Event Handlers

        private void frmImageDisplay_Load(object sender, EventArgs e)
        {  
        }

        private void frmImageDisplay_Shown(object sender, EventArgs e)
        {
            SendEvent(new VideoCommandEvent(VideoCommandEventTypes.CT_START_GRABBING, new VideoCommandPrmSetDisplayWindow(this.Handle , m_myCameraIndex)));
            
            SendEvent(new VideoCommandEvent(VideoCommandEventTypes.CT_GET_LAST_SESSION_SETUP_PARAMETERS, new VideoCommandEventParameters(m_myCameraIndex)));
            
            m_FormShowen = true;
            m_FormClosing = false;
        }

        private void frmImageDisplay_DockStateChanged(object sender, EventArgs e)
        {
            if (m_FormShowen && !m_FormClosing)
            {
                SendEvent(new VideoCommandEvent(VideoCommandEventTypes.CT_START_GRABBING, new VideoCommandPrmSetDisplayWindow(this.Handle , m_myCameraIndex)));
            }
        }

        private void frmImageDisplay_VisibleChanged(object sender, EventArgs e)
        {
            //if (this.Visible == false)
            //{ m_FormShowen = false; }
        }

        private void frmImageDisplay_FormClosing(object sender, FormClosingEventArgs e)
        {
            m_FormClosing = true;
            SendEvent(new VideoCommandEvent(VideoCommandEventTypes.CT_STOP_GRABBING, new VideoCommandEventParameters(m_myCameraIndex)));
            SendEvent(new VideoCommandEvent(VideoCommandEventTypes.CT_RELEASE_COMPONENTS, new VideoCommandEventParameters(m_myCameraIndex)));
        }

        private void frmImageDisplay_MouseWheel(object sender, MouseEventArgs e)
        {
            Single update_zoom_factor = 0;
            if (e.Delta < 0)
            {
                update_zoom_factor = (Single)(-0.01);
            }
            else
            {
                update_zoom_factor = (Single)0.01;
            }

            SendEvent(new VideoCommandEvent(VideoCommandEventTypes.CT_UPDATE_ZOOM_FACTOR, new VideoCommandPrmUpdateZoom(update_zoom_factor, m_myCameraIndex)));
        }

        private void btnZoomIn_Click(object sender, EventArgs e)
        {
            SendEvent(new VideoCommandEvent(VideoCommandEventTypes.CT_UPDATE_ZOOM_FACTOR, new VideoCommandPrmUpdateZoom(0.1F, m_myCameraIndex)));
        }

        private void btnZoomOut_Click(object sender, EventArgs e)
        {
            SendEvent(new VideoCommandEvent(VideoCommandEventTypes.CT_UPDATE_ZOOM_FACTOR, new VideoCommandPrmUpdateZoom(-0.1F, m_myCameraIndex)));
        }

        private void ScrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            SendEvent(new VideoCommandEvent(VideoCommandEventTypes.CT_UPDATE_PAN_FACTOR, new VideoCommandPrmUpdatePan(hScrollBar1.Value, vScrollBar1.Value, m_myCameraIndex)));
        }

        private void startGrabbingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Grab continuously.
            SendEvent(new VideoCommandEvent(VideoCommandEventTypes.CT_START_GRABBING, new VideoCommandPrmSetDisplayWindow(this.Handle , m_myCameraIndex)));
        }

        private void stopGrabbingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Stop continuous grab.
            SendEvent(new VideoCommandEvent(VideoCommandEventTypes.CT_STOP_GRABBING, new VideoCommandEventParameters(m_myCameraIndex)));
        }

        #endregion GUI Event Handlers


        #region IEventGroupConsumer Members

        private void SendEvent(IEventData Event)
        {
            EventGroupData eventGroupData = new EventGroupData();
            eventGroupData.Enqueue(Event);

            if (EventGroupReady != null)
                EventGroupReady(this, eventGroupData);
        }

        public void OnProcessEventGroup(object sender, EventGroupData data)
        {
            foreach (IEventData singleEvent in data)
            {
                if (singleEvent != null)
                {
                    if (m_Handlers == null || !m_Handlers.ContainsKey(singleEvent.GetType()))
                    {
                        //throw new NullReferenceException("No handler mapped to this type of event " + singleEvent.GetType());
                    }
                    else
                    {
                        m_Handlers[singleEvent.GetType()](singleEvent);
                    }
                }
            }

            //RaiseEventGroup(data);
        }

        protected void RaiseEventGroup(EventGroupData data)
        {
            if (EventGroupReady != null)
                EventGroupReady(this, data);
        }

        #endregion IEventGroupConsumer Members
    }
}
