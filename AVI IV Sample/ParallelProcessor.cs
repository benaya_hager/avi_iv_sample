﻿using System;
using System.Threading;

namespace Components
{
    public class ParallelProcessor
    {

        public delegate void Method();

        /// <summary>
        /// Executes a set of methods in parallel and returns the results
        /// from each in an array when all threads have completed.  The methods
        /// must take no parameters and have no return value.
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        [MTAThread]
        public static void ExecuteParallel(params Method[] methods)
        {
            // Initialize the reset events to keep track of completed threads
            ManualResetEvent[] resetEvents = new ManualResetEvent[methods.Length];

            // Launch each method in it's own thread
            for (int i = 0; i < methods.Length; i++)
            {
                resetEvents[i] = new ManualResetEvent(false);
                ThreadPool.QueueUserWorkItem(new WaitCallback((object index) =>
                {
                    int methodIndex = (int)index;
                    try
                    {
                        // Execute the method
                        methods[methodIndex]();
                        resetEvents[methodIndex].Set();
                    }
                    catch (Exception ex)
                    {
                        AVI_IV_Sample.Componet_Builder.ComponentsBuilder.UIThreadException(null, new ThreadExceptionEventArgs(ex)); 
                    }
                    finally
                    {
                       
                    }
                    // Tell the calling thread that we're done
                    
                }), i);
            }

            for (int i = 0; i < methods.Length; i++)
                resetEvents[i].WaitOne();
            // Wait for all threads to execute
            //WaitHandle.WaitOne(resetEvents);
        }
    }
}