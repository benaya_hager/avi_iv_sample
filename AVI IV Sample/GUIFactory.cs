﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NS_Routines_Controller_Common.Interfaces;
using SFW;


namespace AVI_IV_Sample
{
    public class GUIFactory : RuntimeObjectsFactory
    {
        public GUIFactory(Dictionary<string, SFW.IComponent> components)
            : base(components)
        {   
        }

        public override SFW.IComponent CreateItem(string type)
        {
            return CreateItem(type, null);
        }

        public SFW.IComponent CreateItem(string type, Form OwnerWindow)
        {
            SFW.IComponent res = null;
            if (m_Components.ContainsKey(type))
                res = m_Components[type];
            else
            {
                switch (type)
                {
                    case "frmMainForm":
                        {
                            m_Components.Add("frmMainForm", new frmMainForm("frmMainForm", this));
                            ((frmMainForm)m_Components["frmMainForm"]).FormClosing += new System.Windows.Forms.FormClosingEventHandler(GUIFactory_FormClosing);

                            ((frmMainForm)m_Components["frmMainForm"]).EventGroupReady += ((Logic)m_Components["RoutineFactoryLogic"]).OnProcessEventGroup;
                            


                            ((Listener)m_Components["RoutineControllerListener"]).EventGroupReady += ((frmMainForm)m_Components["frmMainForm"]).OnProcessEventGroup;

                            ((frmMainForm)m_Components["frmMainForm"]).axisControl_X.EventGroupReady += ((Logic)m_Components["ZaberControllerLogic"]).OnProcessEventGroup;
                            ((frmMainForm)m_Components["frmMainForm"]).axisControl_Y.EventGroupReady += ((Logic)m_Components["ZaberControllerLogic"]).OnProcessEventGroup;

                            ((Listener)m_Components["ZaberControllerListener"]).EventGroupReady += ((frmMainForm)m_Components["frmMainForm"]).axisControl_X.OnProcessEventGroup;
                            ((Listener)m_Components["ZaberControllerListener"]).EventGroupReady += ((frmMainForm)m_Components["frmMainForm"]).axisControl_Y.OnProcessEventGroup;

                            ((frmMainForm)m_Components["frmMainForm"]).axisControl_X.MyAxisID = 1;
                            ((frmMainForm)m_Components["frmMainForm"]).axisControl_Y.MyAxisID = 2;


                            res = m_Components["frmMainForm"];
                            break;
                        }
                    case "frmImageDisplay":
                        {
                            m_Components.Add("frmImageDisplay", new frmImageDisplay(((frmMainForm)m_Components["frmMainForm"]),
                                                                                    "frmImageDisplay",
                                                                                     NS_Video_Controller_Common.Service.CameraIndexEnum.First,
                                                                                     false));
                            ((frmImageDisplay)m_Components["frmImageDisplay"]).FormClosing += new System.Windows.Forms.FormClosingEventHandler(GUIFactory_FormClosing);

                            ((frmImageDisplay)m_Components["frmImageDisplay"]).EventGroupReady += ((Logic)m_Components["VideoControllerLogic"]).OnProcessEventGroup;

                            ((Listener)m_Components["VideoControllerListener"]).EventGroupReady += ((frmImageDisplay)m_Components["frmImageDisplay"]).OnProcessEventGroup;
                            
                            res = m_Components["frmImageDisplay"];
                            break;
                        }
                    default:
                        {
                            res = null;
                            break;
                        }
                }
            }
            return res;
        }

       
        public override void ReleaseItem(string type)
        {
            switch (type)
            {
                case "frmSystemLog":
                    {
                        m_Components.Remove("frmSystemLog");
                        break;
                    }
            }
        }

        protected void GUIFactory_Unloaded(object sender, System.Windows.RoutedEventArgs e)
        {
            System.Reflection.PropertyInfo pi = sender.GetType().GetProperty("MyComponentName");
            if (null != pi)
            {
                String name = (String)pi.GetValue(sender, null);

                System.Reflection.MethodInfo mi = sender.GetType().GetMethod("ReleaseChildWindows");
                if (mi != null)
                    mi.Invoke(sender, null);
                if (null != name && "" != name)
                    m_Components.Remove(name);
            }
        }


        protected  void GUIFactory_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            if (e.Cancel == true) return;

            System.Reflection.PropertyInfo pi = sender.GetType().GetProperty("MyComponentName");
            if (null != pi)
            {
                String name = (String)pi.GetValue(sender, null);

                System.Reflection.MethodInfo mi = sender.GetType().GetMethod("ReleaseChildWindows");
                if (mi != null)
                    mi.Invoke(sender, null);
                if (null != name && "" != name)
                    m_Components.Remove(name);
            }
        }


        protected void CreateChildWindows(SFW.IComponent component)
        {
            System.Reflection.MethodInfo mi = component.GetType().GetMethod("CreateChildWindows");
            if (mi != null)
                mi.Invoke(component, new object[] { this });
        }
    }
}
 
