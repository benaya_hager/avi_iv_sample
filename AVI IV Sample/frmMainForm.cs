﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AVI_IV_Sample.Routine_Definitions.Actions.AVI_Special;
using NS_Routines_Controller_Common.Events.Routines_Controller;
using NS_Routines_Controller_Common.Events.Routines_Controller.RCComandEventsParameters;
using NS_Routines_Controller_Common.Events.Routines_Controller.RCDataEventParameters;
using NS_Routines_Controller_Common.Routine_Definitions.Base_Classes;
using SFW;

namespace AVI_IV_Sample
{
    public partial class frmMainForm : Form, SFW.IComponent,  SFW.IEventGroupConsumer
    {
        private Int32 m_RoutineIndex;
        private ConcurrentDictionary<System.Type, Action<IEventData>> m_Handlers;
        private GUIFactory m_GuiFactory;

        public event EventGroupHandler EventGroupReady;



        private Guid  m_MyUniqID;
        public Guid  MyUniqID
        {
            get { return m_MyUniqID; }
            set { m_MyUniqID = value; }
        }
        

        public frmMainForm(String component_name, GUIFactory factory)
        {
            InitializeComponent();

            m_MyUniqID = Guid.NewGuid();

            m_GuiFactory = factory;

            m_Handlers = new System.Collections.Concurrent.ConcurrentDictionary<Type, Action<IEventData>>();

            MapHandlers();

        }

        private void frmMainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
             
        }

        private void button1_Click(object sender, EventArgs e)
        {
            NS_Routines_Controller_Common.Routine_Definitions.Routines.Routine routine = new NS_Routines_Controller_Common.Routine_Definitions.Routines.Routine("Test Routine");
            NS_Routines_Controller_Common.Routine_Definitions.Steps.Step step = new NS_Routines_Controller_Common.Routine_Definitions.Steps.Step("capture step");

            AVIActionCapturePanelImages action = new AVIActionCapturePanelImages("capture panel action",
                                                                                 new byte[] { 1,2 },
                                                                                 new PointF(105, 3),
                                                                                 new RectangleF(0, 0, 38, 8),
                                                                                 new PointF(38,8),
                                                                                 new Size(1,1), 
                                                                                 new Size(10, 4),
                                                                                 3, 3,
                                                                                 "Image", @"C:\Medinol\AVI IV\Captured Images\", true);
            step.AddAction(action);
            routine.AddSingleStep(step, true);
 
            SendEvent(new RCCommandEvent (RCCommandType.CT_ADD_ROUTINE_TO_QUEUE, new RCCommandPrmAddRoutineToQueue(this,m_MyUniqID, routine)));

        }

        private delegate void RCDataFormUpdateHandler(IEventData data);

        private void HandleRCDataEvent(IEventData data)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new RCDataFormUpdateHandler(HandleRCDataEvent), new Object[] { data });
            }
            else
            {
                switch (((RCDataEvent)data).DataType)
                {
                    case RCDataEventType.ET_NEW_ROUTINE_LOADED_TO_PERFORMANCE_QUEUE:
                        {
                            UpdateNewLoadedRoutinePlayer((RCDataPrmNewRoutineLoaded)((RCDataEvent)data).DataParameters);
                            break;
                        }
                }
            }
        }

        private void UpdateNewLoadedRoutinePlayer(RCDataPrmNewRoutineLoaded rCDataPrmNewRoutineLoaded)
        {
            if (this.Equals(rCDataPrmNewRoutineLoaded.Owner))
                m_RoutineIndex = ((BaseRoutineDescriptor)rCDataPrmNewRoutineLoaded.Routine).RoutineIndex; 
        }

        #region IEventGroupConsumer Members


        private void SendEvent(IEventData Event)
        {
            EventGroupData eventGroupData = new EventGroupData();
            eventGroupData.Enqueue(Event);

            if (EventGroupReady != null)
                EventGroupReady(this, eventGroupData);
        }

        public void MapHandlers()
        {
            m_Handlers.TryAdd(typeof(RCDataEvent), HandleRCDataEvent);
        }

        public void ClearHandlers()
        {
            Action<IEventData> res;
            if (null != m_Handlers)
            {
                m_Handlers.TryRemove(typeof(RCDataEvent), out res);
            }
        }

        public void OnProcessEventGroup(object sender, EventGroupData data)
        {
            foreach (IEventData singleEvent in data)
            {
                if (singleEvent != null)
                {
                    if (m_Handlers == null || !m_Handlers.ContainsKey(singleEvent.GetType()))
                    {
                        //throw new NullReferenceException("No handler mapped to this type of event " + singleEvent.GetType());
                    }
                    else
                    {
                        m_Handlers[singleEvent.GetType()](singleEvent);
                    }
                }
            }

            //RaiseEventGroup(data);
        }

        protected void RaiseEventGroup(EventGroupData data)
        {
            if (EventGroupReady != null)
                EventGroupReady(this, data);
        }

        #endregion IEventGroupConsumer Members

        private void btnStartRoutinePerformance_Click(object sender, EventArgs e)
        {
            SendEvent(new RCCommandEvent(RCCommandType.CT_START_ROUTINE_BY_INDEX, new RCCommandPrmStartRoutineByIndex( m_RoutineIndex )));

        }

        private void frmMainForm_Shown(object sender, EventArgs e)
        {

        }

        private void btnShowVideo_Click(object sender, EventArgs e)
        {
            frmImageDisplay frm_image_display = (frmImageDisplay)m_GuiFactory.CreateItem("frmImageDisplay", this);
            frm_image_display.Show(this); 
        }

        private void lblRelativeMotionOffset_Click(object sender, EventArgs e)
        {

        }

        private void tlpnlAllControlos_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
