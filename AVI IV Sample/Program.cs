﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using AVI_IV_Sample.Componet_Builder;
using Microsoft.Practices.Unity;
using NS_Common.ApplicationExtensions;


namespace AVI_IV_Sample
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            IUnityContainer container = new UnityContainer();

            using (container)
            {
                ComponentsBuilder builder = new ComponentsBuilder();
                builder.Bulid(container);
            
                builder.CreateMainWindow();

                Application.Run(((System.Windows.Forms.Form)(builder.ComponentsList["frmMainForm"])));
        
            
            //Application.Run(new frmMainForm());
            
            }
        }
    }
}
